import UIKit
import Flutter
import Cloudpayments
import WebKit

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate{
    override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let cardPaymentsChannel = FlutterMethodChannel(name: "com.flutter.benefits/cardPayments",
                                              binaryMessenger: controller.binaryMessenger)
    
    cardPaymentsChannel.setMethodCallHandler({
        [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          // Note: this method is invoked on the UI thread.

        if (call.method == "createCryptogram") {
            guard let args = call.arguments as? [String: String] else {return}
            let cardCryptogramPacket = Card.makeCardCryptogramPacket(with: args["card_number"]!, expDate: args["card_date"]!, cvv: args["card_cvc"]!, merchantPublicID: args["merchant_public_id"]!)
            result(cardCryptogramPacket)
            return
        } else {
            result(FlutterMethodNotImplemented)
            return
        }
    })

    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}


