package com.main.benefit_flutter

import android.os.Build
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.core.view.WindowCompat
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import ru.cloudpayments.sdk.card.Card

class MainActivity: FlutterFragmentActivity(){
    private val channel = "com.flutter.benefits/cardPayments"

    override fun onCreate(savedInstanceState: Bundle?) {
        // Aligns the Flutter view vertically with the window.
        WindowCompat.setDecorFitsSystemWindows(getWindow(), false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            // Disable the Android splash screen fade out animation to avoid
            // a flicker before the similar frame is drawn in Flutter.
            splashScreen.setOnExitAnimationListener { splashScreenView -> splashScreenView.remove() }
        }

        super.onCreate(savedInstanceState)
    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channel).setMethodCallHandler { call, result ->
            if (call.method == "createCryptogram") {
                val arguments = call.arguments() as Map<String, String>
                val cardCryptogram = Card.cardCryptogram(arguments["card_number"].toString(), arguments["card_date"].toString(), arguments["card_cvc"].toString(), arguments["merchant_public_id"].toString())

                result.success(cardCryptogram)
            } else {
                result.notImplemented()
            }
        }
    }

}

