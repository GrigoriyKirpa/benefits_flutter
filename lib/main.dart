import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/models/payment.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/presentation/screens/account_type_selection_screen.dart';
import 'package:benefit_flutter/presentation/screens/chat_with_manager_screen.dart';
import 'package:benefit_flutter/presentation/screens/code_verification_screen.dart';
import 'package:benefit_flutter/presentation/screens/company_details_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/company_details_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/credit_card_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/credit_card_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/documents_screen.dart';
import 'package:benefit_flutter/presentation/screens/login_screen.dart';
import 'package:benefit_flutter/presentation/screens/main_navigation_screen.dart';
import 'package:benefit_flutter/presentation/screens/main_page_screen.dart';
import 'package:benefit_flutter/presentation/screens/order_registered_screen.dart';
import 'package:benefit_flutter/presentation/screens/password_reset_account_selection_screen.dart';
import 'package:benefit_flutter/presentation/screens/password_reset_screen.dart';
import 'package:benefit_flutter/presentation/screens/payment_selection_screen.dart';
import 'package:benefit_flutter/presentation/screens/phone_verification_screen.dart';
import 'package:benefit_flutter/presentation/screens/product_screen.dart';
import 'package:benefit_flutter/presentation/screens/recipient_registration_screen.dart';
import 'package:benefit_flutter/presentation/screens/registration_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_screen.dart';
import 'package:benefit_flutter/presentation/screens/start_screen.dart';
import 'package:benefit_flutter/presentation/screens/three_ds_verification_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:screen_loader/screen_loader.dart';

void main() async {
  configScreenLoader(
    loader: Center(
      child: CircularProgressIndicator(),
    ),
    bgBlur: 10.0,
  );
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    /*
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
     */
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    ButtonStyle customBtnStyle = ButtonStyle(
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.pressed) ||
              states.contains(MaterialState.hovered) ||
              states.contains(MaterialState.selected) ||
              states.contains(MaterialState.focused))
            return customBlueColor.withOpacity(0.5);
          return customBlueColor; // Use the component's default.
        },
      ),
      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
      shape: MaterialStateProperty.all<OutlinedBorder>(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      ),
      padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.zero),
      textStyle: MaterialStateProperty.all<TextStyle>(
        const TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w700,
          color: Colors.white,
        ),
      ),
      splashFactory: InkRipple.splashFactory,
    );
    //Starting point of the app will be the WelcomeScreen. if user is logged in then the starting screen is MainMenuScreen().
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset(imageAssetBuilder("loadScreen.jpg")).image,
            fit: BoxFit.cover,
          ),
        ),
        child: MaterialApp(
          theme: ThemeData(
            backgroundColor: scaffoldBackgroundColor,
            cardTheme: CardTheme(
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            inputDecorationTheme: InputDecorationTheme(
              hintStyle: GoogleFonts.roboto(
                textStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: customGrayColor1,
                ),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: customGrayColor1,
                ),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blue,
                ),
              ),
            ),
            appBarTheme: AppBarTheme(
              backgroundColor: scaffoldBackgroundColor,
              elevation: 0,
              iconTheme: IconThemeData(
                color: customTextGrayColor1,
                size: 21,
              ),
            ),
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
              elevation: 0,
              backgroundColor: scaffoldBackgroundColor,
            ),
            textTheme: TextTheme(
              headline1: GoogleFonts.roboto(
                textStyle: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                ),
              ),
              headline5: GoogleFonts.roboto(
                textStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),
              ),
              bodyText1: GoogleFonts.roboto(
                textStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),
              ),
              bodyText2: GoogleFonts.lora(
                textStyle: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),
              ),
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(style: customBtnStyle),
            outlinedButtonTheme: OutlinedButtonThemeData(style: customBtnStyle),
            floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: customBlueColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
            scaffoldBackgroundColor: scaffoldBackgroundColor,
          ),
          localizationsDelegates: <LocalizationsDelegate>[
            FlutterI18nDelegate(
              translationLoader:
                  FileTranslationLoader(basePath: "assets/locale"),
              missingTranslationHandler: (key, locale) {
                print(
                    "--- Missing Key: $key, languageCode: ${locale.languageCode}");
              },
            ),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
          builder: FlutterI18n.rootAppBuilder(),
          supportedLocales: <Locale>[const Locale("ru")],
          debugShowCheckedModeBanner: false,
          title: 'Box Packing',
          initialRoute: StartScreen.routeName,
          routes: {
            StartScreen.routeName: (context) => StartScreen(),
            PhoneVerificationScreen.routeName: (context) =>
                PhoneVerificationScreen(),
            LoginScreen.routeName: (context) => LoginScreen(),
            RegistrationScreen.routeName: (context) => RegistrationScreen(),
            AccountTypeSelectionScreen.routeName: (context) =>
                AccountTypeSelectionScreen(),
            PasswordResetAccountSelectionScreen.routeName: (context) =>
                PasswordResetAccountSelectionScreen(),
            PasswordResetScreen.routeName: (context) => PasswordResetScreen(),
            ProductScreen.routeName: (context) => ProductScreen(
                  product: null,
                ),
            MainNavigationScreen.routeName: (context) => MainNavigationScreen(),
            MainPageScreen.routeName: (context) => MainPageScreen(),
          },
          onGenerateRoute: (settings) {
            Widget page;
            if (settings.name == SpecificationScreen.routeName) {
              final args = settings.arguments as Specification;

              page = SpecificationScreen(
                specification: args,
              );
            } else if (settings.name == ThreeDsVerificationScreen.routeName) {
              final args = settings.arguments as ThreeDsVerificationArguments;
              page = ThreeDsVerificationScreen(
                verificationArguments: args,
              );
            } else if (settings.name == CompanyDetailsListScreen.routeName) {
              final args = settings.arguments as Function(int);
              page = CompanyDetailsListScreen(
                onContinueButtonPressed: args,
              );
            } else if (settings.name == CompanyDetailsEditorScreen.routeName) {
              final args =
                  settings.arguments as CompanyDetailsEditorScreenParameters;
              page = CompanyDetailsEditorScreen(params: args);
            } else if (settings.name == CodeVerificationScreen.routeName) {
              final args = settings.arguments as String;
              page = CodeVerificationScreen(verificationId: args);
            } else if (settings.name == SpecificationEditorScreen.routeName) {
              final args =
                  settings.arguments as SpecificationEditorScreenArguments;

              page = SpecificationEditorScreen(
                onCreatePressed: args.onCreatePressed,
                specification: args.specification,
              );
            } else if (settings.name == RecipientRegistrationScreen.routeName) {
              final args = settings.arguments as Order;

              page = RecipientRegistrationScreen(
                order: args,
              );
            } else if (settings.name == PaymentSelectionScreen.routeName) {
              final args = settings.arguments as Order;
              page = PaymentSelectionScreen(
                order: args,
              );
            } else if (settings.name == OrderRegisteredScreen.routeName) {
              final args = settings.arguments as Payment;
              page = OrderRegisteredScreen(payment: args);
            } else if (settings.name == DocumentsScreen.routeName) {
              final args = settings.arguments as List<ImageProvider>;
              page = DocumentsScreen(images: args);
            } else if (settings.name == ChatWithManagerScreen.routeName) {
              page = ChatWithManagerScreen();
            } else if (settings.name == CreditCardEditorScreen.routeName) {
              final args =
                  settings.arguments as CreditCardEditorScreenArguments;
              page = CreditCardEditorScreen(
                arguments: args,
              );
            } else if (settings.name == CreditCardListScreen.routeName) {
              final args = settings.arguments as CreditCardListScreenArguments;
              page = CreditCardListScreen(
                arguments: args,
              );
            }

            return MaterialPageRoute(builder: (_) => page);
          },
        ),
      ),
    );
  }
}
