import 'dart:ui';

import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/data/enums/category_view_options.dart';
import 'package:benefit_flutter/data/enums/main_navigation.dart';
import 'package:benefit_flutter/data/enums/payment_method.dart';
import 'package:benefit_flutter/data/enums/replacement_options.dart';

const Color customBlueColor = Color(0xFF1EB8CE);
const Color customGrayColor1 = Color(0xFFC4C4C4);
const Color customGrayColor2 = Color(0xFFD4D4D4);
const Color customGrayColor3 = Color(0xFFF6F6F6);
const Color customRedColor = Color(0xFFE84625);
const Color customTextGrayColor1 = Color(0xFF909090);
const Color scaffoldBackgroundColor = Color(0xFFE5E5E5);
final Color disabledColor = Color(0xFF000000).withOpacity(0.3);
const Color lightBlack = Color(0xFF333333);
const Color customTextGrayColor2 = Color(0xFF919191);
const Color starColor = Color(0xFFFCD462);
const Color loginPageColor = Color(0xFF001C4C);
const MainNavigation startPage = MainNavigation.MainPage;

const double miniProductWidgetHeight = 130.0;
const double miniProductWidgetWidth = 115.0;
const double specificationWidgetHeight = 85;
const double textAppbarHeight = 66;
const double productListWidgetHeight = 105;
const Duration snackBarDisplayDuration = Duration(seconds: 2);

const int singular = 1;
const int pluralType1 = 0;
const int pluralType2 = 2;

final List<Color> specificationColorList = [
  Color(0xFFFFDF8B),
  Color(0xFFFF8B8B),
  Color(0xFF1EB8CE),
  Color(0xFF8BACFF),
  Color(0xFF8EFF8B),
  Color(0xFFF68BFF),
  Color(0xFFC58BFF),
];

const Color defaultSpecificationColor = Color(0xFF1EB8CE);

const CategoryViewOptions defaultView = CategoryViewOptions.GridView;

const ReplacementOptions defaultOption = ReplacementOptions.DeleteFromOrder;

const PaymentMethod defaultMethod = PaymentMethod.Cash;

const imageAssetsPath = "assets/images/";

String imageAssetBuilder(String name) {
  return "$imageAssetsPath$name";
}

const AccountType defaultType = AccountType.LegalEntity;

const Color accountTypeSelectedColor = Color(0xFFE84625);

const int codeVerificationTimeout = 120;

const String userInfoFileName = "userInfo";

const int defaultMiniStock = 10;
const int defaultStock = 500;
