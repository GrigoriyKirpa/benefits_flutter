import 'package:benefit_flutter/data/models/banner.dart';
import 'package:benefit_flutter/data/models/basket.dart';
import 'package:benefit_flutter/data/models/category.dart';
import 'package:benefit_flutter/data/models/phone.dart';
import 'package:benefit_flutter/data/models/product_color.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/user.dart';
import 'package:benefit_flutter/data/utils/local_storage.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class DepartmentContacts {
  final List<String> email;
  final List<String> phone;

  DepartmentContacts({@required this.email, @required this.phone});
}

class Shop {
  Shop._();
  static bool isBusy = false;
  static UserInfo manager = UserInfo(
    fullName: "Анатолий",
    phone: Phone(
      countryCode: "+7",
      phone: "9069005321",
    ),
    email: "info@vigoda-td.ru",
  );
  static const List<String> shopPhones = [
    "+7 812 389 59 55",
    " 8 800 505 94 35",
  ];
  static List<DepartmentContacts> departments = [
    DepartmentContacts(
      email: ["info@vigoda-td.ru"],
      phone: [],
    ),
    DepartmentContacts(
      email: ["zakupki@vigoda-td.ru"],
      phone: [],
    ),
    DepartmentContacts(
      email: ["a.azovskov@vigoda-td.ru"],
      phone: ["+7 906 900 53 21"],
    ),
  ];
  static const shopEmail = "info@vigoda-td.ru";
  static bool initialized = false;
  static User currentUser = User(
    info: UserInfo(
        // id: 1994,
        // phone: Phone(
        //   countryCode: "+7",
        //   phone: "9312001715",
        // ),
        // email: "polina.tv.92@mail.ru",
        // password: "+7(931)200-17-15",
        ),
    specifications: [],
    likedProducts: [],
  );
  static List<Category> categories;
  static Basket basket;
  static List<ProductColor> colors;
  static List<CustomBanner> banners = [];
  static Category discountedCategory;
  static Category newArrivalCategory;

  static void resetShop() {
    LocalStorage.deleteUserInfoFromLocalStorage();
    initialized = false;
    currentUser = User(
      info: UserInfo(),
      specifications: [],
      likedProducts: [],
    );
    categories = null;
    basket = null;
    colors = null;
    banners = [];
  }

  static Future initializeShop() async {
    try {
      if (!initialized) {
        final List<LikedProductDescriptor> liked =
            await ServerRequests.getLikedProducts();
        currentUser.likedProducts.addAll(liked);
        categories = [];
        basket = Basket();
        basket.fill(await ServerRequests.getBasketItems());
        colors = await ServerRequests.getColorTable();
        currentUser
            .specificationsFromJson(await ServerRequests.getSpecifications());
        currentUser.fillProducts(
          await ServerRequests.getSpecificationProducts(
              currentUser.specificationIds),
        );
        banners.addAll(await ServerRequests.getBanners());
        initialized = true;
      } else {
        await updateServerData();
      }

      return initialized;
    } catch (e) {
      return e;
    }
  }

  static Future<List<ProductDescriptor>> get discountedProducts async {
    return await ServerRequests.getProductList(discounted: true);
  }

  static Future<List<ProductDescriptor>> get newArrivalProducts async {
    return await ServerRequests.getProductList(newcomers: true);
  }

  static ImageProvider getColor(String code) {
    if (colors.any((element) => element.code == code)) {
      return colors.firstWhere((element) => element.code == code).color;
    }
    throw Exception("Color with the code: $code doesn't exist");
  }

  static void saveUserInfo() {
    LocalStorage.saveUserInfoToLocalStorage(currentUser.info);
  }

  static List<CachedNetworkImage> bannersForType(BannerType type) {
    return banners
        .where((element) => element.type == type)
        .map((e) => e.banner)
        .toList();
  }

  static void addSearch(String query) {
    if (currentUser.info.searchHistory == null) {
      currentUser.info.searchHistory = [];
    }
    currentUser.info.searchHistory.remove(query);
    while (currentUser.info.searchHistory.length >= 10) {
      currentUser.info.searchHistory.removeAt(0);
    }
    currentUser.info.searchHistory.add(query);
  }

  static Future<void> updateServerData() async {
    isBusy = true;
    currentUser.likedProducts.clear();
    basket.items.clear();
    currentUser.likedProducts.addAll(await ServerRequests.getLikedProducts());
    basket.items.addAll(await ServerRequests.getBasketItems());
    isBusy = false;
  }
}
