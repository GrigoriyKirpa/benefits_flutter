enum UserCheckStatus {
  Ok,
  ErrorEmail,
  Unknown,
  ErrorPhone,
  ErrorPassword,
}

extension UserCheckStatusExtension on UserCheckStatus {
  static Map<String, UserCheckStatus> _statuses = {
    "ok": UserCheckStatus.Ok,
    "email": UserCheckStatus.ErrorEmail,
    "unknown": UserCheckStatus.Unknown,
    "phone": UserCheckStatus.ErrorPhone,
    "password": UserCheckStatus.ErrorPassword,
  };

  static Map<UserCheckStatus, String> _localeKeys = {
    UserCheckStatus.ErrorEmail: "email",
    UserCheckStatus.Unknown: "unknown",
    UserCheckStatus.ErrorPhone: "phone",
    UserCheckStatus.ErrorPassword: "password",
  };

  static UserCheckStatus stringToStatus(String status) {
    if (_statuses.containsKey(status)) {
      return _statuses[status];
    } else {
      throw UnimplementedError();
    }
  }

  String get errorKey {
    return _localeKeys[this];
  }
}
