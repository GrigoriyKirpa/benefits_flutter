import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum SortBy {
  original,
  expensiveFirst,
  cheaperFirst,
  popularFirst,
  discountLargerFirst,
  goodRatingFirst,
}

extension SortByExtension on SortBy {
  static Map<SortBy, Function(ProductDescriptor, ProductDescriptor)>
      _comparators = {
    SortBy.cheaperFirst: (first, second) =>
        first.price.price.compareTo(second.price.price),
    SortBy.expensiveFirst: (first, second) =>
        -1 * first.price.price.compareTo(second.price.price),
    SortBy.goodRatingFirst: (first, second) =>
        -1 * first.rating.rating.compareTo(second.rating.rating),
    SortBy.popularFirst: (first, second) =>
        -1 * first.rating.voteCount.compareTo(second.rating.voteCount),
    SortBy.discountLargerFirst: (first, second) =>
        -1 *
        (first.price.basePrice - first.price.price)
            .compareTo(second.price.basePrice - second.price.price),
  };

  List<ProductDescriptor> sort(List<ProductDescriptor> products) {
    if (!_comparators.keys.contains(this)) {
      return products;
    }
    final List<ProductDescriptor> sortedProducts = products;
    sortedProducts.sort(_comparators[this]);
    return sortedProducts;
  }

  String get localeKey {
    String localeKey = "default";
    if (this == SortBy.expensiveFirst) {
      localeKey = "expensive_first";
    } else if (this == SortBy.original) {
      localeKey = "original";
    } else if (this == SortBy.cheaperFirst) {
      localeKey = "cheaper_first";
    } else if (this == SortBy.popularFirst) {
      localeKey = "popular_first";
    } else if (this == SortBy.discountLargerFirst) {
      localeKey = "discount_larger_first";
    } else if (this == SortBy.goodRatingFirst) {
      localeKey = "good_rating_first";
    }
    return localeKey;
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(context, "product.sort_by.$localeKey");
  }
}
