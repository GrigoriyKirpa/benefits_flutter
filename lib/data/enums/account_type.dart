import 'package:benefit_flutter/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum AccountType {
  LegalEntity,
  Person,
}

extension AccountTypeExtension on AccountType {
  static Map<AccountType, int> _idMap = {
    AccountType.Person: 1,
    AccountType.LegalEntity: 2,
  };
  static Map<AccountType, String> _localeKeys = {
    AccountType.Person: "person",
    AccountType.LegalEntity: "legal_entity",
  };

  static Map<AccountType, String> _images = {
    AccountType.Person: "package.png",
    AccountType.LegalEntity: "trolley.png",
  };

  int get id {
    return _idMap[this];
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(
        context, "auth.screen.register.account_type.${_localeKeys[this]}");
  }

  ImageProvider get image {
    return Image.asset(imageAssetBuilder(_images[this])).image;
  }
}
