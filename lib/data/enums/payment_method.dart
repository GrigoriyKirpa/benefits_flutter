import 'dart:io';

import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum PaymentMethod {
  Cash,
  ApplePay,
  Transfer,
  OnlineBanking,
  Contract,
}

extension PaymentMethodExtension on PaymentMethod {
  static const Map<PaymentMethod, int> _idMap = {
    PaymentMethod.ApplePay: 9,
    PaymentMethod.Cash: 1,
    PaymentMethod.Transfer: 9,
    PaymentMethod.OnlineBanking: 9,
    PaymentMethod.Contract: 9,
  };
  static const Map<PaymentMethod, Color> _colorMap = {
    PaymentMethod.ApplePay: Colors.black,
    PaymentMethod.Cash: customBlueColor,
    PaymentMethod.Transfer: Color(0xFFE84625),
    PaymentMethod.OnlineBanking: Color(0xFFFCD462),
    PaymentMethod.Contract: Color(0xFF8875FF),
  };

  static const Map<PaymentMethod, IconData> _iconsMap = {
    PaymentMethod.ApplePay: CustomIcons.apple,
    PaymentMethod.Cash: CustomIcons.ruble,
    PaymentMethod.Transfer: CustomIcons.bank,
    PaymentMethod.OnlineBanking: CustomIcons.credit_card,
    PaymentMethod.Contract: CustomIcons.invoice,
  };

  String get localeKey {
    String localeKey = "default";

    if (this == PaymentMethod.ApplePay) {
      localeKey = "apple_pay";
    } else if (this == PaymentMethod.Cash) {
      localeKey = "cash";
    } else if (this == PaymentMethod.Contract) {
      localeKey = "contract";
    } else if (this == PaymentMethod.OnlineBanking) {
      localeKey = "online_banking";
    } else if (this == PaymentMethod.Transfer) {
      localeKey = "transfer";
    }

    return localeKey;
  }

  static List<PaymentMethod> availableMethods(
      [AccountType type = AccountType.Person]) {
    final List<PaymentMethod> methods = [];
    methods.add(PaymentMethod.Cash);
    methods.add(PaymentMethod.OnlineBanking);
    if (Platform.isIOS) {
      //methods.add(PaymentMethod.ApplePay);
    }
    if (type == AccountType.LegalEntity) {
      methods.add(PaymentMethod.Transfer);
    }
    return methods;
  }

  String title(BuildContext context) {
    return FlutterI18n.translate(
        context, "order.payment.method.$localeKey.title");
  }

  int get id {
    return _idMap[this];
  }

  String description(BuildContext context) {
    return FlutterI18n.translate(
        context, "order.payment.method.$localeKey.description");
  }

  Color get color {
    return _colorMap[this];
  }

  IconData get icon {
    return _iconsMap[this];
  }
}
