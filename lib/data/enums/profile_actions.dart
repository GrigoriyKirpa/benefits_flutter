import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum ProfileActions {
  Messenger,
  QuestionAndAnswer,
  AddFiles,
  CompanyDetails,
  DeliveryTermsAndConditions,
  CompanyContacts,
  DeliveryAddress,
  CatalogType,
}

extension ProfileActionExtension on ProfileActions {
  static const Map<ProfileActions, IconData> _iconMap = {
    ProfileActions.Messenger: CustomIcons.messenger,
    ProfileActions.QuestionAndAnswer: CustomIcons.text_message,
    ProfileActions.AddFiles: CustomIcons.paper_clip,
    ProfileActions.CompanyDetails: CustomIcons.letter_with_pen,
    ProfileActions.DeliveryTermsAndConditions: CustomIcons.letter,
    ProfileActions.CompanyContacts: CustomIcons.headset,
    ProfileActions.DeliveryAddress: CustomIcons.internet,
    ProfileActions.CatalogType: CustomIcons.notepad,
  };

  String get localeKey {
    String localeKey = '';

    if (this == ProfileActions.Messenger) {
      localeKey = "messenger";
    } else if (this == ProfileActions.QuestionAndAnswer) {
      localeKey = "q&a";
    } else if (this == ProfileActions.AddFiles) {
      localeKey = "add_files";
    } else if (this == ProfileActions.CompanyDetails) {
      localeKey = "company_docs";
    } else if (this == ProfileActions.DeliveryTermsAndConditions) {
      localeKey = "delivery_terms_and_conditions";
    } else if (this == ProfileActions.CompanyContacts) {
      localeKey = "company_contacts";
    } else if (this == ProfileActions.DeliveryAddress) {
      localeKey = "delivery_address";
    } else if (this == ProfileActions.CatalogType) {
      localeKey = "catalog_type";
    }

    return localeKey;
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(context, "user.profile.actions.$localeKey");
  }

  IconData get icon {
    return _iconMap[this];
  }

  Color get iconColor {
    Color color = customGrayColor1;
    if (this == ProfileActions.Messenger) {
      color = customRedColor;
    }

    return color;
  }

  Color get textColor {
    Color color = Colors.black;
    if (this == ProfileActions.Messenger) {
      color = customRedColor;
    }

    return color;
  }
}
