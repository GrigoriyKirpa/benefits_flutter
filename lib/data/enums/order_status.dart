import 'package:benefit_flutter/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum OrderStatus {
  InProcess,
  Confirmed,
  Packed,
  OnTheWay,
  Delivered,
  Canceled,
}

extension OrderStatusExtension on OrderStatus {
  static Map<OrderStatus, Color> _statusLabelColorMap = {
    OrderStatus.InProcess: customRedColor,
    OrderStatus.Confirmed: customRedColor,
    OrderStatus.Packed: customRedColor,
    OrderStatus.OnTheWay: customRedColor,
    OrderStatus.Delivered: customBlueColor,
    OrderStatus.Canceled: customTextGrayColor1,
  };
  static Map<OrderStatus, Color> _actionLabelColorMap = {
    OrderStatus.InProcess: customRedColor,
    OrderStatus.Confirmed: customRedColor,
    OrderStatus.Packed: customRedColor,
    OrderStatus.OnTheWay: customRedColor,
    OrderStatus.Delivered: customTextGrayColor1,
    OrderStatus.Canceled: customTextGrayColor1,
  };

  static OrderStatus statusFromCode(String code, {canceled = false}) {
    OrderStatus status;
    if (canceled) {
      status = OrderStatus.Canceled;
    } else if (code == "DN") {
      status = OrderStatus.InProcess;
    } else if (code == "DA" || code == "DG") {
      status = OrderStatus.Confirmed;
    } else if (code == "DT") {
      status = OrderStatus.Packed;
    } else if (code == "DS") {
      status = OrderStatus.OnTheWay;
    } else if (code == "DF") {
      status = OrderStatus.Delivered;
    }
    return status;
  }

  String get localeKey {
    String localeKey = 'default';

    if (this == OrderStatus.InProcess) {
      localeKey = "in_process";
    } else if (this == OrderStatus.Delivered) {
      localeKey = "confirmed";
    } else if (this == OrderStatus.Delivered) {
      localeKey = "packed";
    } else if (this == OrderStatus.Delivered) {
      localeKey = "on_the_way";
    } else if (this == OrderStatus.Delivered) {
      localeKey = "delivered";
    } else if (this == OrderStatus.Canceled) {
      localeKey = "canceled";
    }

    return localeKey;
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(context, "order.status.$localeKey");
  }

  String actionButtonLabel(BuildContext context) {
    return FlutterI18n.translate(context, "order.action_button.$localeKey");
  }

  Color get actionButtonLabelColor {
    if (!_actionLabelColorMap.keys.contains(this)) {
      return Colors.black;
    }

    return _actionLabelColorMap[this];
  }

  Color get statusLabelColor {
    if (!_statusLabelColorMap.keys.contains(this)) {
      return Colors.black;
    }

    return _statusLabelColorMap[this];
  }
}
