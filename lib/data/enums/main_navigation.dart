import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/category.dart';
import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/question_and_answer.dart';
import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/address_book_screen.dart';
import 'package:benefit_flutter/presentation/screens/address_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/catalog_screen.dart';
import 'package:benefit_flutter/presentation/screens/category_screen.dart';
import 'package:benefit_flutter/presentation/screens/company_details_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/company_details_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/contacts_screen.dart';
import 'package:benefit_flutter/presentation/screens/delivery_and_payment_terms_screen.dart';
import 'package:benefit_flutter/presentation/screens/faq_screen.dart';
import 'package:benefit_flutter/presentation/screens/favourites_screen.dart';
import 'package:benefit_flutter/presentation/screens/main_page_screen.dart';
import 'package:benefit_flutter/presentation/screens/my_basket_screen.dart';
import 'package:benefit_flutter/presentation/screens/order_history_screen.dart';
import 'package:benefit_flutter/presentation/screens/order_page_screen.dart';
import 'package:benefit_flutter/presentation/screens/price_complaint_screen.dart';
import 'package:benefit_flutter/presentation/screens/product_screen.dart';
import 'package:benefit_flutter/presentation/screens/profile_page_screen.dart';
import 'package:benefit_flutter/presentation/screens/question_and_answer_editor.dart';
import 'package:benefit_flutter/presentation/screens/question_and_answer_screen.dart';
import 'package:benefit_flutter/presentation/screens/review_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/review_screen.dart';
import 'package:benefit_flutter/presentation/screens/search_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_selector_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

enum MainNavigation {
  MainPage,
  LikedProductsPage,
  BasketPage,
  HistoryPage,
  ProfilePage,
}

extension MainNavigationExtension on MainNavigation {
  static MainNavigation currentTab;
  static final mainPageKey = GlobalKey<NavigatorState>();
  static final favouritesPageKey = GlobalKey<NavigatorState>();
  static final historyPageKey = GlobalKey<NavigatorState>();
  static final basketPageKey = GlobalKey<NavigatorState>();
  static final profilePageKey = GlobalKey<NavigatorState>();
  Widget get icon {
    Widget icon;

    if (this == MainNavigation.MainPage) {
      icon = Icon(CustomIcons.catalog);
    } else if (this == MainNavigation.BasketPage) {
      icon = Icon(CustomIcons.basket);
    } else if (this == MainNavigation.HistoryPage) {
      icon = Icon(CustomIcons.history);
    } else if (this == MainNavigation.ProfilePage) {
      icon = Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          border: Border.all(color: customGrayColor1),
        ),
        child: Image.asset(
          imageAssetBuilder("profile.png"),
          fit: BoxFit.fill,
        ),
      );
    } else if (this == MainNavigation.LikedProductsPage) {
      icon = Icon(CustomIcons.like);
    } else {
      throw UnimplementedError('MainNavigation get icon');
    }

    return icon;
  }

  int get index {
    int index;
    if (this == MainNavigation.MainPage) {
      index = 0;
    } else if (this == MainNavigation.BasketPage) {
      index = 2;
    } else if (this == MainNavigation.HistoryPage) {
      index = 3;
    } else if (this == MainNavigation.ProfilePage) {
      index = 4;
    } else if (this == MainNavigation.LikedProductsPage) {
      index = 1;
    } else {
      throw UnimplementedError('MainNavigation get index');
    }

    return index;
  }

  GlobalKey<NavigatorState> get navigationKey {
    if (this == MainNavigation.MainPage) {
      return mainPageKey;
    } else if (this == MainNavigation.LikedProductsPage) {
      return favouritesPageKey;
    } else if (this == MainNavigation.HistoryPage) {
      return historyPageKey;
    } else if (this == MainNavigation.BasketPage) {
      return basketPageKey;
    } else if (this == MainNavigation.ProfilePage) {
      return profilePageKey;
    } else {
      throw UnimplementedError();
    }
  }

  Widget page(BuildContext context) {
    String initRoute = "/";
    Widget _page;
    if (this == MainNavigation.MainPage) {
      currentTab = MainNavigation.MainPage;

      _page = Navigator(
        key: navigationKey,
        initialRoute: initRoute,
        onGenerateRoute: (settings) {
          Widget page;
          if (settings.name == initRoute) {
            page = MainPageScreen();
          } else if (settings.name == SearchScreen.routeName) {
            page = SearchScreen();
          } else if (settings.name == CatalogScreen.routeName) {
            final args = settings.arguments as CategoryDescriptor;
            page = CatalogScreen(
              currentCategory: args,
            );
          } else if (settings.name == CategoryScreen.routeName) {
            final args = settings.arguments as Category;

            page = CategoryScreen(category: args);
          } else if (settings.name == ProductScreen.routeName) {
            final args = settings.arguments as ProductDescriptor;

            page = ProductScreen(product: args);
          } else if (settings.name == SpecificationSelectorScreen.routeName) {
            final args = settings.arguments as Function;

            page = SpecificationSelectorScreen(
              onSpecificationChosen: args,
            );
          } else if (settings.name == ReviewScreen.routeName) {
            final args = settings.arguments as List<Review>;

            page = ReviewScreen(
              reviews: args,
            );
          } else if (settings.name == ReviewEditorScreen.routeName) {
            final args = settings.arguments as Function;

            page = ReviewEditorScreen(
              onReviewCreated: args,
            );
          } else if (settings.name == SpecificationListScreen.routeName) {
            page = SpecificationListScreen();
          } else if (settings.name == PriceComplaintScreen.routeName) {
            final args = settings.arguments as Function;

            page = PriceComplaintScreen(
              onSentPressed: args,
            );
          } else if (settings.name == QuestionAndAnswerScreen.routeName) {
            final args = settings.arguments as List<QuestionAndAnswer>;

            page = QuestionAndAnswerScreen(
              questionAndAnswers: args,
            );
          } else if (settings.name == QuestionAndAnswerEditor.routeName) {
            final args = settings.arguments as Function;

            page = QuestionAndAnswerEditor(
              onSentPressed: args,
            );
          } else {
            throw UnimplementedError();
          }
          return MaterialPageRoute(builder: (_) => page);
        },
      );
    } else if (this == MainNavigation.BasketPage) {
      currentTab = MainNavigation.BasketPage;
      _page = Navigator(
        key: navigationKey,
        initialRoute: initRoute,
        onGenerateRoute: (settings) {
          Widget page;
          if (settings.name == initRoute) {
            page = MyBasketScreen();
          } else if (settings.name == AddressBookScreen.routeName) {
            page = AddressBookScreen();
          } else if (settings.name == AddressEditorScreen.routeName) {
            final args = settings.arguments as AddressEditorScreenParameters;
            page = AddressEditorScreen(params: args);
          } else if (settings.name == ProductScreen.routeName) {
            final args = settings.arguments as ProductDescriptor;

            page = ProductScreen(product: args);
          } else if (settings.name == ReviewScreen.routeName) {
            final args = settings.arguments as List<Review>;

            page = ReviewScreen(
              reviews: args,
            );
          } else if (settings.name == ReviewEditorScreen.routeName) {
            final args = settings.arguments as Function;

            page = ReviewEditorScreen(
              onReviewCreated: args,
            );
          } else {
            throw UnimplementedError();
          }

          return MaterialPageRoute(builder: (_) => page);
        },
      );
    } else if (this == MainNavigation.HistoryPage) {
      currentTab = MainNavigation.HistoryPage;
      _page = Navigator(
        key: navigationKey,
        initialRoute: initRoute,
        onGenerateRoute: (settings) {
          Widget page;
          if (settings.name == initRoute) {
            page = OrderHistoryScreen();
          } else if (settings.name == OrderPageScreen.routeName) {
            final args = settings.arguments as Order;

            page = OrderPageScreen(order: args);
          } else if (settings.name == ProductScreen.routeName) {
            final args = settings.arguments as ProductDescriptor;

            page = ProductScreen(product: args);
          } else if (settings.name == ReviewScreen.routeName) {
            final args = settings.arguments as List<Review>;

            page = ReviewScreen(
              reviews: args,
            );
          } else if (settings.name == ReviewEditorScreen.routeName) {
            final args = settings.arguments as Function;

            page = ReviewEditorScreen(
              onReviewCreated: args,
            );
          } else {
            throw UnimplementedError();
          }

          return MaterialPageRoute(builder: (_) => page);
        },
      );
    } else if (this == MainNavigation.ProfilePage) {
      currentTab = MainNavigation.ProfilePage;
      _page = Navigator(
        key: navigationKey,
        initialRoute: initRoute,
        onGenerateRoute: (settings) {
          Widget page;
          if (settings.name == initRoute) {
            page = ProfilePageScreen();
          } else if (settings.name == AddressBookScreen.routeName) {
            page = AddressBookScreen();
          } else if (settings.name == AddressEditorScreen.routeName) {
            final args = settings.arguments as AddressEditorScreenParameters;
            page = AddressEditorScreen(params: args);
          } else if (settings.name == DeliveryAndPaymentTermsScreen.routeName) {
            page = DeliveryAndPaymentTermsScreen();
          } else if (settings.name == FAQScreen.routeName) {
            page = FAQScreen();
          } else if (settings.name == ContactsScreen.routeName) {
            page = ContactsScreen();
          } else if (settings.name == CompanyDetailsListScreen.routeName) {
            final args = settings.arguments as Function(int);
            page = CompanyDetailsListScreen(
              onContinueButtonPressed: args,
            );
          } else if (settings.name == CompanyDetailsEditorScreen.routeName) {
            final args =
                settings.arguments as CompanyDetailsEditorScreenParameters;
            page = CompanyDetailsEditorScreen(params: args);
          } else {
            throw UnimplementedError();
          }

          return MaterialPageRoute(builder: (_) => page);
        },
      );
    } else if (this == MainNavigation.LikedProductsPage) {
      currentTab = MainNavigation.LikedProductsPage;
      _page = Navigator(
        key: navigationKey,
        initialRoute: initRoute,
        onGenerateRoute: (settings) {
          Widget page;
          if (settings.name == initRoute) {
            page = FavouritesScreen();
          } else if (settings.name == ProductScreen.routeName) {
            final args = settings.arguments as ProductDescriptor;

            page = ProductScreen(product: args);
          } else if (settings.name == SpecificationSelectorScreen.routeName) {
            final args = settings.arguments as Function;

            page = SpecificationSelectorScreen(
              onSpecificationChosen: args,
            );
          } else if (settings.name == ReviewScreen.routeName) {
            final args = settings.arguments as List<Review>;

            page = ReviewScreen(
              reviews: args,
            );
          } else if (settings.name == ReviewEditorScreen.routeName) {
            final args = settings.arguments as Function;

            page = ReviewEditorScreen(
              onReviewCreated: args,
            );
          } else {
            throw UnimplementedError();
          }

          return MaterialPageRoute(builder: (_) => page);
        },
      );
    } else {
      throw UnimplementedError('MainNavigation get index');
    }

    return _page;
  }

  static MainNavigation indexToPage(int index) {
    for (MainNavigation element in MainNavigation.values) {
      if (index == element.index) {
        return element;
      }
    }
    throw UnimplementedError('MainNavigation indexToPage');
  }
}
