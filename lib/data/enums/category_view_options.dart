import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum CategoryViewOptions {
  GridView,
  ListView,
}

extension CategoryViewExtension on CategoryViewOptions {
  IconData get icon {
    IconData icon;
    if (this == CategoryViewOptions.GridView) {
      icon = CustomIcons.block_view;
    } else if (this == CategoryViewOptions.ListView) {
      icon = CustomIcons.list_view;
    } else {
      throw UnimplementedError('Category View');
    }

    return icon;
  }
}
