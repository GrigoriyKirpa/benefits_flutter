import 'package:flutter/cupertino.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum ReplacementOptions {
  DeleteFromOrder,
  ReplaceInOrder,
  Call,
}

extension ReplacementOptionsExtension on ReplacementOptions {
  String get localeKey {
    String localeKey = 'default';
    if (this == ReplacementOptions.DeleteFromOrder) {
      localeKey = "delete_from_order";
    } else if (this == ReplacementOptions.ReplaceInOrder) {
      localeKey = "replace";
    } else if (this == ReplacementOptions.Call) {
      localeKey = "call";
    }
    return localeKey;
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(
        context, "order.registration.page.replacement.options.$localeKey");
  }
}
