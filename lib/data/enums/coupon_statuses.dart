import 'package:flutter/cupertino.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum CouponStatuses {
  Ok,
  NotFound,
  DiscountFinished,
}

extension CouponStatusExtension on CouponStatuses {
  static Map<CouponStatuses, String> _localeKey = {
    CouponStatuses.NotFound: "not_found",
    CouponStatuses.Ok: "ok",
    CouponStatuses.DiscountFinished: "discount_finished",
  };

  static Map<String, CouponStatuses> _errors = {
    "not_found": CouponStatuses.NotFound,
    "discount_finished": CouponStatuses.DiscountFinished,
  };

  String label(BuildContext context) {
    return FlutterI18n.translate(
        context, "order.promo_code.status.${_localeKey[this]}");
  }

  static CouponStatuses errorToCouponStatus(String error) {
    if (!_errors.containsKey(error)) {
      throw Exception("Unsupported server error");
    }
    return _errors[error];
  }
}
