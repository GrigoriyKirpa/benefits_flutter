import 'dart:convert';
import 'dart:core';

import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/data/enums/coupon_statuses.dart';
import 'package:benefit_flutter/data/enums/order_status.dart';
import 'package:benefit_flutter/data/enums/payment_method.dart';
import 'package:benefit_flutter/data/enums/replacement_options.dart';
import 'package:benefit_flutter/data/enums/user_check_status.dart';
import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/data/models/banner.dart';
import 'package:benefit_flutter/data/models/card_token.dart';
import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:benefit_flutter/data/models/company_details.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/payment.dart';
import 'package:benefit_flutter/data/models/phone.dart';
import 'package:benefit_flutter/data/models/price.dart';
import 'package:benefit_flutter/data/models/product.dart';
import 'package:benefit_flutter/data/models/product_color.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/product_feedback.dart';
import 'package:benefit_flutter/data/models/promo_code.dart';
import 'package:benefit_flutter/data/models/rating.dart';
import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/data/models/user.dart' as user;
import 'package:benefit_flutter/data/utils/online_requests_handler.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:intl/intl.dart';

enum Repositories {
  Catalog,
  Category,
  Product,
  Order,
  Favourite,
  User,
  Basket,
  ColorTable,
  Specification,
  Banners,
  Coupon,
  Card,
}

extension RepositoryExtension on Repositories {
  static const Map<Repositories, String> _repositories = {
    Repositories.Catalog: "catalog",
    Repositories.Category: "categories",
    Repositories.Product: "product",
    Repositories.Order: "orders",
    Repositories.Favourite: "favs",
    Repositories.User: "mobile_user",
    Repositories.Basket: "basket",
    Repositories.ColorTable: "colorTable",
    Repositories.Specification: "specification",
    Repositories.Banners: "banners",
    Repositories.Coupon: "coupon",
    Repositories.Card: "card",
  };

  String get repository {
    return _repositories[this];
  }
}

class ServerRequests {
  ServerRequests._();

  static const String _baseUrl = "https://vigoda-td.ru";
  static const String _apiUrl = "$_baseUrl/api";
  static String token = "bbae261d-00f9cb8c-2c1b3c3d-c887cce5";
  static FirebaseAuth auth = FirebaseAuth.instance;
  static final Map<String, String> _header = {};

  static final Map<String, String> _headerWithSecurity = {
    "Authorization-Token": token,
  };

  static Future<user.UserInfo> login(
      String newEmail, String newPassword, Phone phone) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.User.repository}/token",
          header: _header,
          body: {
            "login": newEmail,
            "password": newPassword,
            "phone": phone.nonFormatted,
          },
        ),
      );
      token = body["token"];
      final user.UserInfo info = _handleUserInfo(body);
      info.email = newEmail;
      info.password = newPassword;
      info.phone = phone;
      return info;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return null;
  }

  static Future<UserCheckStatus> register(String newEmail, String newPassword,
      Phone phone, Function(user.UserInfo) onStatusOkayCallback) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.User.repository}/register",
          header: _header,
          body: {
            "login": newEmail,
            "email": newEmail,
            "password": newPassword,
            "uf_phone": phone.nonFormatted,
          },
        ),
      );
      if (body["error"] == null) {
        token = body["token"];
        final user.UserInfo info = _handleUserInfo(body);
        info.email = newEmail;
        info.password = newPassword;
        info.phone = phone;
        onStatusOkayCallback(info);
        return UserCheckStatus.Ok;
      } else if (body["error"] != null) {
        return UserCheckStatusExtension.stringToStatus(body["error"]);
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return UserCheckStatus.Unknown;
  }

  static user.UserInfo _handleUserInfo(Map body) {
    final List addressBook =
        body["address_book"] == null || (body["address_book"] as String).isEmpty
            ? []
            : jsonDecode(body["address_book"]);
    final List companyDetails = body["company_details"] == null ||
            (body["company_details"] as String).isEmpty
        ? []
        : jsonDecode(body["company_details"]);
    return user.UserInfo(
      id: int.tryParse(body["id"]),
      fullName: body["name"],
      type: body["type"] == null
          ? body["type"]
          : AccountType.values[int.tryParse(body["type"])],
      addressBook: addressBook
          .map((address) => Address.fromJson(address))
          .toList()
          .cast<Address>(),
      currentAddress: body["address"] == null
          ? body["address"]
          : int.tryParse(body["address"]),
      companyDetails: companyDetails
          .map((element) => CompanyDetails.fromJson(element))
          .toList()
          .cast<CompanyDetails>(),
      ratingMap:
          body["rating_map"] == null || (body["rating_map"] as String).isEmpty
              ? {}
              : (jsonDecode(body["rating_map"]) as Map).map(
                  (key, value) => MapEntry(
                    int.tryParse(key ?? '0') ?? 0,
                    int.tryParse(value ?? '0') ?? 0,
                  ),
                ),
    );
  }

  static Future<void> updateCardTokens(List<CardToken> cardTokens) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Card.repository}/update",
        header: _headerWithSecurity,
        body: {
          "tokens": jsonEncode(
              cardTokens.map((cardToken) => cardToken.toJson()).toList()),
        },
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<List<CardToken>> getCardTokens() async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.Card.repository}",
          header: _headerWithSecurity,
        ),
      );
      if (body["tokens"] == null || body["tokens"].isEmpty) {
        return [];
      } else {
        return (jsonDecode(body["tokens"]) as List)
            .map((element) => CardToken.fromJson(element))
            .toList()
            .cast<CardToken>();
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return [];
  }

  static Future<bool> updateUserInfo(user.UserInfo info) async {
    try {
      final String body = await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.User.repository}/update",
        header: _headerWithSecurity,
        body: {
          "id": info.id.toString(),
          "type": info.type.index.toString(),
          "address": (info.currentAddress ?? -1).toString(),
          "address_book": jsonEncode(
              info.addressBook.map((address) => address.toJson()).toList()),
          "company_details": jsonEncode(
              info.companyDetails.map((element) => element.toJson()).toList()),
          "rating_map": jsonEncode(
            info.ratingMap.map(
              (key, value) => MapEntry(key.toString(), value.toString()),
            ),
          ),
        },
      );
      if (body.isEmpty) {
        return true;
      } else {
        print(jsonDecode(body)["error"]);
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return false;
  }

  static Future<UserCheckStatus> checkUser(String newEmail, String phone,
      [Function(String) onIdFoundCallback]) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.User.repository}/check",
          header: _header,
          body: {
            "email": newEmail,
            "phone": phone,
          },
        ),
      );
      if (body["id"] != null) {
        if (onIdFoundCallback != null) {
          onIdFoundCallback(body["id"]);
        }
        return UserCheckStatus.Ok;
      } else if (body["error"] != null) {
        return UserCheckStatusExtension.stringToStatus(body["error"]);
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return UserCheckStatus.Unknown;
  }

  static Future<String> changePassword(int id, String newPassword) async {
    try {
      final String body = await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.User.repository}/password",
        header: _header,
        body: {
          "id": id.toString(),
          "password": newPassword,
        },
      );
      if (body.isEmpty) {
        return null;
      } else {
        return json.decode(body)["error"];
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      return "Неизвестная ошибка сервера";
    } catch (e) {
      print(e);
      return "Ошибка приложения";
    }
  }

  static Future<dynamic> checkCoupon(String code) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.Coupon.repository}/check?code=$code",
          header: _headerWithSecurity,
        ),
      );
      if (body["error"] == null) {
        return PromoCode(code: code, name: body["name"]);
      } else {
        return CouponStatusExtension.errorToCouponStatus(body["error"]);
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<bool> cancelOrder(int id) async {
    try {
      final String body = await OnlineRequestsHandler.get(
        url: "$_apiUrl/${Repositories.Order.repository}/cancel?id=$id",
        header: _headerWithSecurity,
      );
      if (body.isEmpty) return true;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return false;
  }

  static Future<void> repeatOrder(int id) async {
    try {
      await OnlineRequestsHandler.get(
        url: "$_apiUrl/${Repositories.Order.repository}/repeat?id=$id",
        header: _headerWithSecurity,
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return;
  }

  static Future<List<Order>> getOrders() async {
    try {
      final List body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.Order.repository}/get",
          header: _headerWithSecurity,
        ),
      );
      if (body.isEmpty) return [];
      final List<Order> orders = [];
      final DateFormat format = DateFormat("dd.MM.yyyy hh:mm:ss");
      body.forEach((element) {
        final Order order = Order(
          id: int.tryParse(element["id"] ?? "1") ?? 1,
          status: OrderStatusExtension.statusFromCode(element["status"],
              canceled: element["canceled"] == "Y"),
          date: format.parse(element["date"]),
          payment: Payment(
            paid: element["payed"] == "Y",
            price: Price(
              basePrice: 1.0 * element["total_price"],
            ),
          ),
          items: (element["basket"] as List)
              .map(
                (value) => OrderItem(
                  id: int.tryParse(value["id"] ?? "1") ?? 1,
                  product: _handleProductDescriptor(value["descriptor"] as Map),
                  amount: value["amount"],
                  price: Price(
                    basePrice: double.tryParse(value["price"]) ?? 0.0,
                  ),
                  selectedSize: value["size"],
                  selectedColor: value["color"],
                  variationId: int.tryParse(value['var_id'] ?? ""),
                ),
              )
              .toList()
              .cast<OrderItem>(),
        );
        orders.add(order);
      });
      return orders;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<void> payOrder(Order order) async {
    try {
      await OnlineRequestsHandler.get(
        url: "$_apiUrl/${Repositories.Order.repository}/pay?id=${order.id}",
        header: _headerWithSecurity,
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<Order> createOrder(
      AccountType type, Order order, BuildContext context,
      [CompanyDetails companyDetails]) async {
    Map fuserDetails = {
      "payment_id": order.payment.method.id,
      "email": order.recipient.email,
      "name": order.recipient.name,
      "phone": order.recipient.phone.nonFormatted,
      "pick_up": order.recipient.pickUp,
      "comment": order.recipient.comment,
      "replacement": FlutterI18n.translate(
              context, "order.registration.page.replacement.option_title") +
          order.recipient.option.label(context),
    };
    if (companyDetails != null) {
      fuserDetails["inn"] = companyDetails.inn;
      fuserDetails["kpp"] = companyDetails.kpp;
      fuserDetails["company_name"] = companyDetails.name;
    }
    if (order.recipient.address == null) {
      fuserDetails["city"] = "";
      fuserDetails["address"] = "";
      fuserDetails["delivery_comment"] = "";
    } else {
      fuserDetails["city"] = order.recipient.address.city;
      fuserDetails["address"] = order.recipient.address.buildAddress(context);
      fuserDetails["delivery_comment"] = order.recipient.address.extraInfo;
    }
    var postBody = {
      "fuser_type": type.id.toString(),
      "fuser_details": jsonEncode(fuserDetails),
    };
    if (order.promoCode != null) {
      postBody["coupon"] = order.promoCode.code;
    }
    try {
      var body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.Order.repository}/create",
          header: _headerWithSecurity,
          body: postBody,
        ),
      );
      if (body["error"] != null) {
        throw Exception(body["error"]);
      } else {
        final DateFormat format = DateFormat("dd.MM.yyyy hh:mm:ss");
        order.id = body["id"];
        order.status = OrderStatusExtension.statusFromCode(body["status"]);
        order.date = format.parse(body["date"]);
        return order;
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<void> updateSpecifications(String specificationJson) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Specification.repository}/update",
        header: _headerWithSecurity,
        body: {
          "specs": specificationJson,
        },
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<Map> getSpecifications() async {
    try {
      final String body = await OnlineRequestsHandler.get(
        url: "$_apiUrl/${Repositories.Specification.repository}",
        header: _headerWithSecurity,
      );
      if (body == null || jsonDecode(body).isEmpty) return {};
      return jsonDecode(jsonDecode(body));
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<List<ProductColor>> getColorTable() async {
    try {
      final List body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.ColorTable.repository}",
          header: _headerWithSecurity,
        ),
      );
      final List<ProductColor> colors = [];
      body.forEach(
        (value) {
          final ProductColor color = ProductColor(
            name: value["name"],
            color: Image.network("$_baseUrl${value["image"]}").image,
            code: value["code"],
          );
          colors.add(color);
        },
      );
      return colors;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<List<CustomBanner>> getBanners() async {
    try {
      final List body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.Banners.repository}",
          header: _headerWithSecurity,
        ),
      );
      final List<CustomBanner> banners = [];
      body.forEach((value) {
        final CustomBanner banner = CustomBanner(
          type:
              BannerTypeExtension.intToBannerType(int.tryParse(value["type"])),
          banner: CachedNetworkImage(imageUrl: "$_baseUrl${value["image"]}"),
        );
        banners.add(banner);
      });
      return banners;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<List<CategoryDescriptor>> getCategories(
      [int categoryId = 0]) async {
    try {
      final List body = jsonDecode(
        await OnlineRequestsHandler.get(
          url:
              "$_apiUrl/${Repositories.Category.repository}${categoryId > 0 ? '?section_id=$categoryId' : ''}",
          header: _headerWithSecurity,
        ),
      );
      final List<CategoryDescriptor> categories = [];
      body.forEach((value) {
        final CategoryDescriptor categoryDescriptor = CategoryDescriptor(
          id: int.tryParse(value["id"]),
          name: HtmlUnescape().convert(value["name"] ?? ""),
          productCount: int.tryParse(value["product_count"] ?? "0") ?? 0,
          subCategoryCount: value["sub_category_count"],
          image: value["pic"] == null ? null : "$_baseUrl${value["pic"]}",
        );
        categories.add(categoryDescriptor);
      });
      return categories;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<int> like(ProductDescriptor descriptor) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Favourite.repository}/add",
        header: _headerWithSecurity,
        body: {
          "id": "${descriptor.id.toString()}",
          "price": "${descriptor.price.price.toString()}",
          "quantity": "${descriptor.minAmount.toString()}",
        },
      );
    } catch (e) {
      print(e);
    }
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.get(
          url:
              "$_apiUrl/${Repositories.Basket.repository}/id?id=${descriptor.id.toString()}",
          header: _headerWithSecurity,
        ),
      );
      if (body == null || body["id"] == null) {
        return null;
      }
      return int.tryParse(body["id"]);
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return null;
  }

  static Future<List<ProductDescriptor>> getSpecificationProducts(
      List<int> ids) async {
    if (ids == null || ids.isEmpty) {
      return [];
    }
    try {
      final List body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.Catalog.repository}",
          header: _headerWithSecurity,
          body: {
            "products": ids.map((e) => e.toString()).toList().toString(),
          },
        ),
      );
      final List<ProductDescriptor> products = [];
      body.forEach((element) {
        products.add(_handleProductDescriptor(element));
      });
      return products;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<void> dislike(ProductDescriptor descriptor) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Basket.repository}/remove",
        header: _headerWithSecurity,
        body: {
          "id": "${descriptor.id.toString()}",
        },
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<OrderItem> favToBasket(
      LikedProductDescriptor descriptor) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.Basket.repository}/move",
          header: _headerWithSecurity,
          body: {
            "id": "${descriptor.id.toString()}",
            "to_basket": true.toString(),
          },
        ),
      );
      return OrderItem(
        id: int.tryParse(body["id"]) ?? 1,
        product: descriptor.product,
        amount: body["amount"],
        price: Price(
          basePrice: double.tryParse(body["price"]) ?? 0.0,
        ),
        selectedSize: body["size"],
        selectedColor: body["color"],
        variationId: int.tryParse(body['var_id'] ?? ""),
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return null;
  }

  static Future<void> basketToFav(OrderItem item) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Basket.repository}/move",
        header: _headerWithSecurity,
        body: {
          "id": "${item.id.toString()}",
          "to_basket": false.toString(),
        },
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<Price> getBasketPrice([String code]) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.get(
          url:
              "$_apiUrl/${Repositories.Basket.repository}/price${code == null ? "" : "?code=$code"}",
          header: _headerWithSecurity,
        ),
      );
      return Price(
        basePrice: 0.0 + body["base_price"],
        discountedPrice:
            body["price"] == body["base_price"] || body["price"] == null
                ? null
                : 0.0 + body["price"],
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return null;
  }

  static Future<List<OrderItem>> getBasketItems() async {
    try {
      final List body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.Basket.repository}",
          header: _headerWithSecurity,
        ),
      );
      final List<OrderItem> items = [];
      body.forEach((value) {
        final OrderItem item = OrderItem(
          id: int.tryParse(value["id"] ?? "") ?? 1,
          product: _handleProductDescriptor(value["descriptor"] as Map),
          amount: value["amount"],
          price: Price(
            basePrice: double.tryParse(value["price"]) ?? 0.0,
          ),
          selectedSize: value["size"],
          selectedColor: value["color"],
          variationId: int.tryParse(value['var_id'] ?? ""),
        );
        items.add(item);
      });
      return items;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<void> removeFromBasket(int id) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Basket.repository}/remove",
        header: _headerWithSecurity,
        body: {
          "id": "${id.toString()}",
        },
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<void> basketUpdate(OrderItem item) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Basket.repository}/update",
        header: _headerWithSecurity,
        body: {
          "id": "${item.id.toString()}",
          "price": "${item.price.price.toString()}",
          "quantity": "${item.amount.toString()}",
        },
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
  }

  static Future<OrderItem> addToBasket(OrderItem item) async {
    try {
      await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Basket.repository}/add",
        header: _headerWithSecurity,
        body: {
          "id": "${item.product.id.toString()}",
          "price": "${item.price.price.toString()}",
          "quantity": "${item.amount.toString()}",
          "color": "${item.selectedColor ?? ''}",
          "size": "${item.selectedSize ?? ''}",
        },
      );
    } catch (e) {
      print(e);
    }
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.get(
          url:
              "$_apiUrl/${Repositories.Basket.repository}/id?id=${item.product.id.toString()}${item.selectedColor != null ? "&color=${item.selectedColor}" : ''}${item.selectedSize != null ? "&size=${item.selectedSize}" : ''}",
          header: _headerWithSecurity,
        ),
      );
      final int id = int.tryParse(body["id"] ?? "");
      if (id != null) {
        return OrderItem(
          price: item.price,
          product: item.product,
          selectedColor: item.selectedColor ?? body["color"],
          selectedSize: item.selectedSize ?? body["size"],
          id: id,
          variationId: body["var_id"],
          amount: item.amount,
        );
      }
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
    } catch (e) {
      print(e);
    }
    return null;
  }

  static Future<List<LikedProductDescriptor>> getLikedProducts() async {
    try {
      var body = jsonDecode(
        await OnlineRequestsHandler.get(
          url: "$_apiUrl/${Repositories.Favourite.repository}",
          header: _headerWithSecurity,
        ),
      );
      final List<LikedProductDescriptor> products = [];
      if (body != null && body.length > 0) {
        body.forEach(
          (key, value) {
            final Map product = value;
            products.add(
              LikedProductDescriptor(
                id: int.tryParse(key ?? '1') ?? 1,
                product: _handleProductDescriptor(product),
              ),
            );
          },
        );
      }
      return products;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<List<void>> getProductList(
      {int categoryId, bool newcomers, bool discounted}) async {
    try {
      newcomers = newcomers ?? false;
      discounted = discounted ?? false;
      final List body = jsonDecode(
        await OnlineRequestsHandler.get(
          url:
              "$_apiUrl/${Repositories.Catalog.repository}?${categoryId != null && categoryId > 0 ? "section_id=$categoryId&" : ""}new=$newcomers&discount=$discounted",
          header: _headerWithSecurity,
        ),
      );
      final List<ProductDescriptor> products = [];
      body.forEach((value) {
        final Map product = value;
        products.add(_handleProductDescriptor(product));
      });
      return products;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<Product> getProduct(int productId) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.get(
          url:
              "$_apiUrl/${Repositories.Product.repository}?id=${productId.toString()}",
          header: _headerWithSecurity,
        ),
      );
      final List<ProductDescriptor> suggestions = [];
      final List<String> pics = [];
      final List<String> sizes = [];
      final List<Review> reviews = [];
      final Map<String, String> features = {};
      (body["data"] ?? {}).forEach(
        (key, value) {
          features[key] = value.toString();
        },
      );
      (body["sizes"] ?? []).forEach(
        (value) {
          sizes.add(value.toString());
        },
      );
      final List<String> colors = [];
      (body["colors"] ?? []).forEach(
        (value) {
          colors.add(value.toString());
        },
      );
      ((body["suggestions"] as List) ?? []).forEach((element) {
        suggestions.add(_handleProductDescriptor(element));
      });
      ((body["pics"] as List) ?? []).forEach((element) {
        pics.add("$_baseUrl$element");
      });
      ((body["comments"] as List) ?? []).forEach((element) {
        reviews.add(_handleReview(element));
      });
      final Product product = Product(
        descriptor: _handleProductDescriptor(body["descriptor"]),
        description: body["desc"] ?? "",
        images: pics,
        suggestions: suggestions,
        features: features,
        feedback: ProductFeedback(
          reviews: reviews,
        ),
        availableSizes: sizes,
        availableColors: colors,
      );

      return product;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<List<Review>> addReview(Review review, int productId) async {
    try {
      var body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.Product.repository}/comment",
          header: _headerWithSecurity,
          body: {
            "product_id": "${productId.toString()}",
            "id": "${review.id.toString()}",
            "message": "${review.text.toString()}",
            "name": "${review.name.toString()}",
          },
        ),
      );
      final List<Review> reviews = [];
      if (body["error"] != null) {
        return null;
      }
      ((body["comments"] as List) ?? []).forEach((element) {
        reviews.add(_handleReview(element));
      });
      return reviews;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<List<ProductDescriptor>> search(String query) async {
    try {
      final String body = await OnlineRequestsHandler.post(
        url: "$_apiUrl/${Repositories.Product.repository}/search",
        header: _headerWithSecurity,
        body: {
          "query": query,
        },
      );
      final List<ProductDescriptor> _products = [];
      if (body == null || body.isEmpty) {
        return [];
      }
      (jsonDecode(body) as List).forEach((element) {
        _products.add(_handleProductDescriptor(element));
      });
      return _products;
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Future<Rating> rateProduct(int stars, int productId) async {
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_apiUrl/${Repositories.Product.repository}/rate",
          header: _headerWithSecurity,
          body: {
            "product_id": "${productId.toString()}",
            "stars": "${stars.toString()}",
          },
        ),
      );
      return Rating(
        rating: (body["rating"] ?? 0.0) + 0.0,
        voteCount: (body["votes"] as int) ?? 0,
      );
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      rethrow;
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  static Review _handleReview(Map reviewJson) {
    return Review(
      text: reviewJson["message"],
      id: reviewJson["id"] == null || (reviewJson["id"] as String).isEmpty
          ? null
          : int.tryParse(reviewJson["id"]),
      name: reviewJson["name"],
      date: DateFormat("dd.MM.yyyy hh:mm:ss").parse(reviewJson["date"]),
    );
  }

  static ProductDescriptor _handleProductDescriptor(Map product) {
    final double price = double.tryParse(product["price"] ?? '0') ?? 0;

    return ProductDescriptor(
      id: int.tryParse(product["id"]),
      name: HtmlUnescape().convert(product["name"] ?? ""),
      image: product['main_pic'] == null
          ? null
          : "$_baseUrl${product['main_pic']}",
      price: Price(
        basePrice: product['discount'] != null && product['discount']
            ? price / 0.95
            : price,
        discountedPrice:
            product['discount'] != null && product['discount'] ? price : null,
      ),
      rating: Rating(
        rating: double.tryParse(product["rating"] ?? '0') ?? 0,
        voteCount: int.tryParse(product["votes"] ?? '0') ?? 0,
      ),
      minAmount: int.tryParse(product["min_amount"] ?? "1") ?? 1,
      setNumber: product["artikul"] ?? '',
      manufacturer: product["producer"] ?? '',
    );
  }
}
