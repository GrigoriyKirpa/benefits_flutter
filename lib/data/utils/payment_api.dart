import 'dart:convert';

import 'package:benefit_flutter/data/models/card_token.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/utils/online_requests_handler.dart';
import 'package:benefit_flutter/presentation/screens/three_ds_verification_screen.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:flutter/material.dart';

class PaymentAPI {
  static const String postBackUrl =
      "https://demo.cloudpayments.ru/WebFormPost/GetWebViewData";
  static const String _baseUrl = "https://api.cloudpayments.ru/payments";
  static const String merchantPublicId = "pk_2663c7cd86176853ef07e45460685";
  static const String _cardPaymentsPassword =
      "e611a93c791a162b86906215d06120eb";
  static final Map<String, String> _headers = {
    "authorization": 'Basic ' +
        base64Encode('$merchantPublicId:$_cardPaymentsPassword'.codeUnits),
    "X-Request-ID": "",
  };

  final Function(CardToken, String) onSuccess;
  final Function(String) onFailure;
  final VoidCallback onCanceled;
  final VoidCallback onServerError;
  final Function(String, String, String) on3dsRequired;
  final int userId;
  final Order order;
  final String email;

  PaymentAPI({
    @required this.userId,
    @required this.order,
    @required this.email,
    Function(String, String, String) on3dsRequired,
    VoidCallback onServerError,
    VoidCallback onCanceled,
    Function(CardToken, String) onSuccess,
    Function(String) onFailure,
  })  : this.onServerError = onServerError ?? (() => null),
        this.onCanceled = onCanceled ?? (() => null),
        this.onSuccess = onSuccess ?? ((_, __) => null),
        this.onFailure = onFailure ?? ((_) => null),
        this.on3dsRequired = on3dsRequired ?? ((_, __, ___) => null),
        assert(userId > 0),
        assert(order != null && order.payment.total > 0),
        assert(email != null && email.isNotEmpty);

  Future chargeCard(String cardCryptogram) async {
    final String ipv4 = await Ipify.ipv4();
    _headers["X-Request-ID"] =
        "${order.id.toString()}_${order.payment.total.toString()}_${DateTime.now().millisecondsSinceEpoch.toString()}";
    try {
      final Map body = await jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_baseUrl/cards/auth",
          header: _headers,
          body: {
            "Amount": order.payment.total.toString(),
            "Currency": "RUB",
            "IpAddress": ipv4,
            "CardCryptogramPacket": cardCryptogram,
            "AccountId": "vigoda_user_${userId.toString()}",
            "Email": email,
            "InvoiceId": order.id.toString(),
          },
        ),
      );
      _charge(body);
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      onServerError();
    } catch (e) {
      print(e);
      onServerError();
    }
  }

  Future chargeCardByToken(String token) async {
    _headers["X-Request-ID"] =
        "${order.id.toString()}_${order.payment.total.toString()}_${DateTime.now().millisecondsSinceEpoch.toString()}";
    try {
      final Map body = jsonDecode(
        await OnlineRequestsHandler.post(
          url: "$_baseUrl/tokens/auth",
          header: _headers,
          body: {
            "Amount": order.payment.total.toString(),
            "Currency": "RUB",
            "Token": token,
            "AccountId": "vigoda_user_${userId.toString()}",
            "Email": email,
            "InvoiceId": order.id.toString(),
          },
        ),
      );
      _charge(body);
    } on OnlineRequestFailedException catch (e) {
      print(e.cause);
      onServerError();
    } catch (e) {
      print(e);
      onServerError();
    }
  }

  Future _charge(Map response) async {
    final Map body = await verify3ds(response);
    if (body == null) {
      onServerError();
    } else if (body.isEmpty) {
      onCanceled();
    } else {
      handleResponse(body);
    }
  }

  Future verify3ds(Map body) async {
    _headers["X-Request-ID"] =
        "${order.id.toString()}_${order.payment.total.toString()}_${DateTime.now().millisecondsSinceEpoch.toString()}";
    if (body["Model"] != null && body["Model"]["ThreeDsCallbackId"] != null) {
      var result3Ds = await on3dsRequired(
          body["Model"]["TransactionId"].toString(),
          body["Model"]["AcsUrl"],
          body["Model"]["PaReq"]);
      if (result3Ds == null) {
        return {};
      } else if (result3Ds is ThreeDsVerifiedArguments) {
        return jsonDecode(await OnlineRequestsHandler.post(
          url: "$_baseUrl/cards/post3ds",
          header: _headers,
          body: {
            "TransactionId": result3Ds.transactionId,
            "PaRes": result3Ds.paRes,
          },
        ));
      } else {
        return null;
      }
    }
    return body;
  }

  void handleResponse(Map body) {
    if (body["Model"] == null) {
      throw Exception("Unexpected answer from the server");
    }
    final Map model = body["Model"];
    if (body["Success"]) {
      onSuccess(
        CardToken(
          token: model["Token"],
          cardType: model["CardType"],
          lastDigits: model["CardLastFour"],
        ),
        model["TransactionId"].toString(),
      );
    } else {
      onFailure(model["CardHolderMessage"]);
    }
  }
}
