import 'dart:io';

import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/user.dart';
import 'package:path_provider/path_provider.dart';

class LocalStorage {
  static Future<String> getRootStoragePath([String name]) async {
    final Directory appDocumentsDirectory =
        await getApplicationDocumentsDirectory();
    final String appDocumentsPath = appDocumentsDirectory.path;
    Directory rootStorageDirectory =
        await Directory('$appDocumentsPath/user/').create();
    return rootStorageDirectory.path + name;
  }

  static Future<UserInfo> getUserInfoFromLocalStorage() async {
    String path = await getRootStoragePath(userInfoFileName);
    if (!localPathExists(path)) return null;
    File rootStorageFile = File(path);
    String jsonString = await rootStorageFile.readAsString();
    return UserInfo.fromJson(jsonString);
  }

  static Future<void> saveUserInfoToLocalStorage(UserInfo info) async {
    String path = await getRootStoragePath(userInfoFileName);
    File rootFile = await File(path).create();
    await rootFile.writeAsString(info.toJson());
  }

  static Future<void> deleteUserInfoFromLocalStorage() async {
    String path = await getRootStoragePath(userInfoFileName);
    await File(path).delete();
  }

  static bool localPathExists(String path) {
    return path != null && File(path).absolute.existsSync();
  }
}
