import 'package:benefit_flutter/constants.dart';

class Language {
  static int getPlural(int number) {
    if (number.toString().length >= 2) {
      return _parseLargeNumber(number);
    } else {
      return _parseLastDigit(number);
    }
  }

  static int _parseLastDigit(int number) {
    if (number == 0 || number > 4)
      return pluralType1;
    else if (number == 1)
      return singular;
    else {
      return pluralType2;
    }
  }

  static int _parseLargeNumber(int number) {
    final String numStr = number.toString();
    if (numStr[numStr.length - 2] == '1')
      return pluralType1;
    else
      return _parseLastDigit(number);
  }

  static String removeHtml(String text) {
    return text.replaceAll(RegExp(r'<[^>]*>'), '');
  }

  static String extractJson(String text) {
    var match = RegExp('.*?({.*}).*', dotAll: true).firstMatch(text);

    return match == null ? "" : match.group(1);
  }
}
