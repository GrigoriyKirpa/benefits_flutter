import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class OnlineRequestFailedException implements Exception {
  final int statusCode;
  final String message;

  String get cause {
    return "Response code: ${statusCode.toString()}\nError: $message";
  }

  OnlineRequestFailedException(this.statusCode, this.message);
}

class OnlineRequestsHandler {
  OnlineRequestsHandler._();

  static Future<String> get(
      {@required String url, Map<String, String> header}) async {
    var uri = Uri.parse(url);

    var response = await http.get(
      uri,
      headers: header,
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      return response.body;
    } else {
      throw OnlineRequestFailedException(response.statusCode, response.body);
    }
  }

  static Future<String> post(
      {@required String url, Map header, Map body}) async {
    var uri = Uri.parse(url);

    var response = await http.post(
      uri,
      headers: header,
      body: body,
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      return response.body;
    } else {
      throw OnlineRequestFailedException(response.statusCode, response.body);
    }
  }
}
