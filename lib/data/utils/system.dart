import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:url_launcher/url_launcher.dart';

class System {
  static const cardPaymentsChannel =
      MethodChannel("com.flutter.benefits/cardPayments");
  static void displaySnackBar(BuildContext context, SnackBar snackBar) {
    if (!ScaffoldMessenger.of(context).mounted) {
      return;
    }
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static void displayErrorSnackBar(BuildContext context,
      {String errorLocaleKey = "error.no_service"}) {
    displaySnackBar(
      context,
      CustomWidgets.textSnackBar(
        customRedColor,
        FlutterI18n.translate(context, errorLocaleKey),
        Theme.of(context)
            .textTheme
            .bodyText1
            .copyWith(fontSize: 20, color: Colors.white),
      ),
    );
  }

  static void displayErrorSnackBarWithMessage(
      BuildContext context, String message) {
    displaySnackBar(
      context,
      CustomWidgets.textSnackBar(
        customRedColor,
        message,
        Theme.of(context)
            .textTheme
            .bodyText1
            .copyWith(fontSize: 20, color: Colors.white),
      ),
    );
  }

  static Future<void> call(String phone) async {
    String url = "tel:$phone";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw Exception('Could not launch $url');
    }
  }

  static Future<void> email(String email) async {
    String url = "mailto:$email";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw Exception('Could not launch $url');
    }
  }

  static Future<List<PlatformFile>> pickFiles() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: [
        'jpg',
        'pdf',
        'doc',
        'jpeg',
        'docx',
        'xlsx',
        'xls',
        'csv',
        'txt',
        'png',
        'svg',
      ],
    );

    if (result != null) {
      return result.files;
    } else {
      return null;
    }
  }
}
