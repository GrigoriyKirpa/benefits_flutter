import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/product_feedback.dart';
import 'package:flutter/cupertino.dart';

class Product {
  final ProductDescriptor descriptor;
  final String description;
  final Map<String, String> features;
  final ProductFeedback feedback;
  final List<String> images;
  final List<ProductDescriptor> suggestions;
  final List<String> availableColors;
  final List<String> availableSizes;
  Product({
    @required this.descriptor,
    @required this.description,
    @required this.features,
    @required this.feedback,
    List<String> availableSizes,
    List<String> availableColors,
    List<ProductDescriptor> suggestions,
    List<String> images,
  })  : this.availableColors = availableColors ?? [],
        this.availableSizes = availableSizes ?? [],
        this.suggestions = suggestions ?? [],
        this.images = images ?? [],
        assert(description != null),
        assert(descriptor != null),
        assert(features != null);
}
/*
Phone phone = Phone(countryCode: "+60", phone: "1133518919");
List<Product> mockProducts = [
  Product(
    setNumber: '123456',
    id: 1,
    price: Price(basePrice: 123000),
    name: 'Мишка',
    availableColors: specificationColorList,
    availableSizes: [20, 26, 30, 40],
    feedback: ProductFeedback(
      questionsAndAnswers: [
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
          answer: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
      ],
      rating: Rating(reviews: [
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
        Review(
          date: DateTime.now(),
          stars: 1.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 4.0,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
      ]),
    ),
    description: 'heyheyehyehehdasdahnvubduqhicjqeb',
    features: {
      "Код товара": "33444219",
      "Высота, мм": "90",
      "Ширина, мм": "25",
    },
    images: [
      Image.asset(
        'assets/images/randomImage1.jpg',
      ).image,
      Image.asset(
        'assets/images/randomImage3.jpg',
      ).image,
    ],
  ),
  Product(
    availableSizes: [20, 26, 30, 40],
    feedback: ProductFeedback(
      questionsAndAnswers: [
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
          answer: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
      ],
      rating: Rating(reviews: [
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
        Review(
          date: DateTime.now(),
          stars: 1.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 4.0,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
      ]),
    ),
    setNumber: '123456',
    id: 2,
    price: Price(basePrice: 123, discountedPrice: 100),
    name: 'Нечто совсем иное и красивое',
    description: 'heyheyehyehehdasdahnvubduqhicjqeb',
    features: {
      "Код товара": "33444219",
      "Высота, мм": "90",
      "Ширина, мм": "25",
    },
    images: [
      Image.asset(
        'assets/images/randomImage2.jpg',
      ).image,
    ],
  ),
  Product(
    availableSizes: [20, 26, 30, 40],
    feedback: ProductFeedback(
      questionsAndAnswers: [
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
          answer: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
      ],
      rating: Rating(reviews: [
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
        Review(
          date: DateTime.now(),
          stars: 1.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 4.0,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
      ]),
    ),
    setNumber: '123456',
    id: 3,
    price: Price(basePrice: 12300, discountedPrice: 10000),
    name: 'Еще один мишка',
    description: 'heyheyehyehehdasdahnvubduqhicjqeb',
    features: {
      "Код товара": "33444219",
      "Высота, мм": "90",
      "Ширина, мм": "25",
    },
    images: [
      Image.asset(
        'assets/images/randomImage3.jpg',
      ).image,
    ],
  ),
  Product(
    availableSizes: [20, 26, 30, 40],
    feedback: ProductFeedback(
      questionsAndAnswers: [
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
          answer: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
        QuestionAndAnswer(
          question: Message(
            sender: User(
              id: "1",
              phone: phone,
              fullName: "Grigoriy Kirpa",
            ),
            text: "Hello how are you?",
            date: DateTime.now(),
          ),
        ),
      ],
      rating: Rating(reviews: [
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
        Review(
          date: DateTime.now(),
          stars: 1.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 4.0,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "hey its all good",
        ),
        Review(
          date: DateTime.now(),
          stars: 3.5,
          user: User(
            id: "1",
            phone: phone,
            fullName: "Grigoriy Kirpa",
          ),
          text: "",
        ),
      ]),
    ),
    setNumber: '123456',
    id: 4,
    description: 'heyheyehyehehdasdahnvubduqhicjqeb',
    features: {
      "Код товара": "33444219",
      "Высота, мм": "90",
      "Ширина, мм": "25",
    },
    price: Price(basePrice: 12300, discountedPrice: 10000),
    name: 'Еще один мишка',
  ),
];*/
