import 'dart:convert';

import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:flutter/material.dart';

class Phone {
  final String countryCode;
  final String phone;

  Phone({@required this.countryCode, @required this.phone})
      : assert(countryCode != null &&
            countryCode.length > 1 &&
            countryCode.length < 4 &&
            countryCode[0] == '+'),
        assert(phone != null && phone.length >= 10 && phone.length <= 12);

  factory Phone.fromJson(String data) {
    Map json = jsonDecode(data);
    return Phone(countryCode: json["code"], phone: json["phone"]);
  }

  @override
  String toString() {
    return "$countryCode ${FormFields.maskPhoneFormatter.maskText(phone)}";
  }

  String get nonFormatted {
    return "$countryCode$phone";
  }

  String toJson() {
    Map json = {
      "code": countryCode,
      "phone": phone,
    };
    return jsonEncode(json);
  }
}
