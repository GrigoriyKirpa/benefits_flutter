import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class Filters {
  bool productAvailable = true;
  PriceRange priceRange = PriceRange();
  StockRange stockRange = StockRange();
  ColorFilter color;

  static String label(BuildContext context) {
    return FlutterI18n.translate(context, "product.filters.default");
  }
}

class PriceRange {
  double from;
  double to;
}

class StockRange {
  int from;
  int to;
}

enum MaterialFilter {
  element,
}

extension MaterialFilterExtension on MaterialFilter {
  static const localePath = "product.filters.material";

  static filterName(BuildContext context) {
    return FlutterI18n.translate(context, "$localePath.default");
  }

  String get localeKey {
    String localeKey = "default";
    if (this == MaterialFilter.element) {
      localeKey = "white";
    }
    return localeKey;
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(context, "$localePath.$localeKey");
  }
}

enum ColorFilter {
  White,
  Black,
  Blue,
  Red,
  Yellow,
  Purple,
  Orange,
  Transparent,
}

extension ColorFilterExtension on ColorFilter {
  static const localePath = "product.filters.color";

  static filterName(BuildContext context) {
    return FlutterI18n.translate(context, "$localePath.default");
  }

  String get localeKey {
    String localeKey = "default";
    if (this == ColorFilter.White) {
      localeKey = "white";
    } else if (this == ColorFilter.Black) {
      localeKey = "black";
    } else if (this == ColorFilter.Blue) {
      localeKey = "blue";
    } else if (this == ColorFilter.Red) {
      localeKey = "red";
    } else if (this == ColorFilter.Yellow) {
      localeKey = "yellow";
    } else if (this == ColorFilter.Purple) {
      localeKey = "purple";
    } else if (this == ColorFilter.Orange) {
      localeKey = "orange";
    } else if (this == ColorFilter.Transparent) {
      localeKey = "transparent";
    }
    return localeKey;
  }

  String label(BuildContext context) {
    return FlutterI18n.translate(context, "$localePath.$localeKey");
  }
}
