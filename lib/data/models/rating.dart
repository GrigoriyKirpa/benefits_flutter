class Rating {
  final double rating;
  final int voteCount;

  Rating({this.rating = 0, this.voteCount = 0})
      : assert(rating >= 0.0),
        assert(voteCount >= 0);

  String get ratingString {
    if (rating == 0) {
      return '-';
    }
    return rating.toStringAsFixed(1);
  }
}
