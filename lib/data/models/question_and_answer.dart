import 'package:benefit_flutter/data/models/message.dart';
import 'package:flutter/material.dart';

class QuestionAndAnswer {
  final Message question;
  final Message answer;

  QuestionAndAnswer({@required this.question, this.answer})
      : assert(question != null);

  bool get isClosed {
    return answer == null;
  }

  bool get isNotClosed {
    return answer != null;
  }
}
