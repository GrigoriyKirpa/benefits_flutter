import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

enum BannerType {
  Main,
  Delivery,
}

extension BannerTypeExtension on BannerType {
  static const Map<int, BannerType> _types = {
    1: BannerType.Main,
    2: BannerType.Delivery,
  };

  static BannerType intToBannerType(int num) {
    if (_types.keys.contains(num)) {
      return _types[num];
    } else {
      throw Exception("Unsupported type");
    }
  }
}

class CustomBanner {
  final BannerType type;
  final CachedNetworkImage banner;

  CustomBanner({@required this.type, @required this.banner})
      : assert(type != null),
        assert(banner != null);
}
