import 'package:benefit_flutter/data/enums/payment_method.dart';
import 'package:benefit_flutter/data/models/price.dart';
import 'package:flutter/cupertino.dart';

class Payment {
  Price price;
  Price delivery;
  PaymentMethod method;
  bool paid;

  Payment({
    @required this.price,
    this.delivery,
    this.method,
    this.paid = false,
  });

  double get total {
    return price.price + (delivery != null ? delivery.price : 0.0);
  }
}
