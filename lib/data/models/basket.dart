import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';

class Basket {
  static final Basket _instance = Basket._internal();

  factory Basket() {
    return _instance;
  }

  Basket._internal() : items = [];

  List<OrderItem> items;

  Future<bool> add(OrderItem item) async {
    if (Shop.currentUser.isLiked(item.product.id)) {
      final LikedProductDescriptor liked = Shop.currentUser.find(item.product);
      final OrderItem newItem = await ServerRequests.favToBasket(liked);
      if (newItem != null) {
        if (!Shop.isBusy) {
          items.add(newItem);
          Shop.currentUser.dislike(
            liked,
          );
        }
      } else {
        return false;
      }
    } else {
      final OrderItem newItem = await ServerRequests.addToBasket(item);
      if (newItem != null) {
        final OrderItem existingItem = items.firstWhere(
          (element) => element.id == newItem.id,
          orElse: () {
            return null;
          },
        );
        if (existingItem != null) {
          newItem.amount += existingItem.amount;
          items.remove(existingItem);
        }
        if (!Shop.isBusy) items.add(newItem);
      } else {
        return false;
      }
    }
    return true;
  }

  void fill(List<OrderItem> items) {
    this.items.addAll(items);
  }

  void remove(OrderItem item) {
    ServerRequests.removeFromBasket(item.id);
    items.remove(item);
  }

  void clear() {
    items.clear();
  }

  bool containsProduct(ProductDescriptor product) {
    return items.any((element) => element.product.id == product.id);
  }

  OrderItem find(ProductDescriptor product) {
    return items.firstWhere(
      (element) => element.product.id == product.id,
      orElse: () {
        return null;
      },
    );
  }

  void removeLocally(OrderItem item) {
    if (item != null) {
      items.remove(item);
    }
  }
}
