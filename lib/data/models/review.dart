import 'package:flutter/material.dart';

class Review {
  final String text;
  final DateTime date;
  final String name;
  final int id;

  Review({this.text = '', @required this.date, this.name, this.id})
      : assert(date != null);
}
