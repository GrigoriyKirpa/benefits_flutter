import 'dart:convert';

import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/data/models/company_details.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/phone.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';

class User {
  UserInfo info;
  final List<Specification> specifications;
  final List<LikedProductDescriptor> likedProducts;

  User({
    this.info,
    List<Specification> specifications,
    List<LikedProductDescriptor> likedProducts,
  })  : this.specifications = specifications ?? [],
        this.likedProducts = likedProducts ?? [];
  //assert(id != null && id.isNotEmpty),
  //assert(fullName != null && fullName.isNotEmpty),
  //assert(phone != null);

  Future<bool> handleLike(ProductDescriptor product) async {
    if (Shop.basket.containsProduct(product)) {
      final OrderItem item = Shop.basket.find(product);
      ServerRequests.basketToFav(item);
      if (!Shop.isBusy) {
        Shop.basket.removeLocally(item);
        likedProducts
            .add(LikedProductDescriptor(id: item.id, product: product));
      }
      return true;
    } else if (isLiked(product.id)) {
      final LikedProductDescriptor descriptor = find(product);
      if (descriptor != null) {
        likedProducts.remove(descriptor);
        ServerRequests.removeFromBasket(descriptor.id);
        return true;
      }
    } else {
      final int id = await ServerRequests.like(product);
      if (id != null) {
        if (!Shop.isBusy) {
          likedProducts.add(
            LikedProductDescriptor(
              id: id,
              product: product,
            ),
          );
        }
        return true;
      }
    }
    return false;
  }

  Address get currentAddress {
    return info.addressBook.isNotEmpty && info.currentAddress >= 0
        ? info.addressBook[info.currentAddress]
        : null;
  }

  void like(LikedProductDescriptor product) {
    if (product != null && !Shop.isBusy) {
      likedProducts.add(product);
    }
  }

  String specificationsToJson() {
    Map specs = {};
    specifications.forEach((element) {
      specs[specifications.indexOf(element).toString()] = element.toJson();
    });
    return jsonEncode(specs);
  }

  void specificationsFromJson(Map json) {
    if (json == null || json.isEmpty) return;

    json.values.forEach((value) {
      specifications.add(Specification.fromJson(value));
    });
  }

  void dislike(LikedProductDescriptor product) {
    if (product != null) {
      likedProducts.remove(product);
    }
  }

  LikedProductDescriptor find(ProductDescriptor product) {
    return likedProducts.firstWhere(
      (element) => element.product.id == product.id,
      orElse: () => null,
    );
  }

  bool isLiked(int id) {
    return likedProducts.any((element) => element.product.id == id);
  }

  bool isInSpecification(ProductDescriptor product) {
    for (Specification specification in specifications) {
      if (specification.contains(product)) {
        return true;
      }
    }
    return false;
  }

  String specificationName(ProductDescriptor product) {
    final Specification specification = inSpecification(product);
    return specification == null ? null : specification.name;
  }

  Specification inSpecification(ProductDescriptor product) {
    for (Specification specification in specifications) {
      if (specification.contains(product)) {
        return specification;
      }
    }
    return null;
  }

  void addToSpecification(
      Specification specification, ProductDescriptor product) {
    specification.add(product);
    ServerRequests.updateSpecifications(specificationsToJson());
  }

  void edit(Specification editFrom, int index) {
    specifications[index].editFrom(editFrom);
    ServerRequests.updateSpecifications(specificationsToJson());
  }

  void removeFromSpecification(ProductDescriptor product) {
    final Specification specification = inSpecification(product);
    if (specification != null) {
      specification.remove(product);
      ServerRequests.updateSpecifications(specificationsToJson());
    }
  }

  List<int> get specificationIds {
    final List<int> ids = [];
    specifications.forEach((element) {
      ids.addAll(element.productIds);
    });
    return ids;
  }

  void fillProducts(List<ProductDescriptor> products) {
    products.forEach(
      (element) {
        final Specification specification = inSpecification(element);
        if (specification != null) {
          specification.products.add(element);
        }
      },
    );
  }

  void deleteSpecification(Specification specification) {
    if (this.specifications.contains(specification)) {
      this.specifications.remove(specification);
      specification.delete();
      ServerRequests.updateSpecifications(specificationsToJson());
    }
  }

  void addSpecification(Specification specification) {
    if (!this.specifications.contains(specification)) {
      this.specifications.add(specification);
      ServerRequests.updateSpecifications(specificationsToJson());
    }
  }
}

class UserInfo {
  int id;
  Phone phone;
  String fullName;
  String email;
  String password;
  AccountType type;
  int currentAddress;
  final List<Address> addressBook;
  final List<CompanyDetails> companyDetails;
  final Map<int, int> ratingMap;
  List<String> searchHistory;

  UserInfo({
    this.id,
    this.phone,
    this.fullName = "",
    this.email = "",
    this.password = "",
    this.type,
    this.currentAddress,
    List<Address> addressBook,
    List<CompanyDetails> companyDetails,
    Map<int, int> ratingMap,
    List<String> searchHistory,
  })  : this.searchHistory = searchHistory ?? [],
        this.addressBook = addressBook ?? [],
        this.companyDetails = companyDetails ?? [],
        this.ratingMap = ratingMap ?? {};

  String toJson() {
    final Map json = {
      "id": id.toString(),
      "phone": phone.toJson(),
      "name": fullName,
      "email": email,
      "password": password,
      "type": type == null ? "-1" : type.index.toString(),
      "address": (currentAddress ?? -1).toString(),
      "address_book": addressBook.map((address) => address.toJson()).toList(),
      "company_details":
          companyDetails.map((element) => element.toJson()).toList(),
      "rating_map": jsonEncode(
        ratingMap.map(
          (key, value) => MapEntry(key.toString(), value.toString()),
        ),
      ),
      "search_history": jsonEncode(searchHistory),
    };

    return jsonEncode(json);
  }

  factory UserInfo.fromJson(String data) {
    final Map json = jsonDecode(data);
    final int id = int.tryParse(json["id"]);
    final Phone phone = Phone.fromJson(json["phone"]);
    final String fullName = json["name"];
    final String email = json["email"];
    final String password = json["password"];
    final List<Address> addressBook = (json["address_book"] ?? [])
        .map((address) => Address.fromJson(address))
        .toList()
        .cast<Address>();
    final List<CompanyDetails> companyDetails = (json["company_details"] ?? [])
        .map((element) => CompanyDetails.fromJson(element))
        .toList()
        .cast<CompanyDetails>();
    final int typeIndex = int.tryParse(json["type"]);
    final int addressIndex = json["address"] == null
        ? json["address"]
        : int.tryParse(json["address"]);
    final Map<int, int> ratingMap =
        json["rating_map"] == null || (json["rating_map"] as String).isEmpty
            ? {}
            : (jsonDecode(json["rating_map"]) as Map).map(
                (key, value) => MapEntry(
                  int.tryParse(key ?? '0') ?? 0,
                  int.tryParse(value ?? '0') ?? 0,
                ),
              );
    final List<String> searchHistory = json['search_history'] == null ||
            (json['search_history'] as String).isEmpty
        ? []
        : (jsonDecode(json['search_history']) as List).cast<String>();
    return UserInfo(
      id: id,
      phone: phone,
      fullName: fullName,
      email: email,
      password: password,
      addressBook: addressBook,
      companyDetails: companyDetails,
      type: typeIndex == -1 ? null : AccountType.values[typeIndex],
      currentAddress: addressIndex,
      ratingMap: ratingMap,
      searchHistory: searchHistory,
    );
  }
}
