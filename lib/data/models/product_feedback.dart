import 'package:benefit_flutter/data/models/question_and_answer.dart';
import 'package:benefit_flutter/data/models/review.dart';

class ProductFeedback {
  List<Review> reviews;
  List<QuestionAndAnswer> questionsAndAnswers;

  ProductFeedback({
    List<QuestionAndAnswer> questionsAndAnswers,
    List<Review> reviews,
  })  : this.questionsAndAnswers = questionsAndAnswers ?? [],
        this.reviews = reviews ?? [];

  void addReview(Review review) {
    reviews.add(review);
  }
}
