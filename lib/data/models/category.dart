import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Category {
  final CategoryDescriptor descriptor;
  final List<ProductDescriptor> products;

  Category({
    @required this.descriptor,
    this.products,
  }) : assert(descriptor != null);
}
/*
final discountCategory = Category(
    id: 15,
    icon: Icons.image_not_supported,
    name: "Акции",
    products: mockProducts.getRange(1, mockProducts.length).toList());
final newArrivalCategory = Category(
    id: 16,
    icon: Icons.image_not_supported,
    name: "Новинки",
    products: mockProducts);

List<Category> mockCategory = [
  Category(
      id: 1,
      icon: CustomIcons.teddy_bear,
      name: "Детские товары",
      products: mockProducts),
  Category(
      id: 2,
      icon: CustomIcons.wood,
      name: "Деревянные размешиватели",
      products: mockProducts),
  Category(
      id: 3, icon: CustomIcons.virus, name: "Covid-19", products: mockProducts),
  Category(
      id: 4,
      icon: CustomIcons.robe,
      name: "Одежда из спанбонда",
      products: mockProducts),
  Category(
      id: 5,
      icon: CustomIcons.disinfection,
      name: "Диспенсеры (дозаторы)",
      products: mockProducts),
  Category(
      id: 6,
      icon: CustomIcons.football_jersey,
      name: "Футбольная форма",
      products: mockProducts),
  Category(
      id: 7,
      icon: CustomIcons.toilet_paper,
      name: "Бумажная продукция",
      products: mockProducts),
  Category(
      id: 8,
      icon: CustomIcons.bowls,
      name: "Кухонные принадлежности",
      products: mockProducts),
  Category(
      id: 9,
      icon: CustomIcons.washing_machine,
      name: "Хозяйственные товары",
      products: mockProducts),
  Category(
      id: 10,
      icon: CustomIcons.tshirt,
      name: "Текстиль (форма)",
      products: mockProducts),
  Category(
      id: 11,
      icon: CustomIcons.pencil,
      name: "Канцелярия",
      products: mockProducts),
  Category(
      id: 12,
      icon: CustomIcons.cutlery,
      name: "Одноразовая посуда",
      products: mockProducts),
  Category(
      id: 13,
      icon: CustomIcons.first_aid_kit,
      name: "Медицинские товары",
      products: mockProducts),
  Category(
      id: 14,
      icon: CustomIcons.broom,
      name: "Уборочный инвентарь",
      products: mockProducts),
];
*/
