import 'package:flutter/cupertino.dart';

class PromoCode {
  final String code;
  final String name;

  PromoCode({@required this.code, @required this.name})
      : assert(code != null && code.isNotEmpty),
        assert(name != null);
}
