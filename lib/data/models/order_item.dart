import 'package:benefit_flutter/data/models/price.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:flutter/material.dart';

class OrderItem {
  int id;
  final ProductDescriptor product;
  final String selectedColor;
  final String selectedSize;
  final Price price;
  final int variationId;
  int amount;

  OrderItem({
    this.id,
    @required this.price,
    @required this.product,
    this.selectedColor,
    this.selectedSize,
    this.variationId,
    this.amount = 1,
  })  : assert(product != null),
        assert(amount != null && amount > 0),
        assert(amount > 0);

  double get totalPrice {
    return amount * price.price;
  }
}
