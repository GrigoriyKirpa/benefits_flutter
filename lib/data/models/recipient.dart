import 'package:benefit_flutter/data/enums/replacement_options.dart';
import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/data/models/phone.dart';

class Recipient {
  Phone phone;
  String email;
  String name;
  String comment;
  Address address;
  ReplacementOptions option;
  bool pickUp;

  Recipient({
    this.phone,
    this.email,
    this.name,
    this.option,
    this.address,
    this.comment = '',
    this.pickUp = false,
  });
}
