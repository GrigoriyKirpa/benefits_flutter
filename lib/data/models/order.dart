import 'package:benefit_flutter/data/enums/order_status.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/payment.dart';
import 'package:benefit_flutter/data/models/promo_code.dart';
import 'package:benefit_flutter/data/models/recipient.dart';
import 'package:flutter/cupertino.dart';

class Order {
  int id;
  List<OrderItem> items;
  DateTime date;
  Recipient recipient;
  Payment payment;
  List<ImageProvider> images;
  OrderStatus status;
  PromoCode promoCode;

  Order({
    this.id,
    this.items,
    this.status,
    this.date,
    this.recipient,
    this.payment,
    this.promoCode,
    List<ImageProvider> images,
  }) : this.images = images ?? [];
}
