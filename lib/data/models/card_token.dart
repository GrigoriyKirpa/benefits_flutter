import 'dart:convert';

import 'package:flutter/cupertino.dart';

class CardToken {
  final String token;
  final String lastDigits;
  final String cardType;

  CardToken({
    @required this.token,
    @required this.cardType,
    @required this.lastDigits,
  })  : assert(token != null && token.isNotEmpty),
        assert(lastDigits != null && lastDigits.length == 4),
        assert(cardType != null && cardType.isNotEmpty);

  factory CardToken.fromJson(String data) {
    final Map json = jsonDecode(data);
    return CardToken(
      token: json["token"],
      lastDigits: json["last_digits"],
      cardType: json["card_type"],
    );
  }

  String toJson() {
    return jsonEncode({
      "token": token,
      "last_digits": lastDigits,
      "card_type": cardType,
    });
  }
}
