import 'dart:convert';

import 'package:flutter/cupertino.dart';

class CompanyDetails {
  String name;
  String kpp;
  String inn;

  CompanyDetails({
    @required this.name,
    this.kpp = "",
    @required this.inn,
  });

  factory CompanyDetails.fromJson(String data) {
    final Map json = jsonDecode(data);
    return CompanyDetails(
      name: json["name"],
      kpp: json["kpp"],
      inn: json["inn"],
    );
  }

  void editFrom(CompanyDetails companyDetails) {
    name = companyDetails.name;
    inn = companyDetails.inn;
    kpp = companyDetails.kpp;
  }

  String toJson() {
    return jsonEncode({
      "name": name,
      "kpp": kpp,
      "inn": inn,
    });
  }
}
