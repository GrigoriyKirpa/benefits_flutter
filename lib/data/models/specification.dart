import 'dart:convert';

import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:flutter/material.dart';

class Specification {
  static int idProvider = 1;
  final int id;
  String name;
  String description;
  Color color;
  final List<int> productIds;
  List<ProductDescriptor> products;

  Specification({
    @required this.id,
    @required this.name,
    this.description = '',
    @required this.color,
    List<ProductDescriptor> products,
    List<int> productIds,
  })  : this.productIds = productIds ?? [],
        this.products = products ?? [],
        assert(id != null && id > 0),
        assert(name != null && name.isNotEmpty),
        assert(color != null) {
    idProvider = this.id >= idProvider ? this.id + 1 : idProvider;
  }

  Specification.fromJson(Map json)
      : this.id = int.tryParse(json["id"]) ?? 1,
        this.name = json["name"],
        this.description = json["description"],
        this.color = specificationColorList[int.tryParse(json["color"]) ??
            specificationColorList.indexOf(defaultSpecificationColor)],
        this.products = [],
        this.productIds =
            (json["products"] as List).map((value) => value as int).toList() {
    idProvider = this.id >= idProvider ? this.id + 1 : idProvider;
  }

  void editFrom(Specification specification) {
    this.name = specification.name;
    this.description = specification.description;
    this.color = specification.color;
    ServerRequests.updateSpecifications(jsonEncode(toJson()));
  }

  bool contains(ProductDescriptor product) {
    return this.productIds.contains(product.id);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id.toString(),
      "name": name,
      "description": description,
      "color": specificationColorList.indexOf(color).toString(),
      "products": productIds,
    };
  }

  void handleProduct(ProductDescriptor product) {
    if (contains(product)) {
      add(product);
    } else {
      add(product);
    }
  }

  void add(ProductDescriptor product) {
    this.productIds.add(product.id);
    this.products.add(product);
  }

  void remove(ProductDescriptor product) {
    this.productIds.remove(product.id);
    this.products.remove(product);
  }

  void delete() {
    this.productIds.clear();
    this.products.clear();
  }
}
/*
List<Specification> mockSpecifications = [
  Specification(
      id: 1,
      name: 'Химия',
      color: specificationColorList.first,
      products: [mockProducts.first]),
  Specification(
      id: 2,
      name: 'Химия',
      color: specificationColorList.last,
      products: [mockProducts[1]]),
  Specification(
      id: 3,
      name: 'Химия',
      color: specificationColorList.last,
      products: [mockProducts[2]]),
];*/
