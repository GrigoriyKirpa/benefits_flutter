import 'package:benefit_flutter/data/models/price.dart';
import 'package:benefit_flutter/data/models/rating.dart';
import 'package:flutter/cupertino.dart';

class ProductDescriptor {
  final int id;
  final String name;
  final int minAmount;
  final String setNumber;
  final String manufacturer;
  String image;
  Price price;
  Rating rating;

  ProductDescriptor({
    @required this.id,
    @required this.price,
    @required this.name,
    @required this.minAmount,
    this.manufacturer = '',
    this.setNumber = '',
    this.image,
    this.rating,
  })  : assert(price != null),
        assert(id > 0),
        assert(name != null && name.isNotEmpty),
        assert(minAmount != null && minAmount > 0);

  static double calculateTotalPrice(List<ProductDescriptor> products) {
    return Price.getTotalPrice(
      products.map((product) {
        return product.price;
      }).toList(),
    );
  }
}

class LikedProductDescriptor {
  final int id;
  final ProductDescriptor product;

  LikedProductDescriptor({@required this.id, @required this.product})
      : assert(id != null && id > 0),
        assert(product != null);
}
