import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class Address {
  String city;
  String street;
  String house;
  String flat;
  String level;
  String extraInfo;

  Address({
    @required this.city,
    @required this.street,
    @required this.house,
    @required this.flat,
    @required this.level,
    this.extraInfo = '',
  })  : assert(city != null && city.isNotEmpty),
        assert(street != null && street.isNotEmpty),
        assert(house != null && house.isNotEmpty),
        assert(flat != null && flat.isNotEmpty),
        assert(level != null && level.isNotEmpty),
        assert(extraInfo != null);

  factory Address.fromJson(String data) {
    final Map json = jsonDecode(data);
    return Address(
      city: json["city"],
      street: json["street"],
      house: json["house"],
      flat: json["flat"],
      level: json["level"],
      extraInfo: json["extra_info"],
    );
  }

  String buildAddress(BuildContext context) {
    return "$street ${FlutterI18n.translate(context, "user.address.street")}, ${FlutterI18n.translate(context, "user.address.flat")} $house, " +
        "${FlutterI18n.translate(context, "user.address.house")} $flat, ${FlutterI18n.translate(context, "user.address.level")} $level, " +
        "${FlutterI18n.translate(context, "user.address.city")} $city, ${FlutterI18n.translate(context, "user.address.country")}";
  }

  void editFrom(Address address) {
    city = address.city;
    street = address.street;
    house = address.house;
    flat = address.flat;
    level = address.level;
    extraInfo = address.extraInfo;
  }

  String toJson() {
    return jsonEncode({
      "city": city,
      "street": street,
      "house": house,
      "flat": flat,
      "level": level,
      "extra_info": extraInfo,
    });
  }
}
