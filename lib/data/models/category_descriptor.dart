import 'package:flutter/material.dart';

class CategoryDescriptor {
  final int id;
  final String image;
  final String name;
  final int productCount;
  final int subCategoryCount;

  CategoryDescriptor({
    @required this.id,
    this.image,
    this.productCount = 0,
    this.subCategoryCount = 0,
    @required this.name,
  })  : assert(id != null && id >= 0),
        assert(productCount >= 0),
        assert(name != null && name.isNotEmpty);

  bool get hasImage{
    return image != null && image.isNotEmpty;
  }
}
