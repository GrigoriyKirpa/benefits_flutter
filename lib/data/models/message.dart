import 'package:benefit_flutter/data/models/user.dart';
import 'package:flutter/cupertino.dart';

class Message {
  final User sender;
  final String text;
  final DateTime date;

  Message({@required this.sender, @required this.text, @required this.date})
      : assert(sender != null),
        assert(text != null && text.isNotEmpty),
        assert(date != null &&
            date.millisecondsSinceEpoch <=
                DateTime.now().millisecondsSinceEpoch);
}
