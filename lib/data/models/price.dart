import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Price {
  final double basePrice;
  final double discountedPrice;

  Price({@required this.basePrice, this.discountedPrice})
      : assert(basePrice != null && basePrice >= 0),
        assert(discountedPrice == null ? true : discountedPrice <= basePrice);

  double get price {
    return discountedPrice ?? basePrice;
  }

  bool get isDiscounted {
    return discountedPrice != null;
  }

  String getPriceString(BuildContext context) {
    return doubleToPriceString(context, price);
  }

  static String doubleToPriceString(BuildContext context, double price) {
    final List pieces = price.toStringAsFixed(2).split('.');

    return "${splitThousands(pieces[0])}.${pieces[1]} ${currency(context)}";
  }

  static double getTotalPrice(List<Price> prices) {
    double totalPrice = 0;
    prices.forEach((element) {
      totalPrice += element.price;
    });
    return totalPrice;
  }

  static String getTotalPriceString(BuildContext context, List<Price> prices) {
    double totalPrice = 0;
    prices.forEach((element) {
      totalPrice += element.price;
    });
    return "${splitThousands(totalPrice.toString())} ${currency(context)}";
  }

  static String currency(BuildContext context) {
    final Locale locale = Localizations.localeOf(context);
    final format = NumberFormat.simpleCurrency(locale: locale.toString());
    return format.currencySymbol;
  }

  static String splitThousands(String number) {
    if (number.length > 3) {
      final String before = number.substring(0, number.length - 3);
      final String after = number.substring(number.length - 3, number.length);
      return '${splitThousands(before)} $after';
    }
    return number;
  }
}
