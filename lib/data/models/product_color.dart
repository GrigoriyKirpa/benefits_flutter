import 'package:flutter/cupertino.dart';

class ProductColor {
  final String name;
  final String code;
  final ImageProvider color;

  ProductColor({@required this.name, @required this.color, @required this.code})
      : assert(name != null && name.isNotEmpty),
        assert(code != null && code.isNotEmpty),
        assert(code != null);
}
