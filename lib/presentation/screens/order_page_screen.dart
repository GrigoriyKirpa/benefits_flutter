import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/presentation/screens/documents_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/order_item_list_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

class OrderPageScreen extends StatefulWidget {
  final Order order;
  static const routeName = "/order_page";

  OrderPageScreen({Key key, @required this.order})
      : assert(order != null),
        super(key: key);

  @override
  _OrderPageScreenState createState() => _OrderPageScreenState();
}

class _OrderPageScreenState extends State<OrderPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
        context,
        "${FlutterI18n.translate(context, "order.page.title")} #${widget.order.id.toString()}",
        /*Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "${FlutterI18n.translate(context, "order.page.title")} #${widget.order.id.toString()}",
              style: Theme.of(context).textTheme.headline1,
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                CustomIcons.open_in_new,
                color: customBlueColor,
                size: 28,
              ),
            ),
          ],
        ),*/
      ),
      body: Column(
        children: [
          Space.vertical(28),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 11, horizontal: 27),
            child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomWidgets.totalPriceWidget(
                      context,
                      widget.order.payment.total,
                      Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 14),
                    ),
                    Text(
                      "${widget.order.items.length} ${FlutterI18n.plural(
                        context,
                        "order.items.number",
                        Language.getPlural(widget.order.items.length),
                      )}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 12, color: customTextGrayColor1),
                    ),
                  ],
                ),
                Spacer(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      DateFormat("dd.MM.yyyy").format(widget.order.date),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 12, color: customTextGrayColor1),
                    ),
                  ],
                ),
              ],
            ),
          ),
          RawMaterialButton(
            onPressed: () => Navigator.of(context, rootNavigator: true)
                .pushNamed(DocumentsScreen.routeName,
                    arguments: widget.order.images),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 11, horizontal: 24),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(width: 0.5, color: customGrayColor1),
                  bottom: BorderSide(width: 0.5, color: customGrayColor1),
                ),
              ),
              child: Row(
                children: [
                  Text(
                    FlutterI18n.translate(context, "order.documents.title"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                  Spacer(),
                  Text(
                    widget.order.images.length.toString(),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(color: customBlueColor),
                  ),
                  Space.horizontal(4),
                  Icon(
                    Icons.arrow_forward_ios_rounded,
                    size: 12,
                    color: customTextGrayColor1,
                  ),
                ],
              ),
            ),
          ),
          Space.vertical(3),
          Flexible(
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 11),
              child: ListView.builder(
                itemCount: widget.order.items.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(vertical: 11),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 0.5,
                          color: customGrayColor1,
                        ),
                      ),
                    ),
                    child: OrderItemListWidget(
                      item: widget.order.items[index],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
