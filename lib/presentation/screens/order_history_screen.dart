import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/order_list_item_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class OrderHistoryScreen extends StatefulWidget {
  @override
  _OrderHistoryScreenState createState() => _OrderHistoryScreenState();
}

class _OrderHistoryScreenState extends State<OrderHistoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          appBar: CustomWidgets.appbar(
            context,
            Row(
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  child: Center(
                    child: Image.asset(
                      imageAssetBuilder('appBar.png'),
                    ),
                  ),
                ),
                Text(
                  FlutterI18n.translate(context, "user.order_history.page.title"),
                  style: Theme.of(context).textTheme.headline1,
                ),
                Flexible(
                  fit: FlexFit.tight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      /*IconButton(
                        onPressed: () => null,
                        icon: Icon(
                          Icons.add,
                          color: customRedColor,
                          size: 28,
                        ),
                      ),*/
                    ],
                  ),
                ),
              ],
            ),
            true,
          ),
          body: FutureBuilder(
            future: ServerRequests.getOrders(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final List<Order> _orders = snapshot.data;
                return Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 11,
                    vertical: 8,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Space.vertical(16),
                      Flexible(
                          child: ListView.builder(
                        itemCount: _orders.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 11.0),
                            child: OrderListItemWidget(
                              order: _orders.reversed.toList()[index],
                              onOrderRepeatPressed: () =>
                                  ServerRequests.repeatOrder(
                                          _orders.reversed.toList()[index].id)
                                      .then(
                                (value) => setState(() {}),
                              ),
                            ),
                          );
                        },
                      )),
                    ],
                  ),
                );
              } else if (snapshot.hasError) {
                return Center(
                  child: Text(
                    FlutterI18n.translate(context, "error.no_service"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(color: customGrayColor1),
                    textAlign: TextAlign.center,
                  ),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ),
      ),
    );
  }
}
