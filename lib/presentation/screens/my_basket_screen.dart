import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/coupon_statuses.dart';
import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/payment.dart';
import 'package:benefit_flutter/data/models/price.dart';
import 'package:benefit_flutter/data/models/promo_code.dart';
import 'package:benefit_flutter/data/models/recipient.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/address_book_screen.dart';
import 'package:benefit_flutter/presentation/screens/recipient_registration_screen.dart';
import 'package:benefit_flutter/presentation/widgets/basket_list_item.dart';
import 'package:benefit_flutter/presentation/widgets/circled_checkbox.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:screen_loader/screen_loader.dart';

class MyBasketScreen extends StatefulWidget {
  static const routeName = "/my_basket_screen";

  @override
  _MyBasketScreenState createState() => _MyBasketScreenState();
}

class _MyBasketScreenState extends State<MyBasketScreen> with ScreenLoader {
  final TextEditingController _promoCodeController = TextEditingController();
  PromoCode _promoCode;
  List<OrderItem> _items;
  double _total;
  Address _currentAddress;
  bool _sendReceiptToMail = false;
  bool _pickUp = false;

  @override
  void dispose() {
    _promoCodeController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _currentAddress = Shop.currentUser.currentAddress;
    super.initState();
  }

  Future<List<OrderItem>> initBasket() async {
    await Shop.updateServerData();
    return Shop.basket.items;
  }

  double calculateTotal() {
    double total = 0;
    for (OrderItem item in _items) {
      total += item.totalPrice;
    }
    return total;
  }

  void reloadItems() {
    setState(() {
      _items = Shop.basket.items;
      _total = calculateTotal();
    });
  }

  void reloadAddress() {
    setState(() {
      _currentAddress = Shop.currentUser.currentAddress;
    });
  }

  void onItemRemoved(OrderItem item) {
    Shop.basket.remove(item);
    reloadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: ScaffoldMessenger(
          key: UniqueKey(),
          child: loadableWidget(
            child: Scaffold(
              appBar: CustomWidgets.appbar(
                context,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                            imageAssetBuilder('appBar.png'),
                          ),
                        ],
                      ),
                    ),
                    Text(
                      FlutterI18n.translate(context, "basket.text"),
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    Space.filler(),
                  ],
                ),
                true,
              ),
              body: _items == null
                  ? FutureBuilder(
                      future: initBasket(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          _items = snapshot.data;
                          _total = calculateTotal();
                          return body;
                        } else if (snapshot.hasError) {
                          return Center(
                            child: Text(
                              FlutterI18n.translate(
                                  context, "error.no_service"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .copyWith(color: customGrayColor1),
                              textAlign: TextAlign.center,
                            ),
                          );
                        } else {
                          return Center(child: CircularProgressIndicator());
                        }
                      },
                    )
                  : body,
            ),
          ),
        ),
      ),
    );
  }

  Future<PromoCode> openPromoResult(String code) async {
    final Widget alert = AlertDialog(
      contentPadding: const EdgeInsets.all(14),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      content: FutureBuilder(
        future: ServerRequests.checkCoupon(code),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data is PromoCode) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.check,
                    color: customBlueColor,
                    size: 64,
                  ),
                  Space.vertical(8),
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: CouponStatuses.Ok.label(context),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(fontSize: 16),
                        ),
                        TextSpan(
                          text: (snapshot.data as PromoCode).name,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                  Space.vertical(8),
                  OutlinedButton(
                    onPressed: () => Navigator.of(context, rootNavigator: true)
                        .pop(snapshot.data as PromoCode),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 36, vertical: 5),
                      child: Text(
                        FlutterI18n.translate(
                            context, "action.continue.default"),
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(fontSize: 14, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              );
            } else {
              final CouponStatuses status = snapshot.data;
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.clear,
                    size: 64,
                    color: customRedColor,
                  ),
                  Space.vertical(8),
                  Text(
                    status.label(context),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontSize: 16,
                        ),
                  ),
                  Space.vertical(8),
                  OutlinedButton(
                    onPressed: () =>
                        Navigator.of(context, rootNavigator: true).pop(null),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 36, vertical: 5),
                      child: Text(
                        FlutterI18n.translate(
                            context, "action.continue.default"),
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(fontSize: 14, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              );
            }
          } else if (snapshot.hasError) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Center(
                  child: Text(
                    FlutterI18n.translate(context, "error.no_service"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(color: customGrayColor1),
                    textAlign: TextAlign.center,
                  ),
                ),
                Space.vertical(8),
                OutlinedButton(
                  onPressed: () =>
                      Navigator.of(context, rootNavigator: true).pop(null),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 36, vertical: 5),
                    child: Text(
                      FlutterI18n.translate(context, "action.continue.default"),
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 14, color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          } else {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 22.0, vertical: 16),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ],
            );
          }
        },
      ),
    );

    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
      barrierDismissible: false,
    );
  }

  Future<String> openPromoInput() async {
    final Widget alert = AlertDialog(
      contentPadding: const EdgeInsets.all(14),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () =>
                    Navigator.of(context, rootNavigator: true).pop(null),
                child: Icon(
                  Icons.close_rounded,
                  size: 16,
                  color: customGrayColor1,
                ),
              ),
            ],
          ),
          Flexible(
            child: FormFields.simpleTextField(
              context,
              FlutterI18n.translate(context, "order.promo_code.input"),
              _promoCodeController,
              maxLength: 50,
              disableHelpText: true,
            ),
          ),
          Space.vertical(16),
          OutlinedButton(
            onPressed: () => Navigator.of(context, rootNavigator: true)
                .pop(_promoCodeController.text),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 36, vertical: 5),
              child: Text(
                FlutterI18n.translate(context, "action.continue.default"),
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 14, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );

    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  bool get buyButtonNotActive {
    return _items.isEmpty || (_currentAddress == null && !_pickUp);
  }

  Widget get body {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 11),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Space.vertical(8),
          GestureDetector(
            onTap: () => Navigator.of(context)
                .pushNamed(AddressBookScreen.routeName)
                .then((_) => reloadAddress()),
            child: Row(
              children: _currentAddress == null
                  ? [
                      Icon(
                        Icons.add,
                        size: 18,
                        color: customRedColor,
                      ),
                      Space.horizontal(10),
                      Text(
                        FlutterI18n.translate(context, "action.add.address"),
                        style: Theme.of(context)
                            .textTheme
                            .headline1
                            .copyWith(color: customRedColor, fontSize: 14),
                      ),
                    ]
                  : [
                      Text(
                        FlutterI18n.translate(
                            context, "user.address.your_address"),
                        style: Theme.of(context).textTheme.headline5,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Space.horizontal(11),
                      Flexible(
                        child: Text(
                          _currentAddress.buildAddress(context),
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              .copyWith(fontSize: 14, color: customBlueColor),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
            ),
          ),
          Space.vertical(4),
          Flexible(
            child: ListView.builder(
              itemCount: _items.length,
              itemBuilder: (context, index) {
                OrderItem item = _items[index];
                return Slidable(
                  key: UniqueKey(),
                  actionPane: SlidableScrollActionPane(),
                  actionExtentRatio: 0.20,
                  secondaryActions: [
                    IconSlideAction(
                      caption: FlutterI18n.translate(
                          context, "action.delete.basket"),
                      color: customRedColor,
                      icon: Icons.delete,
                      foregroundColor: Colors.white,
                      onTap: () => onItemRemoved(item),
                    ),
                    IconSlideAction(
                      caption: FlutterI18n.translate(context, "favourites.add"),
                      color: customBlueColor,
                      foregroundColor: Colors.white,
                      icon: CustomIcons.like,
                      onTap: () async {
                        await Shop.currentUser
                            .handleLike(item.product)
                            .then((value) => reloadItems());
                      },
                    ),
                  ],
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 16),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 0.5,
                          color: customGrayColor1,
                        ),
                      ),
                    ),
                    child: BasketListItem(
                      item: item,
                      onItemDeleted: () => onItemRemoved(item),
                      onAmountChanged: () async =>
                          await ServerRequests.basketUpdate(item).then(
                        (value) => setState(() {
                          _total = calculateTotal();
                        }),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 65.0, right: 65, bottom: 28, top: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width - 151,
                    height: 120,
                    child: Column(
                      children: [
                        Space.vertical(4),
                        Row(
                          children: [
                            Icon(
                              CustomIcons.promo,
                              color: customBlueColor,
                              size: 23,
                            ),
                            Space.horizontal(7),
                            Text(
                              FlutterI18n.translate(
                                  context, "order.promo_code.title"),
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            Spacer(),
                            if (_promoCode == null)
                              GestureDetector(
                                onTap: () => openPromoInput().then((value) {
                                  if (value != null && (value).isNotEmpty) {
                                    openPromoResult(value).then(
                                      (value) => setState(() {
                                        _promoCode = value;
                                      }),
                                    );
                                  }
                                }),
                                child: Icon(
                                  Icons.more_horiz_outlined,
                                  color: Colors.black,
                                ),
                              ),
                            if (_promoCode != null) ...[
                              Text(
                                _promoCode.code,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(fontSize: 14),
                              ),
                              GestureDetector(
                                onTap: () => setState(() {
                                  _promoCode = null;
                                }),
                                child: Icon(
                                  Icons.cancel_rounded,
                                  color: customGrayColor1,
                                  size: 16,
                                ),
                              ),
                            ],
                          ],
                        ),
                        // Space.vertical(8),
                        // Row(
                        //   children: [
                        //     Text(
                        //       FlutterI18n.translate(
                        //           context, "order.send_receipt_to_mail"),
                        //       style: Theme.of(context).textTheme.headline5,
                        //     ),
                        //     Spacer(),
                        //     CircledCheckBox(
                        //       initialValue: _sendReceiptToMail,
                        //       onChecked: (checked) =>
                        //           _sendReceiptToMail = checked,
                        //     ),
                        //   ],
                        // ),
                        Space.vertical(11),
                        Row(
                          children: [
                            Text(
                              FlutterI18n.translate(context, "order.pick_up"),
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            Spacer(),
                            CircledCheckBox(
                              initialValue: _pickUp,
                              onChecked: (checked) => setState(() {
                                _pickUp = checked;
                              }),
                            ),
                          ],
                        ),
                        Space.vertical(11),
                        Row(
                          children: [
                            Text(
                              FlutterI18n.translate(context, "order.total"),
                              style: Theme.of(context).textTheme.headline1,
                            ),
                            Spacer(),
                            CustomWidgets.totalPriceWidget(
                              context,
                              _total,
                              Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(color: customRedColor),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Center(
            key: UniqueKey(),
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) {
                    if (buyButtonNotActive) return customGrayColor1;
                    return customBlueColor;
                  },
                ),
              ),
              onPressed: buyButtonNotActive
                  ? null
                  : () async => await this.performFuture(registerOrder),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 9),
                width: MediaQuery.of(context).size.width - 151,
                child: Center(
                  child: Text(
                    FlutterI18n.translate(context, "order.registration.button"),
                  ),
                ),
              ),
            ),
          ),
          Space.vertical(33),
        ],
      ),
    );
  }

  Future<void> registerOrder() async {
    final Price price = await ServerRequests.getBasketPrice(
      _promoCode != null ? _promoCode.code : _promoCode,
    );
    if (price == null) {
      System.displayErrorSnackBar(context,
          errorLocaleKey: "error.basket_price_not_in_sync");
      return;
    }
    Navigator.of(context, rootNavigator: true)
        .pushNamed(RecipientRegistrationScreen.routeName,
            arguments: Order(
              items: _items,
              payment: Payment(
                price: price,
              ),
              recipient: Recipient(
                address: _currentAddress,
                pickUp: _pickUp,
              ),
              promoCode: _promoCode,
            ));
  }
}
