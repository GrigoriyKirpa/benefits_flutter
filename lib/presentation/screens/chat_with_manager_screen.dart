import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/message.dart';
import 'package:benefit_flutter/data/models/phone.dart';
import 'package:benefit_flutter/data/models/user.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

class ChatWithManagerScreen extends StatefulWidget {
  static const routeName = "/chat_with_manager";

  @override
  _ChatWithManagerScreenState createState() => _ChatWithManagerScreenState();
}

class _ChatWithManagerScreenState extends State<ChatWithManagerScreen> {
  final User _user = Shop.currentUser;
  final TextEditingController _controller = TextEditingController();
  User _manager;
  List<Message> _messages;

  @override
  void initState() {
    _manager = User(
      info: UserInfo(
        id: 1,
        fullName: "Some dude",
        phone: Phone(
          countryCode: "+7",
          phone: "1231231212",
        ),
      ),
    );
    _messages = [
      Message(
          sender: _user,
          text:
              "Как дела? Тексттекст Тексттекст. Тексттекст Тексттекст. Тексттекст Тексттекст Тексттекст Тексттекст Тексттекст Тексттекст",
          date: DateTime.now()),
      Message(sender: _user, text: "Здравствуйте?", date: DateTime.now()),
      Message(sender: _manager, text: "Чем могу помочь?", date: DateTime.now()),
    ];
    super.initState();
  }

  void sendMessage() {
    if (_controller.text.isEmpty) {
      return;
    }
    setState(() {
      _messages.insert(0,
          Message(sender: _user, text: _controller.text, date: DateTime.now()));
      _controller.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          appBar: CustomWidgets.appbar(
            context,
            Center(
              child: Column(
                children: [
                  Text(
                    FlutterI18n.translate(
                        context, "user.chat_with_manager.title"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(color: customGrayColor1),
                  ),
                  Text(
                    _manager.info.fullName,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ],
              ),
            ),
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 27),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: ListView.builder(
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: _messages.length,
                    itemBuilder: (context, index) {
                      final Message message = _messages[index];
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Align(
                          alignment: message.sender == _user
                              ? Alignment.topRight
                              : Alignment.topLeft,
                          child: Stack(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: Column(
                                  crossAxisAlignment: message.sender == _user
                                      ? CrossAxisAlignment.end
                                      : CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 5,
                                      width: 3,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: message.sender == _user
                                            ? customRedColor
                                            : customBlueColor,
                                      ),
                                    ),
                                    Space.vertical(2),
                                    Container(
                                      width: 74,
                                      height: 25,
                                      color: message.sender == _user
                                          ? customRedColor
                                          : customBlueColor,
                                      child: Center(
                                        child: Text(
                                          FlutterI18n.translate(context,
                                              "user.chat_with_manager.message_sender.${message.sender == _user ? 'user' : 'manager'}"),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .copyWith(color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0, vertical: 6),
                                      child: Text(
                                        message.text,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .copyWith(fontSize: 12),
                                        textAlign: message.sender == _user
                                            ? TextAlign.right
                                            : TextAlign.left,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 6),
                                      child: Text(
                                        DateFormat("dd/MM/yy, hh:mm")
                                            .format(message.date),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .copyWith(
                                                fontSize: 12,
                                                color: customGrayColor1),
                                        textAlign: message.sender == _user
                                            ? TextAlign.right
                                            : TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                right: message.sender == _user ? 0 : null,
                                left: message.sender != _user ? 0 : null,
                                top: 7,
                                height: 87,
                                child: Container(
                                  width: 1,
                                  color: message.sender == _user
                                      ? customRedColor
                                      : customBlueColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: TextField(
                          textCapitalization: TextCapitalization.sentences,
                          controller: _controller,
                          style: Theme.of(context).textTheme.headline5,
                          maxLines: 4,
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                            helperStyle: TextStyle(fontSize: 0),
                            fillColor: Colors.white,
                            filled: true,
                            hintText: FlutterI18n.translate(
                                context, "user.chat_with_manager.input_hint"),
                            hintStyle: Theme.of(context)
                                .textTheme
                                .headline5
                                .copyWith(color: customGrayColor1),
                            border: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                          ),
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      Space.horizontal(15),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            iconSize: 32,
                            onPressed: sendMessage,
                            icon: Icon(
                              Icons.send,
                              color: customRedColor,
                            ),
                          ),
                          IconButton(
                            iconSize: 20,
                            onPressed: () {},
                            icon: Icon(
                              CustomIcons.paper_clip,
                              color: customTextGrayColor2,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
