import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/replacement_options.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/models/phone.dart';
import 'package:benefit_flutter/data/models/price.dart';
import 'package:benefit_flutter/data/models/recipient.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/screens/payment_selection_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/replacement_option_selector.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

/*
class RecipientRegistrationOptions {
  final OrderBuilder builder;

  RecipientRegistrationOptions({@required this.builder})
      : assert(builder != null);
}
*/
class RecipientRegistrationScreen extends StatefulWidget {
  static const routeName = "/order_registration";
  final Order order;

  RecipientRegistrationScreen({Key key, @required this.order})
      : assert(order != null &&
            order.payment.price != null &&
            (order.recipient.address != null || order.recipient.pickUp)),
        super(key: key);

  @override
  _RecipientRegistrationScreenState createState() =>
      _RecipientRegistrationScreenState();
}

class _RecipientRegistrationScreenState
    extends State<RecipientRegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _commentController = TextEditingController();
  ReplacementOptions _option = defaultOption;
  CountryCode _currentCountryCode;
  double _delivery;

  @override
  void initState() {
    _delivery = widget.order.payment.price.price >= 5000 ||
            widget.order.recipient.pickUp
        ? 0
        : 350;
    if (Shop.currentUser.info != null) {
      if (Shop.currentUser.info.fullName != null &&
          Shop.currentUser.info.fullName.isNotEmpty) {
        _nameController.text = Shop.currentUser.info.fullName;
      }
      if (Shop.currentUser.info.email != null &&
          Shop.currentUser.info.email.isNotEmpty) {
        _emailController.text = Shop.currentUser.info.email;
      }
      if (Shop.currentUser.info.phone != null &&
          Shop.currentUser.info.phone.phone != null &&
          Shop.currentUser.info.phone.phone.isNotEmpty) {
        _phoneController.text = Shop.currentUser.info.phone.phone;
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
        context,
        FlutterI18n.translate(context, "order.registration.page.title"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 27.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(22),
              Text(
                FlutterI18n.translate(
                    context, "order.registration.page.recipient"),
                style: Theme.of(context).textTheme.headline5,
              ),
              Space.vertical(8),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CountryCodePicker(
                            onInit: (code) => _currentCountryCode = code,
                            onChanged: (code) => _currentCountryCode = code,
                            initialSelection: "RU",
                            // optional. Shows only country name and flag
                            showCountryOnly: false,
                            // optional. Shows only country name and flag when popup is closed.
                            showOnlyCountryWhenClosed: false,
                            padding: EdgeInsets.zero,
                            flagWidth: 18,
                          ),
                          Flexible(
                            child: FormFields.compulsoryTextField(
                              context,
                              FlutterI18n.translate(context,
                                  "order.registration.page.form.phone_field"),
                              _phoneController,
                              validator: (text) =>
                                  FormFields.phoneNumberValidator(
                                      context, text),
                              maxLength: 15,
                              formatters: [FormFields.maskPhoneFormatter],
                              disableHelpText: true,
                              type: TextInputType.phone,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "order.registration.page.form.name_field"),
                      _nameController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                      maxLength: 30,
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "order.registration.page.form.email_field"),
                      _emailController,
                      validator: (text) =>
                          FormFields.emailFieldValidator(context, text),
                      maxLength: 40,
                    ),
                  ],
                ),
              ),
              Space.vertical(8),
              Text(
                FlutterI18n.translate(
                    context, "order.registration.page.email_info"),
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontSize: 12, color: customTextGrayColor1),
              ),
              Space.vertical(11),
              Text(
                FlutterI18n.translate(
                    context, "order.registration.page.replacement.title"),
                style: Theme.of(context).textTheme.headline5,
              ),
              Text(
                FlutterI18n.translate(
                    context, "order.registration.page.replacement.info"),
                style: Theme.of(context).textTheme.headline5.copyWith(
                    color: Colors.black.withOpacity(0.5), fontSize: 12),
              ),
              Space.vertical(10),
              SizedBox(
                width: MediaQuery.of(context).size.width - 54,
                child: ReplacementOptionSelector(
                  initialOption: _option,
                  onChanged: (option) => _option = option,
                ),
              ),
              Space.vertical(14),
              FormFields.simpleTextField(
                context,
                FlutterI18n.translate(
                    context, "order.registration.page.comment_hint"),
                _commentController,
                maxLength: 40,
              ),
              Space.vertical(20),
              Row(
                children: [
                  Text(
                    FlutterI18n.translate(context,
                        "order.registration.page.price_info.product_price"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                  Spacer(),
                  Text(
                    widget.order.payment.price.basePrice.toStringAsFixed(2),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 12),
                  ),
                ],
              ),
              Space.vertical(4),
              if (widget.order.payment.price.price !=
                  widget.order.payment.price.basePrice) ...[
                Row(
                  children: [
                    Text(
                      FlutterI18n.translate(context,
                          "order.registration.page.price_info.promo_discount_price"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 12),
                    ),
                    Spacer(),
                    Text(
                      widget.order.payment.price.price.toStringAsFixed(2),
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(fontSize: 12),
                    ),
                  ],
                ),
                Space.vertical(4),
              ],
              Row(
                children: [
                  Text(
                    FlutterI18n.translate(
                        context, "order.registration.page.price_info.delivery"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                  Spacer(),
                  Text(
                    _delivery.toStringAsFixed(2),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 12),
                  ),
                ],
              ),
              Space.vertical(4),
              Row(
                children: [
                  Text(
                    FlutterI18n.translate(
                        context, "order.registration.page.price_info.total"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                  Spacer(),
                  Text(
                    (widget.order.payment.price.price + _delivery)
                        .toStringAsFixed(2),
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .copyWith(fontSize: 12),
                  ),
                ],
              ),
              Space.vertical(28),
              Center(
                child: OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      widget.order.payment.delivery =
                          Price(basePrice: _delivery);
                      widget.order.recipient = Recipient(
                        phone: Phone(
                          countryCode: _currentCountryCode.toString(),
                          phone: FormFields.unMask(_phoneController.text),
                        ),
                        email: _emailController.text,
                        name: _nameController.text,
                        option: _option,
                        address: widget.order.recipient.address,
                        pickUp: widget.order.recipient.pickUp,
                        comment: _commentController.text,
                      );
                      Navigator.of(context, rootNavigator: true).pushNamed(
                        PaymentSelectionScreen.routeName,
                        arguments: widget.order,
                      );
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 44, vertical: 10),
                    child: Text(
                      FlutterI18n.translate(context, "action.confirm.order"),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
