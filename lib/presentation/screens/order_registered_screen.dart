import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/payment.dart';
import 'package:benefit_flutter/presentation/screens/main_navigation_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class OrderRegisteredScreen extends StatelessWidget {
  static const routeName = "/order_registered";
  final Payment payment;

  OrderRegisteredScreen({Key key, @required this.payment})
      : assert(payment != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          appBar: CustomWidgets.appbar(
            context,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.popAndPushNamed(
                        context, MainNavigationScreen.routeName);
                  },
                  icon: Icon(
                    Icons.close_rounded,
                    size: 34,
                    color: customGrayColor1,
                  ),
                ),
              ],
            ),
            true,
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Space.vertical(30),
                Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Icon(
                      Icons.check,
                      color: customRedColor,
                      size: 64,
                    ),
                  ),
                ),
                Spacer(
                  flex: 10,
                ),
                CustomWidgets.totalPriceWidget(
                    context,
                    payment.total,
                    Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 24)),
                Space.vertical(13),
                Text(
                  FlutterI18n.translate(context, "order.registration.finished"),
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontSize: 18),
                ),
                // Space.vertical(13),
                // GestureDetector(
                //   onTap: () {},
                //   child: Text(
                //     FlutterI18n.translate(context, "order.receipt"),
                //     style: Theme.of(context).textTheme.headline5.copyWith(
                //         decoration: TextDecoration.underline, color: lightBlack),
                //   ),
                // ),
                Spacer(
                  flex: 40,
                ),
                Center(
                  child: OutlinedButton(
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true)
                          .popUntil((route) => route.isFirst);
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 9, horizontal: 45),
                      child: Text(
                        FlutterI18n.translate(context, "action.back.to_menu"),
                      ),
                    ),
                  ),
                ),
                Spacer(
                  flex: 25,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
