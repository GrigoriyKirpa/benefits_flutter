import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/main_navigation.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/screens/catalog_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_editor_screen.dart';
import 'package:benefit_flutter/presentation/widgets/cancel_icon_button.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/product_list_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SpecificationScreen extends StatefulWidget {
  final Specification specification;
  static const routeName = "/specification";

  SpecificationScreen({
    Key key,
    @required this.specification,
  })  : assert(specification != null),
        super(key: key);

  @override
  _SpecificationScreenState createState() => _SpecificationScreenState();
}

class _SpecificationScreenState extends State<SpecificationScreen> {
  List<ProductDescriptor> _products;

  @override
  void initState() {
    _products = widget.specification.products;
    super.initState();
  }

  void reloadProducts() {
    if(!mounted) return;
    setState(() {
      _products = widget.specification.products;
    });
  }

  List<Widget> get _body {
    if (_products.isNotEmpty) {
      return _notEmptyBody;
    } else {
      return _emptyBody;
    }
  }

  List<Widget> get _emptyBody {
    return [
      Spacer(flex: 1),
      Padding(
        padding: EdgeInsets.only(
          left: 0.182 * MediaQuery.of(context).size.width,
          right: 0.182 * MediaQuery.of(context).size.width,
        ),
        child: Text(
          FlutterI18n.translate(context, "user.specification.empty"),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
      Spacer(flex: 2),
    ];
  }

  List<Widget> get _notEmptyBody {
    return [
      Space.vertical(32),
      Flexible(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 11.0),
          child: ListView.builder(
            clipBehavior: Clip.antiAlias,
            itemCount: _products.length,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: customGrayColor1,
                    ),
                  ),
                ),
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ProductListWidget(
                  product: _products[index],
                  onSpecificationHandled: reloadProducts,
                  onItemPressed: () => Navigator.pop(context),
                ),
              );
            },
          ),
        ),
      ),
      Space.vertical(78),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, VoidCallback> optionMap = {
      FlutterI18n.translate(context, "action.delete.specification"): () {
        Shop.currentUser.deleteSpecification(widget.specification);
        Navigator.pop(context);
      },
      FlutterI18n.translate(context, "action.edit.specification"): () {
        Navigator.of(context, rootNavigator: true)
            .pushNamed(SpecificationEditorScreen.routeName,
                arguments: SpecificationEditorScreenArguments(
                  specification: widget.specification,
                  onCreatePressed: (specification) {
                    final int index = Shop.currentUser.specifications
                        .indexOf(widget.specification);
                    Shop.currentUser.edit(specification, index);
                    Navigator.pop(context);
                  },
                ))
            .then(
          (_) {
            if(mounted)
              setState(() {});
          },
        );
      },
    };

    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          appBar: CustomWidgets.customAppbar(
            72,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Space.horizontal(14),
                PopupMenuButton(
                  onSelected: (value) => optionMap[value](),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(7.0))),
                  offset: Offset.fromDirection(1.0, 42),
                  icon: Icon(
                    Icons.more_horiz_outlined,
                    size: 32,
                    color: customGrayColor1,
                  ),
                  itemBuilder: (BuildContext context) =>
                      optionMap.keys.map<PopupMenuItem<String>>((String value) {
                    return PopupMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    );
                  }).toList(),
                ),
                Spacer(),
                CancelIconButton(),
                Space.horizontal(14),
              ],
            ),
            false,
          ),
          body: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 0.182 * MediaQuery.of(context).size.width,
                    right: 0.182 * MediaQuery.of(context).size.width,
                  ),
                  child: Text(
                    widget.specification.name,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                if (widget.specification.description.isNotEmpty) ...[
                  Space.vertical(16),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 0.182 * MediaQuery.of(context).size.width,
                      right: 0.182 * MediaQuery.of(context).size.width,
                    ),
                    child: Text(
                      "${FlutterI18n.translate(context, "user.specification.description")}\n${widget.specification.description}",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                ],
                ..._body,
              ],
            ),
          ),
          floatingActionButton: Container(
            width: MediaQuery.of(context).size.width,
            height: 43,
            padding: EdgeInsets.only(
              left: 0.182 * MediaQuery.of(context).size.width,
              right: 0.182 * MediaQuery.of(context).size.width,
            ),
            margin: EdgeInsets.only(
              bottom: _products.isEmpty
                  ? 0.164 * MediaQuery.of(context).size.height
                  : 18,
            ),
            child: FloatingActionButton(
              onPressed: () {
                Navigator.popUntil(context, (route) => route.isFirst);
                MainNavigationExtension.currentTab.navigationKey.currentState
                    .pushNamed(CatalogScreen.routeName);
              },
              child: Center(
                child: Text(
                  FlutterI18n.translate(context, "action.add.product.plural"),
                ),
              ),
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ),
      ),
    );
  }
}
