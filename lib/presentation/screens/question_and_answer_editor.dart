import 'package:benefit_flutter/data/models/message.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class QuestionAndAnswerEditor extends StatefulWidget {
  final Function(Message) onSentPressed;
  static const routeName = "/question_and_answer_editor";

  QuestionAndAnswerEditor({Key key, Function(Message) onSentPressed})
      : this.onSentPressed = onSentPressed ?? ((_) => null),
        super(key: key);

  @override
  _QuestionAndAnswerEditorState createState() =>
      _QuestionAndAnswerEditorState();
}

class _QuestionAndAnswerEditorState extends State<QuestionAndAnswerEditor> {
  final TextEditingController _controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomWidgets.appbarWithText(context,
          FlutterI18n.translate(context, "action.leave.question_and_answer")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 26),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: Space.vertical(50),
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                validator: (text) {
                  return text == null || text.isEmpty
                      ? FlutterI18n.translate(context, "form.compulsory_field")
                      : null;
                },
                maxLength: 150,
                maxLines: 7,
                textCapitalization: TextCapitalization.sentences,
                controller: _controller,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontSize: 22),
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                  hintText: FlutterI18n.translate(
                      context, "question_and_answer_editor.input_hint"),
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                keyboardType: TextInputType.text,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width,
        height: 43,
        padding: EdgeInsets.only(
          left: 0.182 * MediaQuery.of(context).size.width,
          right: 0.182 * MediaQuery.of(context).size.width,
        ),
        margin: EdgeInsets.only(
          bottom: 56 - MediaQuery.of(context).viewInsets.bottom > 0
              ? 56 - MediaQuery.of(context).viewInsets.bottom
              : MediaQuery.of(context).viewInsets.bottom - 50,
        ),
        child: FloatingActionButton(
          onPressed: () {
            final bool result = _formKey.currentState.validate();
            if (result) {
              widget.onSentPressed(
                Message(
                  sender: Shop.currentUser,
                  date: DateTime.now(),
                  text: _controller.text,
                ),
              );
              Navigator.pop(context);
            }
          },
          child: Center(
            child: Text(
              FlutterI18n.translate(context, "action.send.default"),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
