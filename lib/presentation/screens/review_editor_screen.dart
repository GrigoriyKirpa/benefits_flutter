import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ReviewEditorScreen extends StatefulWidget {
  final Function(Review) onReviewCreated;
  static const routeName = "/review_editor";

  ReviewEditorScreen({Key key, Function(Review) onReviewCreated})
      : this.onReviewCreated = onReviewCreated ?? ((_) => {}),
        super(key: key);

  @override
  _ReviewEditorScreenState createState() => _ReviewEditorScreenState();
}

class _ReviewEditorScreenState extends State<ReviewEditorScreen> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomWidgets.appbarWithText(
          context, FlutterI18n.translate(context, "action.leave.review")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 26),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
              child: Space.vertical(46),
            ),
            TextFormField(
              maxLength: 150,
              maxLines: 7,
              textCapitalization: TextCapitalization.sentences,
              controller: _controller,
              style:
                  Theme.of(context).textTheme.headline5.copyWith(fontSize: 22),
              decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(5.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(5.0),
                ),
                hintText:
                    FlutterI18n.translate(context, "review_screen.input_hint"),
              ),
              keyboardType: TextInputType.text,
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width,
        height: 43,
        padding: EdgeInsets.only(
          left: 0.182 * MediaQuery.of(context).size.width,
          right: 0.182 * MediaQuery.of(context).size.width,
        ),
        margin: EdgeInsets.only(
          bottom: 56 - MediaQuery.of(context).viewInsets.bottom > 0
              ? 56 - MediaQuery.of(context).viewInsets.bottom
              : MediaQuery.of(context).viewInsets.bottom - 50,
        ),
        child: FloatingActionButton(
          onPressed: () {
            widget.onReviewCreated(
              Review(
                id: Shop.currentUser.info.id,
                name: Shop.currentUser.info.fullName == null ||
                        Shop.currentUser.info.fullName.isEmpty
                    ? FlutterI18n.translate(
                        context, "review_screen.default_name")
                    : Shop.currentUser.info.fullName,
                date: DateTime.now(),
                text: _controller.text,
              ),
            );
            Navigator.pop(context);
          },
          child: Center(
            child: Text(
              FlutterI18n.translate(context, "action.send.default"),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
