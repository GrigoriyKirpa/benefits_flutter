import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SpecificationSelectorScreen extends StatelessWidget {
  final List<Specification> specifications;
  final Function(Specification) onSpecificationChosen;
  static const routeName = "/specification_selector";

  SpecificationSelectorScreen(
      {Key key, Function(Specification) onSpecificationChosen})
      : this.specifications = Shop.currentUser.specifications,
        this.onSpecificationChosen = onSpecificationChosen ?? ((_) => {}),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context,
          FlutterI18n.translate(
              context, "user.specification.specification_selector.title")),
      body: Padding(
        padding: const EdgeInsets.all(9.0),
        child: Column(
          children: [
            Text(
              FlutterI18n.translate(context,
                  "user.specification.specification_selector.subtitle"),
            ),
            Space.vertical(18),
            Flexible(
              child: ListView.builder(
                  itemCount: specifications.length,
                  itemBuilder: (context, index) {
                    final Specification specification = specifications[index];
                    return Container(
                      padding: specification != specifications.last
                          ? EdgeInsets.only(bottom: 16)
                          : EdgeInsets.zero,
                      width: MediaQuery.of(context).size.width,
                      child: RawMaterialButton(
                        onPressed: () {
                          Navigator.pop(context);
                          onSpecificationChosen(specification);
                        },
                        child: Card(
                          color: specification.color,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 18, vertical: 9),
                            child: Row(
                              children: [
                                Text(
                                  specification.name,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(fontSize: 18),
                                ),
                                Spacer(),
                                Icon(
                                  Icons.add,
                                  size: 30,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
