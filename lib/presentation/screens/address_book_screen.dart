import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/address_editor_screen.dart';
import 'package:benefit_flutter/presentation/widgets/address_book_list_item.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class AddressBookScreen extends StatefulWidget {
  static const routeName = "/address_book";

  @override
  _AddressBookScreenState createState() => _AddressBookScreenState();
}

class _AddressBookScreenState extends State<AddressBookScreen> {
  List<Address> _addresses = [];
  int _selectedAddressIndex;

  @override
  void initState() {
    if (Shop.currentUser.info.addressBook.isNotEmpty) {
      _addresses = Shop.currentUser.info.addressBook;
      _selectedAddressIndex = Shop.currentUser.info.currentAddress;
    }
    super.initState();
  }

  bool isSelected(int index) {
    return index == _selectedAddressIndex;
  }

  void handleSelection(int index) {
    if (_selectedAddressIndex != index) {
      setState(() {
        _selectedAddressIndex = index;
      });
      Shop.currentUser.info.currentAddress = _selectedAddressIndex;
      saveChanges();
    }
  }

  void reload() {
    setState(() {
      if (Shop.currentUser.info.addressBook.isNotEmpty) {
        _addresses = Shop.currentUser.info.addressBook;
        _selectedAddressIndex = Shop.currentUser.info.currentAddress;
      }
    });
    saveChanges();
  }

  void saveChanges() {
    Shop.saveUserInfo();
    ServerRequests.updateUserInfo(Shop.currentUser.info);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context, FlutterI18n.translate(context, "user.address.page.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
        child: Column(
          children: [
            Space.vertical(18),
            RawMaterialButton(
              onPressed: () => Navigator.pushNamed(
                context,
                AddressEditorScreen.routeName,
                arguments: AddressEditorScreenParameters(
                  onCreateButtonPressed: (address) {
                    Shop.currentUser.info.addressBook.add(address);
                    handleSelection(
                        Shop.currentUser.info.addressBook.indexOf(address));
                  },
                ),
              ).then((_) => reload()),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 26),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: customBlueColor,
                    width: 1,
                  ),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.add,
                        size: 18,
                        color: customBlueColor,
                      ),
                      Space.horizontal(10),
                      Text(
                        FlutterI18n.translate(context, "action.add.address"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: customBlueColor),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Space.vertical(24),
            Flexible(
              child: ListView.builder(
                itemCount: _addresses.length,
                itemBuilder: (context, index) {
                  Address address = _addresses[index];
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    child: AddressBookListItem(
                      address: address,
                      headerText: FlutterI18n.translate(context,
                          "user.address.page.${isSelected(index) ? 'current_address' : 'other_address'}"),
                      headerColor:
                          isSelected(index) ? customBlueColor : Colors.black,
                      onItemPressed: () => handleSelection(index),
                      onEditButtonPressed: () => (() => Navigator.pushNamed(
                            context,
                            AddressEditorScreen.routeName,
                            arguments: AddressEditorScreenParameters(
                              onCreateButtonPressed: (address) => Shop
                                  .currentUser.info.addressBook[index]
                                  .editFrom(address),
                              initialAddress: address,
                            ),
                          ).then(
                            (_) => reload(),
                          )),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
