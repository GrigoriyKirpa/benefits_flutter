import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/sort_by.dart';
import 'package:benefit_flutter/data/models/category.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/product_list_widget.dart';
import 'package:benefit_flutter/presentation/widgets/search_widget.dart';
import 'package:benefit_flutter/presentation/widgets/sort_button.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CategoryScreen extends StatefulWidget {
  final Category category;
  final VoidCallback onBasketHandled;
  static const routeName = '/category';

  CategoryScreen({
    Key key,
    @required this.category,
    VoidCallback onBasketHandled,
  })  : this.onBasketHandled = onBasketHandled ?? (() => {}),
        assert(category != null),
        super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  SortBy _sortBy = SortBy.values.first;

  @override
  void initState() {
    super.initState();
  }

  Future getProducts() async {
    final List<ProductDescriptor> products =
        await ServerRequests.getProductList(
            categoryId: widget.category.descriptor.id);
    await Shop.updateServerData();
    return products;
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: UniqueKey(),
      child: Scaffold(
        appBar: CustomWidgets.appbarWithText(
          context,
          widget.category.descriptor.name,
        ),
        body: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: SearchWidget(),
            ),
            Space.vertical(11),
            Text(
              "${widget.category.descriptor.productCount} ${FlutterI18n.plural(
                context,
                "product.products.number",
                Language.getPlural(widget.category.descriptor.productCount),
              )}",
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: customTextGrayColor1),
            ),
            Space.vertical(14),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 11.0),
              child: Row(
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    child: SortButton(
                      sortBy: _sortBy,
                      onSortOptionChanged: (option) => setState(
                        () {
                          _sortBy = option;
                        },
                      ),
                    ),
                  ),
                  // Space.horizontal(11),
                  // Flexible(
                  //   fit: FlexFit.tight,
                  //   child: FilterButton(),
                  // ),
                ],
              ),
            ),
            Space.vertical(5),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 11.0),
                child: widget.category.products == null
                    ? FutureBuilder(
                        future: getProducts(),
                        builder: (context, snapshot) {
                          if (snapshot.hasError) {
                            return Center(
                              child: Text(
                                FlutterI18n.translate(
                                    context, "error.no_service"),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(color: customGrayColor1),
                                textAlign: TextAlign.center,
                              ),
                            );
                          } else if (snapshot.hasData) {
                            return productList(_sortBy.sort(snapshot.data));
                          } else {
                            return LinearProgressIndicator();
                          }
                        },
                      )
                    : productList(_sortBy.sort(widget.category.products)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget productList(List<ProductDescriptor> products) {
    return products.isNotEmpty
        ? ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: customGrayColor1,
                    ),
                  ),
                ),
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ProductListWidget(
                  product: products[index],
                  onBasketHandled: widget.onBasketHandled,
                ),
              );
            },
          )
        : Center(
            child: Text(
              FlutterI18n.translate(context, "empty.content"),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: customGrayColor1),
              textAlign: TextAlign.center,
            ),
          );
  }
}
