import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/presentation/screens/specification_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SpecificationListScreen extends StatefulWidget {
  static const routeName = "/specification_list";

  @override
  _SpecificationListScreenState createState() =>
      _SpecificationListScreenState();
}

class _SpecificationListScreenState extends State<SpecificationListScreen> {
  List<Specification> _specifications = Shop.currentUser.specifications;

  void reloadSpecifications() {
    if (mounted)
      setState(() {
        _specifications = Shop.currentUser.specifications;
      });
  }

  void openSpecificationScreen(Specification specification) {
    Navigator.of(context, rootNavigator: true)
        .pushNamed(
      SpecificationScreen.routeName,
      arguments: specification,
    )
        .then((_) {
      reloadSpecifications();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          appBar: CustomWidgets.appbar(
            context,
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(),
                Text(
                  FlutterI18n.translate(context, "user.specification.page.title"),
                  style: Theme.of(context).textTheme.headline1,
                ),
                Spacer(),
                IconButton(
                  onPressed: () =>
                      Navigator.of(context, rootNavigator: true).pushNamed(
                    SpecificationEditorScreen.routeName,
                    arguments: SpecificationEditorScreenArguments(
                      onCreatePressed: (specification) {
                        Shop.currentUser.addSpecification(specification);
                        Navigator.of(context, rootNavigator: true)
                            .pushReplacementNamed(
                          SpecificationScreen.routeName,
                          arguments: specification,
                        )
                            .then((_) {
                          reloadSpecifications();
                        });
                      },
                    ),
                  ),
                  icon: Icon(
                    Icons.add,
                    color: customTextGrayColor1,
                    size: 28,
                  ),
                ),
              ],
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(9.0),
            child: Column(
              children: [
                Space.vertical(26),
                Flexible(
                  child: ListView.builder(
                      itemCount: _specifications.length,
                      itemBuilder: (context, index) {
                        final Specification specification =
                            _specifications[index];
                        return Container(
                          padding: specification != _specifications.last
                              ? EdgeInsets.only(bottom: 16)
                              : EdgeInsets.zero,
                          width: MediaQuery.of(context).size.width,
                          child: RawMaterialButton(
                            onPressed: () =>
                                openSpecificationScreen(specification),
                            child: Card(
                              color: specification.color,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 18, vertical: 7),
                                child: Row(
                                  children: [
                                    Text(
                                      specification.name,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline5
                                          .copyWith(fontSize: 18),
                                    ),
                                    Spacer(),
                                    Column(
                                      children: [
                                        Text(
                                            "${specification.products.length} ${FlutterI18n.plural(
                                              context,
                                              "product.products.number",
                                              Language.getPlural(
                                                  specification.products.length),
                                            )}",
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5),
                                        Space.vertical(2),
                                        CustomWidgets.totalPriceWidget(
                                          context,
                                          ProductDescriptor.calculateTotalPrice(
                                              specification.products),
                                          Theme.of(context).textTheme.headline5,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
