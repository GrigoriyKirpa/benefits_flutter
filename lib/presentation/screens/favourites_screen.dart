import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/product_list_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class FavouritesScreen extends StatefulWidget {
  static const routeName = '/favourites';

  FavouritesScreen({
    Key key,
  }) : super(key: key);

  @override
  _FavouritesScreenState createState() => _FavouritesScreenState();
}

class _FavouritesScreenState extends State<FavouritesScreen> {
  List<LikedProductDescriptor> _products;

  @override
  void initState() {
    super.initState();
  }

  Future<List<LikedProductDescriptor>> initFavourites() async {
    await Shop.updateServerData();
    return Shop.currentUser.likedProducts;
  }

  void reload() {
    setState(() {
      _products = Shop.currentUser.likedProducts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: UniqueKey(),
      child: Scaffold(
        appBar: CustomWidgets.appbarWithText(
          context,
          FlutterI18n.translate(context, "favourites.title"),
        ),
        body: _products == null
            ? FutureBuilder(
                future: initFavourites(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    _products = snapshot.data;
                    return body;
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        FlutterI18n.translate(context, "error.no_service"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: customGrayColor1),
                        textAlign: TextAlign.center,
                      ),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              )
            : body,
      ),
    );
  }

  Widget get body {
    return Column(
      children: [
        Space.vertical(11),
        Text(
          "${_products.length} ${FlutterI18n.plural(
            context,
            "product.products.number",
            Language.getPlural(_products.length),
          )}",
          style: Theme.of(context)
              .textTheme
              .headline5
              .copyWith(color: customTextGrayColor1),
        ),
        Space.vertical(14),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 11.0),
            child: productList(
              _products.map(
                (element) {
                  return element.product;
                },
              ).toList(),
            ),
          ),
        ),
      ],
    );
  }

  Widget productList(List<ProductDescriptor> products) {
    return products.isNotEmpty
        ? ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: customGrayColor1,
                    ),
                  ),
                ),
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ProductListWidget(
                  product: products[index],
                  onBasketHandled: reload,
                  onLikeHandled: reload,
                ),
              );
            },
          )
        : Center(
            child: Text(
              FlutterI18n.translate(context, "empty.content"),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: customGrayColor1),
              textAlign: TextAlign.center,
            ),
          );
  }
}
