import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/review_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ReviewScreen extends StatelessWidget {
  final List<Review> reviews;
  static const routeName = "/reviews";

  ReviewScreen({Key key, @required this.reviews})
      : assert(reviews != null && reviews.length > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context,
          FlutterI18n.translate(
              context, "product.page.feedback.tab_name.review")),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 9),
        child: Column(
          children: [
            Flexible(
              child: ListView.builder(
                itemCount: reviews.length,
                itemBuilder: (context, index) {
                  final Review review = reviews.reversed.toList()[index];
                  return ReviewWidget(review: review);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
