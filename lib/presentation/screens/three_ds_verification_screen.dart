import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/data/utils/payment_api.dart';
import 'package:benefit_flutter/presentation/widgets/cancel_icon_button.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:screen_loader/screen_loader.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ThreeDsVerificationArguments {
  final String transactionId;
  final String paReq;
  final String acsUrl;

  ThreeDsVerificationArguments({
    @required this.transactionId,
    @required this.paReq,
    @required this.acsUrl,
  })  : assert(transactionId != null && transactionId.isNotEmpty),
        assert(paReq != null && paReq.isNotEmpty),
        assert(acsUrl != null && acsUrl.isNotEmpty);
}

class ThreeDsVerifiedArguments {
  final String transactionId;
  final String paRes;

  ThreeDsVerifiedArguments({@required this.transactionId, @required this.paRes})
      : assert(paRes != null && paRes.isNotEmpty),
        assert(transactionId != null && transactionId.isNotEmpty);
}

class ThreeDsVerificationScreen extends StatefulWidget {
  static const String routeName = "/3ds_verification";
  final ThreeDsVerificationArguments verificationArguments;

  ThreeDsVerificationScreen({
    @required this.verificationArguments,
  }) : assert(verificationArguments != null);

  _ThreeDsVerificationScreenState createState() =>
      _ThreeDsVerificationScreenState();
}

class _ThreeDsVerificationScreenState extends State<ThreeDsVerificationScreen>
    with ScreenLoader {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  WebViewController _webViewController;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
  }

  void onSuccess(ThreeDsVerifiedArguments args) {
    Navigator.pop(context, args);
  }

  void onFailure([String error]) {
    Navigator.pop(context, error);
  }

  Future<void> _loadForm(WebViewController webViewController) async {
    var uri = Uri.parse(widget.verificationArguments.acsUrl);
    var response = await http.post(
      uri,
      body: {
        "PaReq": widget.verificationArguments.paReq,
        "MD": widget.verificationArguments.transactionId,
        "TermUrl": PaymentAPI.postBackUrl,
      },
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      webViewController.loadHtmlString(response.body,
          baseUrl: widget.verificationArguments.acsUrl);
      _webViewController = webViewController;
      _controller.complete(webViewController);
    } else {
      onFailure("server");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: CustomWidgets.customAppbar(
            72,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                CancelIconButton(),
                Space.horizontal(14),
              ],
            ),
            false,
          ),
          body: Builder(builder: (BuildContext context) {
            return loadableWidget(
              child: WebView(
                javascriptChannels: <JavascriptChannel>[
                  _javascriptChannel(context),
                ].toSet(),
                javascriptMode: JavascriptMode.unrestricted,
                onWebResourceError: (_) => Navigator.pop(context),
                onWebViewCreated: (WebViewController webViewController) =>
                    this.performFuture(() => _loadForm(webViewController)),
                onProgress: (int progress) {
                  print('WebView is loading (progress : $progress%)');
                },
                navigationDelegate: (NavigationRequest request) {
                  return NavigationDecision.navigate;
                },
                onPageStarted: (String url) {
                  print('Page started loading: $url');
                },
                onPageFinished: (String url) async {
                  if (url == PaymentAPI.postBackUrl) {
                    await _webViewController.runJavascript(
                        "(function(){three_ds_verifier.postMessage(window.document.body.outerHTML)})();");
                  }
                },
                gestureNavigationEnabled: true,
                backgroundColor: const Color(0x00000000),
              ),
            );
          }),
        ),
      ),
    );
  }

  JavascriptChannel _javascriptChannel(BuildContext context) {
    return JavascriptChannel(
      name: 'three_ds_verifier',
      onMessageReceived: (JavascriptMessage message) {
        final String body = Language.extractJson(message.message);
        var map = {};
        if (body != null && body.isNotEmpty) {
          map = jsonDecode(body);
        }
        if (map["MD"] != null && map["PaRes"] != null) {
          onSuccess(ThreeDsVerifiedArguments(
              transactionId: map["MD"], paRes: map["PaRes"]));
        } else {
          onFailure("server");
        }
      },
    );
  }
}
