import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/user_check_status.dart';
import 'package:benefit_flutter/data/models/user.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/local_storage.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/account_type_selection_screen.dart';
import 'package:benefit_flutter/presentation/screens/login_screen.dart';
import 'package:benefit_flutter/presentation/screens/main_navigation_screen.dart';
import 'package:benefit_flutter/presentation/screens/phone_verification_screen.dart';
import 'package:flutter/material.dart';

class StartScreen extends StatelessWidget {
  static String routeName = "/";
  static bool started = false;

  Future<void> startUp(BuildContext context) async {
    if (started) return;
    started = true;
    final UserInfo info = await LocalStorage.getUserInfoFromLocalStorage();
    if (info == null || info.phone == null) {
      Navigator.pushReplacementNamed(
          context, PhoneVerificationScreen.routeName);
      return;
    } else if (!(info.id == null ||
        info.email == null ||
        info.email.isEmpty ||
        info.password == null ||
        info.password.isEmpty)) {
      final UserCheckStatus status =
          await ServerRequests.checkUser(info.email, info.phone.nonFormatted);
      if (status == UserCheckStatus.Ok) {
        final UserInfo newInfo =
            await ServerRequests.login(info.email, info.password, info.phone);
        if (newInfo != null) {
          Shop.currentUser.info = newInfo;
          Shop.currentUser.info.fullName =
              Shop.currentUser.info.fullName.trim();
          Shop.currentUser.info.searchHistory = info.searchHistory;
          Shop.saveUserInfo();
          if (newInfo.type != null) {
            Navigator.pushReplacementNamed(
                context, MainNavigationScreen.routeName);
          } else {
            Navigator.pushReplacementNamed(
                context, PhoneVerificationScreen.routeName);
            Navigator.pushNamed(context, LoginScreen.routeName);
            Navigator.pushNamed(context, AccountTypeSelectionScreen.routeName);
          }
          return;
        }
      }
    }
    Shop.currentUser.info = info;
    Navigator.pushReplacementNamed(context, PhoneVerificationScreen.routeName);
    Navigator.pushNamed(context, LoginScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(
      Duration(milliseconds: 500),
      () => startUp(context),
    );
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset(imageAssetBuilder("loadScreen.jpg")).image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
