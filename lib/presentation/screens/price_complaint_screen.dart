import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/message.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class PriceComplaintScreen extends StatefulWidget {
  final Function(Message) onSentPressed;
  static const routeName = "/price_complaint";

  PriceComplaintScreen({Key key, Function(Message) onSentPressed})
      : this.onSentPressed = onSentPressed ?? ((_) => null),
        super(key: key);

  @override
  _PriceComplaintScreenState createState() => _PriceComplaintScreenState();
}

class _PriceComplaintScreenState extends State<PriceComplaintScreen> {
  final TextEditingController _controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomWidgets.appbarWithText(context,
          FlutterI18n.translate(context, "price_complaint_screen.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Text(
                FlutterI18n.translate(
                    context, "price_complaint_screen.subtitle"),
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: customTextGrayColor1),
                textAlign: TextAlign.center,
              ),
            ),
            Flexible(
              child: Space.vertical(83),
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                validator: (text) {
                  return text == null || text.isEmpty
                      ? FlutterI18n.translate(context, "form.compulsory_field")
                      : null;
                },
                maxLength: 70,
                maxLines: 3,
                textCapitalization: TextCapitalization.sentences,
                controller: _controller,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontSize: 22),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 6, vertical: 8),
                  filled: true,
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
                keyboardType: TextInputType.text,
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Container(
        width: MediaQuery.of(context).size.width,
        height: 43,
        padding: EdgeInsets.only(
          left: 0.182 * MediaQuery.of(context).size.width,
          right: 0.182 * MediaQuery.of(context).size.width,
        ),
        margin: EdgeInsets.only(
          bottom: 56 - MediaQuery.of(context).viewInsets.bottom > 0
              ? 56 - MediaQuery.of(context).viewInsets.bottom
              : MediaQuery.of(context).viewInsets.bottom - 50,
        ),
        child: FloatingActionButton(
          onPressed: () {
            final bool result = _formKey.currentState.validate();
            if (result) {
              widget.onSentPressed(
                Message(
                  sender: Shop.currentUser,
                  date: DateTime.now(),
                  text: _controller.text,
                ),
              );
              Navigator.pop(context);
            }
          },
          child: Center(
            child: Text(
              FlutterI18n.translate(context, "action.send.default"),
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
