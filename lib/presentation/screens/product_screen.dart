import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/product.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/product_feedback.dart';
import 'package:benefit_flutter/data/models/rating.dart';
import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/mixins/basket_notifier.dart';
import 'package:benefit_flutter/presentation/screens/specification_selector_screen.dart';
import 'package:benefit_flutter/presentation/widgets/carousel_slider_with_indicator.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/double_stated_buttons.dart';
import 'package:benefit_flutter/presentation/widgets/feedback_widget.dart';
import 'package:benefit_flutter/presentation/widgets/product_color_selector.dart';
import 'package:benefit_flutter/presentation/widgets/product_info.dart';
import 'package:benefit_flutter/presentation/widgets/product_number_selector.dart';
import 'package:benefit_flutter/presentation/widgets/size_selector.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:benefit_flutter/presentation/widgets/web_module_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ProductScreen extends StatefulWidget {
  final ProductDescriptor product;
  static const routeName = "/product";

  ProductScreen({Key key, @required this.product})
      : assert(product != null),
        super(key: key);

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> with BasketNotifier {
  String _selectedColor;
  String _selectedSize;
  int _selectedNumber;
  bool _isInSpecification = false;
  ProductFeedback _feedback;
  Rating _rating;
  Product _product;
  bool _feedbackLoading = false;
  bool _rated;

  @override
  void initState() {
    _rated = Shop.currentUser.info.ratingMap.containsKey(widget.product.id);
    super.initState();
  }

  @override
  void displayAddedToBasket(BuildContext context) {
    if (!mounted) {
      return;
    }
    super.displayAddedToBasket(context);
  }

  @override
  void displayNotAddedToBasket(BuildContext context) {
    if (!mounted) {
      return;
    }
    super.displayNotAddedToBasket(context);
  }

  void reload() {
    setState(() {
      _selectedNumber = widget.product.minAmount;
      _feedback = _product.feedback;
      _rating = _product.descriptor.rating;
      _isInSpecification =
          Shop.currentUser.isInSpecification(_product.descriptor);
      _selectedColor = _product.availableColors.isNotEmpty
          ? _product.availableColors.first
          : null;
      _selectedSize = _product.availableSizes.isNotEmpty
          ? _product.availableSizes.first
          : null;
    });
  }

  Future getProduct() async {
    _product = await ServerRequests.getProduct(widget.product.id);

    _selectedNumber = widget.product.minAmount;
    _feedback = _product.feedback;
    _rating = _product.descriptor.rating;
    _isInSpecification =
        Shop.currentUser.isInSpecification(_product.descriptor);
    _selectedColor = _product.availableColors.isNotEmpty
        ? _product.availableColors.first
        : null;
    _selectedSize = _product.availableSizes.isNotEmpty
        ? _product.availableSizes.first
        : null;
    await Shop.updateServerData();
    return true;
  }

  void handleColorSelection(String color) {
    _selectedColor = color;
  }

  void handleSizeSelection(String size) {
    _selectedSize = size;
  }

  Text get specificationText {
    if (_isInSpecification) {
      return Text(
        Shop.currentUser.specificationName(_product.descriptor),
        style: Theme.of(context)
            .textTheme
            .headline5
            .copyWith(color: customBlueColor),
        textAlign: TextAlign.center,
      );
    } else {
      return Text(
        "+ ${FlutterI18n.translate(context, "user.specification.add")}",
        style: Theme.of(context)
            .textTheme
            .headline5
            .copyWith(color: customRedColor),
        textAlign: TextAlign.center,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: UniqueKey(),
      child: _product == null
          ? FutureBuilder(
              future: getProduct(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Scaffold(
                    appBar: CustomWidgets.appbarWithText(context, ""),
                    body: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          snapshot.error.toString(),
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(color: customGrayColor1),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  );
                } else if (snapshot.hasData) {
                  return body;
                } else {
                  return Scaffold(
                    appBar: CustomWidgets.appbarWithText(context, ""),
                    body: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
              },
            )
          : body,
    );
  }

  Widget get body {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          appBar: CustomWidgets.appbar(
            context,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Space.horizontal(27),
                DoubleStateButtons.likeButton(
                  isInitiallyActive: Shop.currentUser.isLiked(widget.product.id),
                  handleTap: (_) async =>
                      await Shop.currentUser.handleLike(widget.product).then(
                            (success) => success ? null : reload(),
                          ),
                ),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CarouselSliderWithIndicator(
                    height: 410,
                    indicatorOnImage: false,
                    images: _product.images.map((provider) {
                      return CachedNetworkImage(
                        imageUrl: provider,
                        fit: BoxFit.cover,
                        placeholder: (context, url) =>
                            Center(child: CircularProgressIndicator()),
                      );
                    }).toList(),
                  ),
                  Space.vertical(4),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(horizontal: 14.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${FlutterI18n.translate(context, "product.page.set_number")} ${_product.descriptor.setNumber}",
                          style: Theme.of(context).textTheme.headline5.copyWith(
                                fontSize: 10,
                                color: customGrayColor1,
                              ),
                        ),
                        Space.vertical(2),
                        Text(
                          _product.descriptor.name,
                          style: Theme.of(context).textTheme.headline1.copyWith(
                                fontSize: 14,
                              ),
                        ),
                        if (_product.availableColors.isNotEmpty) ...[
                          Space.vertical(5),
                          Text(
                            FlutterI18n.translate(
                                context, "product.page.color_selection"),
                            style: Theme.of(context).textTheme.headline5.copyWith(
                                  fontSize: 12,
                                  color: customGrayColor1,
                                ),
                          ),
                          Space.vertical(7),
                          ProductColorSelector(
                            colors: _product.availableColors,
                            initialColorIndex:
                                _product.availableColors.indexOf(_selectedColor),
                            onColorSelected: handleColorSelection,
                          ),
                        ],
                        if (_product.availableSizes.isNotEmpty) ...[
                          Space.vertical(5),
                          Text(
                            FlutterI18n.translate(
                                context, "product.page.size_selection"),
                            style: Theme.of(context).textTheme.headline5.copyWith(
                                  fontSize: 12,
                                  color: customGrayColor1,
                                ),
                          ),
                          Space.vertical(7),
                          SizeSelector(
                            sizes: _product.availableSizes,
                            initialSizeIndex:
                                _product.availableSizes.indexOf(_selectedSize),
                            onSizeSelected: handleSizeSelection,
                          ),
                        ],
                        Space.vertical(18),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ProductNumberSelector(
                              stock: defaultStock * widget.product.minAmount,
                              initiallySelected: _selectedNumber,
                              onValueChanged: (value) => _selectedNumber = value,
                              color: customGrayColor2,
                              step: widget.product.minAmount,
                            ),
                            CustomWidgets.pricePerPieceRichText(
                              context,
                              _product.descriptor.price.price,
                              Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontSize: 24),
                              Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .copyWith(fontSize: 18),
                            ),
                          ],
                        ),
                        Space.vertical(15),
                        Row(
                          children: [
                            OutlinedButton(
                              onPressed: () async {
                                if (await Shop.basket.add(
                                  OrderItem(
                                    product: widget.product,
                                    price: widget.product.price,
                                    amount: _selectedNumber,
                                    selectedSize: _selectedSize,
                                    selectedColor: _selectedColor,
                                  ),
                                )) {
                                  reload();
                                  displayAddedToBasket(context);
                                } else {
                                  reload();
                                  displayNotAddedToBasket(context);
                                }
                              },
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 36, vertical: 8),
                                child: Text(
                                  FlutterI18n.translate(
                                      context, "action.add.basket"),
                                ),
                              ),
                            ),
                            Flexible(
                              fit: FlexFit.tight,
                              child: GestureDetector(
                                onTap: () {
                                  if (_isInSpecification) {
                                    Shop.currentUser.removeFromSpecification(
                                        _product.descriptor);
                                    setState(() {
                                      _isInSpecification = !_isInSpecification;
                                    });
                                  } else {
                                    Navigator.pushNamed(context,
                                        SpecificationSelectorScreen.routeName,
                                        arguments: (specification) {
                                      Shop.currentUser.addToSpecification(
                                          specification, _product.descriptor);
                                      setState(() {
                                        _isInSpecification = !_isInSpecification;
                                      });
                                    });
                                  }
                                },
                                child: specificationText,
                              ),
                            ),
                          ],
                        ),
                        Space.vertical(13),
                        ProductInfo(
                          description: _product.description,
                          features: _product.features,
                        ),
                        Space.vertical(8),
                        WebModuleWidget(
                          title: FlutterI18n.translate(
                              context, "product.page.recommended"),
                          products: _product.suggestions,
                        ),
                        // Space.vertical(12),
                        // GestureDetector(
                        //   onTap: () => Navigator.pushNamed(
                        //       context, PriceComplaintScreen.routeName,
                        //       arguments: (_) => null),
                        //   child: Text(
                        //     FlutterI18n.translate(context, "product.page.complain"),
                        //     style: Theme.of(context)
                        //         .textTheme
                        //         .headline5
                        //         .copyWith(color: customRedColor, fontSize: 12),
                        //   ),
                        // ),
                        Space.vertical(12),
                        _feedbackLoading
                            ? Center(child: CircularProgressIndicator())
                            : FeedbackWidget(
                                rated: _rated,
                                feedback: _feedback,
                                rating: _rating,
                                onRatingAdded: (stars) {
                                  setState(() {
                                    _feedbackLoading = true;
                                  });
                                  reloadRating(stars);
                                },
                                onReviewAdded: (review) {
                                  setState(() {
                                    _feedbackLoading = true;
                                  });
                                  reloadReviews(review);
                                },
                                onQuestionAndAnswerAdded: (_) => null,
                              ),
                        Space.vertical(12),
                        Text(
                          FlutterI18n.translate(context, "product.page.caution"),
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(fontSize: 10, color: customGrayColor1),
                        ),
                        Space.vertical(20),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> reloadRating(int stars) async {
    try {
      final Rating rating =
          await ServerRequests.rateProduct(stars, _product.descriptor.id);
      _product.descriptor.rating = rating;
      Shop.currentUser.info.ratingMap[_product.descriptor.id] = stars;
      setState(() {
        _rated = true;
        _rating = rating;
        _feedbackLoading = false;
      });
    } catch (e) {
      setState(() {
        _feedbackLoading = false;
      });
      System.displayErrorSnackBar(context);
      return;
    }
    Shop.saveUserInfo();
    ServerRequests.updateUserInfo(Shop.currentUser.info);
  }

  Future<void> reloadReviews(Review review) async {
    try {
      final List<Review> reviews =
          await ServerRequests.addReview(review, _product.descriptor.id);

      if (reviews != null) {
        _product.feedback.reviews = reviews;
        setState(() {
          _feedback.reviews = reviews;
          _feedbackLoading = false;
        });
        return;
      }
    } catch (e) {}
    setState(() {
      _feedbackLoading = false;
    });
    System.displayErrorSnackBar(context);
  }
}
