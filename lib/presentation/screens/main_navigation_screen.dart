import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/main_navigation.dart';
import 'package:flutter/material.dart';

class MainNavigationScreen extends StatefulWidget {
  static const routeName = '/main';

  MainNavigationScreen({Key key}) : super(key: key);

  @override
  _MainNavigationScreenState createState() => _MainNavigationScreenState();
}

class _MainNavigationScreenState extends State<MainNavigationScreen> {
  MainNavigation _currentPage;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _currentPage =
        ModalRoute.of(context).settings.arguments as MainNavigation ??
            startPage;
    super.didChangeDependencies();
  }

  void _handleNavigationTap(int index) {
    final MainNavigation navigation =
        MainNavigationExtension.indexToPage(index);
    setState(() {
      _currentPage = navigation;
    });
    if (_currentPage.navigationKey.currentState != null) {
      _currentPage.navigationKey.currentState
          .popUntil((route) => route.isFirst);
      _currentPage.navigationKey.currentState.pushReplacementNamed("/");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: _currentPage.page(context),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 28,
        unselectedItemColor: customGrayColor1,
        showSelectedLabels: false,
        selectedItemColor: customBlueColor,
        onTap: _handleNavigationTap,
        currentIndex: _currentPage.index,
        items: MainNavigation.values.map((element) {
          return BottomNavigationBarItem(
            backgroundColor: scaffoldBackgroundColor,
            label: '',
            icon: element.icon,
          );
        }).toList(),
      ),
    );
  }
}
