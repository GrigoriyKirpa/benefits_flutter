import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/phone.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/code_verification_screen.dart';
import 'package:benefit_flutter/presentation/screens/login_screen.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class PhoneVerificationScreen extends StatefulWidget {
  static const routeName = "/phone_verification";
  _PhoneVerificationScreenState createState() =>
      _PhoneVerificationScreenState();
}

class _PhoneVerificationScreenState extends State<PhoneVerificationScreen> {
  CountryCode _currentCountryCode;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _phoneController = TextEditingController();

  TextStyle get headline {
    return Theme.of(context)
        .textTheme
        .headline1
        .copyWith(color: Colors.white, fontSize: 30);
  }

  TextStyle get bodyText {
    return Theme.of(context)
        .textTheme
        .bodyText1
        .copyWith(color: Colors.white, fontSize: 18);
  }

  @override
  void dispose() {
    if (mounted) {
      _phoneController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: loginPageColor,
      ),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(
            horizontal: 27,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(41),
              Text(
                FlutterI18n.translate(context, "auth.screen.title"),
                style: headline,
              ),
              Space.vertical(41),
              Text(
                FlutterI18n.translate(
                    context, "auth.screen.phone_verification.input_number"),
                style: bodyText,
              ),
              Space.vertical(52),
              Form(
                key: _formKey,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xFFFFFF).withOpacity(0.2),
                      ),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CountryCodePicker(
                        onInit: (code) => _currentCountryCode = code,
                        onChanged: (code) => _currentCountryCode = code,
                        initialSelection: 'RU',
                        // optional. Shows only country name and flag
                        showCountryOnly: false,
                        // optional. Shows only country name and flag when popup is closed.
                        showOnlyCountryWhenClosed: false,
                        padding: EdgeInsets.zero,
                        showFlagMain: false,
                        textStyle: bodyText.copyWith(fontSize: 24),
                      ),
                      Space.horizontal(5),
                      Flexible(
                        fit: FlexFit.tight,
                        child: TextFormField(
                          maxLength: 15,
                          style: bodyText.copyWith(fontSize: 24),
                          keyboardType: TextInputType.phone,
                          validator: (text) =>
                              FormFields.phoneNumberValidator(context, text),
                          controller: _phoneController,
                          scrollPadding: EdgeInsets.zero,
                          decoration: InputDecoration(
                            helperStyle: TextStyle(fontSize: 0),
                            contentPadding: EdgeInsets.zero,
                            focusedBorder: InputBorder.none,
                            border: InputBorder.none,
                          ),
                          inputFormatters: [FormFields.maskPhoneFormatter],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: Container(
          margin: EdgeInsets.only(
              bottom: 92 - MediaQuery.of(context).viewInsets.bottom > 10
                  ? 92 - MediaQuery.of(context).viewInsets.bottom
                  : 10,
              right: 36),
          width: 43,
          height: 73,
          child: FloatingActionButton(
            splashColor: Colors.white.withOpacity(0.1),
            elevation: 0,
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.transparent,
            focusColor: Colors.white.withOpacity(0.1),
            shape: CircleBorder(),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                final Phone phone = Phone(
                  countryCode: _currentCountryCode.dialCode,
                  phone: FormFields.unMask(_phoneController.text),
                );
                Shop.currentUser.info.phone = phone;
                ServerRequests.auth.verifyPhoneNumber(
                  timeout: Duration(seconds: codeVerificationTimeout),
                  phoneNumber: phone.nonFormatted,
                  verificationCompleted: (authCredential) {
                    ServerRequests.auth
                        .signInWithCredential(authCredential)
                        .then(
                      (result) {
                        Shop.saveUserInfo();
                        Navigator.pushNamed(
                          context,
                          LoginScreen.routeName,
                        );
                      },
                    ).catchError(
                      (e) {
                        print(e);
                      },
                    );
                  },
                  verificationFailed: (authException) {
                    print(authException.message);
                  },
                  codeSent: (verificationId, [int forceResendingToken]) {
                    Navigator.pushNamed(
                        context, CodeVerificationScreen.routeName,
                        arguments: verificationId);
                  },
                  codeAutoRetrievalTimeout: (verificationId) {},
                );
              }
            },
            child: Image.asset(
              imageAssetBuilder("loginButton.png"),
              fit: BoxFit.fill,
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        resizeToAvoidBottomInset: false,
      ),
    );
  }
}
