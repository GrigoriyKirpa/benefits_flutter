import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/main_navigation_screen.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class AccountTypeSelectionScreen extends StatefulWidget {
  static const routeName = "/accountTypeSelection";
  _AccountTypeSelectionScreenState createState() =>
      _AccountTypeSelectionScreenState();
}

class _AccountTypeSelectionScreenState
    extends State<AccountTypeSelectionScreen> {
  AccountType _typeChosen = defaultType;
  TextStyle get headline {
    return Theme.of(context)
        .textTheme
        .headline1
        .copyWith(color: Colors.white, fontSize: 30);
  }

  TextStyle get bodyText {
    return Theme.of(context)
        .textTheme
        .bodyText1
        .copyWith(color: Colors.white, fontSize: 18);
  }

  bool isSelected(AccountType type) {
    return _typeChosen == type;
  }

  void handleSelection(AccountType type) {
    if (!mounted) return;

    setState(() {
      _typeChosen = type;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: loginPageColor,
      ),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(
            horizontal: 27,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(41),
              Text(
                FlutterI18n.translate(
                    context, "auth.screen.register.account_type.title"),
                style: headline,
              ),
              Space.vertical(21),
              Flexible(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: AccountType.values.length,
                  itemBuilder: (context, index) {
                    final AccountType type = AccountType.values[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                          color: Colors.white,
                          border: isSelected(type)
                              ? Border.all(
                                  color: accountTypeSelectedColor,
                                  width: 2,
                                )
                              : null,
                        ),
                        child: RawMaterialButton(
                          onPressed: () => handleSelection(type),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(26.0),
                                child: Text(
                                  type.label(context),
                                  style: headline.copyWith(
                                      color: Colors.black, fontSize: 18),
                                ),
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 8.0,
                                  horizontal: 15,
                                ),
                                child: Image(
                                  image: type.image,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Space.vertical(29),
              RichText(
                text: TextSpan(
                  style: bodyText.copyWith(fontSize: 14),
                  children: [
                    TextSpan(
                      text: FlutterI18n.plural(
                        context,
                        "auth.screen.register.account_type.confirmation.number",
                        0,
                      ),
                    ),
                    TextSpan(
                      text: FlutterI18n.plural(
                        context,
                        "auth.screen.register.account_type.confirmation.number",
                        1,
                      ),
                      style: TextStyle(
                        color: customBlueColor,
                      ),
                    ),
                    TextSpan(
                      text: FlutterI18n.plural(
                        context,
                        "auth.screen.register.account_type.confirmation.number",
                        2,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            right: 46,
            left: 27,
            bottom: 92 - MediaQuery.of(context).viewInsets.bottom > 10
                ? 92 - MediaQuery.of(context).viewInsets.bottom
                : 10,
          ),
          child: Row(
            children: [
              Container(
                width: 21,
                height: 29,
                child: RawMaterialButton(
                  onPressed: () => Navigator.pop(context),
                  shape: CircleBorder(),
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  fillColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  child: Image.asset(
                    imageAssetBuilder("loginButtonBack.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 43,
                height: 73,
                child: FloatingActionButton(
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  foregroundColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  shape: CircleBorder(),
                  onPressed: () async {
                    Shop.currentUser.info.type = _typeChosen;
                    if (await ServerRequests.updateUserInfo(
                        Shop.currentUser.info)) {
                      Shop.saveUserInfo();
                      Navigator.pushNamedAndRemoveUntil(context,
                          MainNavigationScreen.routeName, (route) => false);
                    }
                  },
                  child: Image.asset(
                    imageAssetBuilder("loginButton.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        resizeToAvoidBottomInset: false,
      ),
    );
  }
}
