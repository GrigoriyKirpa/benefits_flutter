import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class FAQScreen extends StatelessWidget {
  static String routeName = "/faq";

  TextStyle text(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12);
  }

  TextStyle title(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(fontSize: 12);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context, FlutterI18n.translate(context, "info_screens.faq.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 34, vertical: 8),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                FlutterI18n.translate(
                    context, "info_screens.faq.paragraphs.1.title"),
                style: title(context),
              ),
              Space.vertical(7),
              Text(
                FlutterI18n.translate(
                    context, "info_screens.faq.paragraphs.1.text"),
                style: text(context),
              ),
              Space.vertical(12),
              Text(
                FlutterI18n.translate(
                    context, "info_screens.faq.paragraphs.2.title"),
                style: title(context),
              ),
              Space.vertical(7),
              Text(
                FlutterI18n.translate(
                    context, "info_screens.faq.paragraphs.2.text"),
                style: text(context),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
