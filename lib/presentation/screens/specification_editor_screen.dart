import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/presentation/widgets/cancel_icon_button.dart';
import 'package:benefit_flutter/presentation/widgets/color_selector.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SpecificationEditorScreenArguments {
  final Function(Specification) onCreatePressed;
  final Specification specification;

  SpecificationEditorScreenArguments(
      {this.onCreatePressed, this.specification});
}

class SpecificationEditorScreen extends StatefulWidget {
  final Function(Specification) onCreatePressed;
  final Specification specification;
  static const routeName = "/specification_editor";

  SpecificationEditorScreen(
      {Key key, Function(Specification) onCreatePressed, this.specification})
      : this.onCreatePressed = onCreatePressed ?? (() => {}),
        super(key: key);

  @override
  _SpecificationEditorScreenState createState() =>
      _SpecificationEditorScreenState();
}

class _SpecificationEditorScreenState extends State<SpecificationEditorScreen> {
  TextEditingController _nameController;
  TextEditingController _descriptionController;
  Color _selectedColor = defaultSpecificationColor;

  String get currentName {
    return _nameController.text == ''
        ? FlutterI18n.translate(context, "user.specification.defaultName")
        : _nameController.text;
  }

  String get currentDescription {
    return _descriptionController.text;
  }

  void handleSelection(Color color) {
    FocusScope.of(context).unfocus();
    _selectedColor = color;
  }

  @override
  void initState() {
    _nameController = TextEditingController();
    _descriptionController = TextEditingController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (widget.specification != null) {
      _nameController.text = widget.specification.name ==
              FlutterI18n.translate(context, "user.specification.defaultName")
          ? ''
          : widget.specification.name;
      _descriptionController.text = widget.specification.description;
      _selectedColor = widget.specification.color;
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: CustomWidgets.customAppbar(
            72,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                CancelIconButton(),
                Space.horizontal(14),
              ],
            ),
            false,
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(
              left: 0.182 * MediaQuery.of(context).size.width,
              right: 0.182 * MediaQuery.of(context).size.width,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    FlutterI18n.translate(
                        context, 'user.specification.createTitle'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Flexible(child: Space.vertical(62)),
                FormFields.simpleTextField(
                    context,
                    FlutterI18n.translate(
                        context, "user.specification.editor.nameHint"),
                    _nameController,
                    maxLines: 1,
                    maxLength: 30,
                    type: TextInputType.name),
                Flexible(child: Space.vertical(34)),
                FormFields.simpleTextField(
                    context,
                    FlutterI18n.translate(
                        context, "user.specification.editor.descriptionHint"),
                    _descriptionController,
                    maxLines: 1,
                    maxLength: 100),
                Flexible(child: Space.vertical(66)),
                Text(
                  FlutterI18n.translate(
                      context, "user.specification.editor.color"),
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(color: customGrayColor1),
                ),
                Flexible(child: Space.vertical(14)),
                ColorSelector(
                  colors: specificationColorList,
                  initialColorIndex:
                      specificationColorList.indexOf(_selectedColor),
                  onColorSelected: handleSelection,
                ),
                Space.vertical(0.1 * MediaQuery.of(context).size.height),
              ],
            ),
          ),
          floatingActionButton: Container(
            width: MediaQuery.of(context).size.width,
            height: 43,
            padding: EdgeInsets.only(
              left: 0.182 * MediaQuery.of(context).size.width,
              right: 0.182 * MediaQuery.of(context).size.width,
            ),
            margin: EdgeInsets.only(
              bottom: 0.164 * MediaQuery.of(context).size.height -
                          MediaQuery.of(context).viewInsets.bottom >
                      0
                  ? 0.164 * MediaQuery.of(context).size.height -
                      MediaQuery.of(context).viewInsets.bottom
                  : 0,
            ),
            child: FloatingActionButton(
              onPressed: () => widget.onCreatePressed(
                Specification(
                  id: widget.specification != null
                      ? widget.specification.id
                      : Specification.idProvider,
                  name: currentName,
                  color: _selectedColor,
                  description: currentDescription,
                ),
              ),
              child: Center(
                child: Text(
                  FlutterI18n.translate(context, "action.create.default"),
                ),
              ),
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ),
      ),
    );
  }
}
