import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/category_view_options.dart';
import 'package:benefit_flutter/data/models/category.dart';
import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/category_screen.dart';
import 'package:benefit_flutter/presentation/widgets/category_list_item.dart';
import 'package:benefit_flutter/presentation/widgets/category_widget.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/search_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CatalogScreen extends StatefulWidget {
  static const routeName = '/catalog';
  final CategoryDescriptor currentCategory;
  CatalogScreen({Key key, this.currentCategory}) : super(key: key);

  @override
  _CatalogScreenState createState() => _CatalogScreenState();
}

class _CatalogScreenState extends State<CatalogScreen> {
  List<CategoryDescriptor> _categories;
  CategoryViewOptions _currentOption;
  CategoryDescriptor _currentCategory;

  @override
  void initState() {
    _currentCategory = widget.currentCategory;
    _currentOption = defaultView;
    super.initState();
  }

  Future<bool> getCategories() async {
    _categories = await ServerRequests.getCategories(
        _currentCategory == null ? 0 : _currentCategory.id);
    if (_currentCategory != null && _currentCategory.productCount > 0)
      _categories.insert(
        0,
        CategoryDescriptor(
          id: _currentCategory.id,
          name: FlutterI18n.translate(context, "catalog.all_products"),
          productCount: _currentCategory.productCount,
        ),
      );
    return true;
  }

  Widget get _categoryView {
    Widget view;
    if (_categories.isEmpty) {
      return Center(
        child: Text(
          FlutterI18n.translate(context, "catalog.error.categoryFetch"),
          style: Theme.of(context).textTheme.headline5,
          textAlign: TextAlign.center,
        ),
      );
    }
    if (_currentOption == CategoryViewOptions.GridView) {
      view = GridView.builder(
        itemCount: _categories.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 1.2,
            crossAxisSpacing: 16,
            mainAxisSpacing: 16),
        itemBuilder: (context, index) {
          CategoryDescriptor category = _categories[index];
          return CategoryWidget(
            backgroundColor:
                _currentCategory != null && _currentCategory.id == category.id
                    ? customBlueColor
                    : Colors.white,
            textColor:
                _currentCategory != null && _currentCategory.id == category.id
                    ? Colors.white
                    : Colors.black,
            category: category,
            onPressed: () => openCatalogPage(category),
          );
        },
      );
    } else if (_currentOption == CategoryViewOptions.ListView) {
      view = ListView.builder(
        itemCount: _categories.length,
        itemBuilder: (context, index) {
          CategoryDescriptor category = _categories[index];
          return Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: CategoryListItem(
              backgroundColor:
                  _currentCategory != null && _currentCategory.id == category.id
                      ? customBlueColor
                      : Colors.white,
              textColor:
                  _currentCategory != null && _currentCategory.id == category.id
                      ? Colors.white
                      : Colors.black,
              category: category,
              onPressed: () => openCatalogPage(category),
            ),
          );
        },
      );
    } else {
      throw UnimplementedError('Category View');
    }

    return view;
  }

  void openCatalogPage(CategoryDescriptor category) {
    if (_currentCategory != null && category.id == _currentCategory.id) {
      Navigator.pushNamed(
        context,
        CategoryScreen.routeName,
        arguments: Category(descriptor: _currentCategory),
      );
    } else if (category.subCategoryCount == 0) {
      Navigator.pushNamed(
        context,
        CategoryScreen.routeName,
        arguments: Category(descriptor: category),
      );
    } else {
      Navigator.pushNamed(context, CatalogScreen.routeName,
          arguments: category);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
        context,
        _currentCategory == null
            ? FlutterI18n.translate(context, "catalog.title")
            : _currentCategory.name,
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: SearchWidget(),
          ),
          Space.vertical(11),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: CategoryViewOptions.values.map((element) {
              return Padding(
                padding: element != CategoryViewOptions.values.last
                    ? EdgeInsets.only(right: 8.0)
                    : EdgeInsets.zero,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      _currentOption = element;
                    });
                  },
                  child: Icon(
                    element.icon,
                    size: 21,
                    color: element == _currentOption
                        ? customBlueColor
                        : customGrayColor1,
                  ),
                ),
              );
            }).toList(),
          ),
          Space.vertical(12),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: FutureBuilder(
                future: getCategories(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        FlutterI18n.translate(context, "error.no_service"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: customTextGrayColor1),
                        textAlign: TextAlign.center,
                      ),
                    );
                  } else if (snapshot.hasData) {
                    return _categoryView;
                  } else {
                    return LinearProgressIndicator();
                  }
                },
              ),
            ),
          ),
          Space.vertical(10),
        ],
      ),
    );
  }
}
