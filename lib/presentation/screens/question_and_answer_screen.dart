import 'package:benefit_flutter/data/models/question_and_answer.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/question_and_answer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class QuestionAndAnswerScreen extends StatelessWidget {
  final List<QuestionAndAnswer> questionAndAnswers;
  static const routeName = "/question_and_answer";

  QuestionAndAnswerScreen({Key key, @required this.questionAndAnswers})
      : assert(questionAndAnswers != null && questionAndAnswers.length > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context,
          FlutterI18n.translate(
              context, "product.page.feedback.tab_name.question_and_answer")),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 9),
        child: Column(
          children: [
            Flexible(
              child: ListView.builder(
                itemCount: questionAndAnswers.length,
                itemBuilder: (context, index) {
                  final QuestionAndAnswer questionAndAnswer =
                      questionAndAnswers.reversed.toList()[index];
                  return QuestionAndAnswerWidget(
                      questionAndAnswer: questionAndAnswer);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
