import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class DocumentsScreen extends StatefulWidget {
  static const routeName = "/documents";
  final List<ImageProvider> images;

  DocumentsScreen({Key key, @required this.images})
      : assert(images != null),
        super(key: key);

  @override
  _DocumentsScreenState createState() => _DocumentsScreenState();
}

class _DocumentsScreenState extends State<DocumentsScreen> {
  bool _fullScreen = false;
  int _currentIndex = 0;

  List<Widget> get nonEmptyBody {
    if (_fullScreen) {
      return [
        Flexible(
          fit: FlexFit.tight,
          child: Image(
            image: widget.images[_currentIndex],
            fit: BoxFit.cover,
          ),
        ),
        Space.vertical(28),
        Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                onPressed: () => setState(() {
                  _currentIndex = _currentIndex == 0
                      ? widget.images.length - 1
                      : --_currentIndex;
                }),
                icon: Icon(
                  Icons.arrow_back_ios_rounded,
                  size: 30,
                  color: customBlueColor,
                ),
              ),
              Space.horizontal(35),
              IconButton(
                onPressed: () => setState(() {
                  _currentIndex = _currentIndex == widget.images.length - 1
                      ? 0
                      : ++_currentIndex;
                }),
                icon: Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 30,
                  color: customRedColor,
                ),
              ),
            ],
          ),
        ),
        Space.vertical(32),
      ];
    } else {
      return [
        Flexible(
          child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 118 / 175,
              crossAxisSpacing: 2,
              mainAxisSpacing: 2,
            ),
            itemCount: widget.images.length,
            itemBuilder: (context, index) {
              return FittedBox(
                fit: BoxFit.fill,
                child: RawMaterialButton(
                  onPressed: () => setState(() {
                    _currentIndex = index;
                    _fullScreen = true;
                  }),
                  child: Image(
                    image: widget.images[index],
                    fit: BoxFit.cover,
                  ),
                ),
              );
            },
          ),
        ),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context, FlutterI18n.translate(context, "order.documents.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 27, vertical: 9),
        child: widget.images.isEmpty
            ? Text(
                FlutterI18n.translate(context, "order.documents.empty"),
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: customTextGrayColor1),
                textAlign: TextAlign.center,
              )
            : Padding(
                padding: EdgeInsets.only(top: 28),
                child: Column(
                  children: nonEmptyBody,
                ),
              ),
      ),
    );
  }
}
