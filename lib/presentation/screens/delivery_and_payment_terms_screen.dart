import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class DeliveryAndPaymentTermsScreen extends StatelessWidget {
  static String routeName = "/delivery_and_payment_terms";

  TextStyle text(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 12);
  }

  TextStyle title(BuildContext context) {
    return Theme.of(context).textTheme.headline1.copyWith(fontSize: 12);
  }

  TextStyle accountTypeTitle(BuildContext context) {
    return text(context).copyWith(color: customBlueColor);
  }

  String get deliveryTermsLocaleKey {
    return "info_screens.delivery_and_payment_terms.delivery_terms";
  }

  String get paymentTermsLocaleKey {
    return "info_screens.delivery_and_payment_terms.payment_terms";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(
          context,
          FlutterI18n.translate(
              context, "info_screens.delivery_and_payment_terms.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 34, vertical: 8),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                FlutterI18n.translate(
                    context, "$deliveryTermsLocaleKey.business_account.title"),
                style: accountTypeTitle(context),
              ),
              for (int i = 1; i <= 3; i++) ...[
                Text(
                  FlutterI18n.translate(context,
                      "info_screens.delivery_and_payment_terms.regions.$i"),
                  style: title(context),
                ),
                Text(
                  FlutterI18n.translate(context,
                      "$deliveryTermsLocaleKey.business_account.paragraphs.$i"),
                  style: text(context),
                ),
                Space.vertical(10)
              ],
              Text(
                FlutterI18n.translate(
                    context, "$deliveryTermsLocaleKey.personal_account.title"),
                style: accountTypeTitle(context),
              ),
              for (int i = 1; i <= 2; i++) ...[
                Text(
                  FlutterI18n.translate(context,
                      "info_screens.delivery_and_payment_terms.regions.$i"),
                  style: title(context),
                ),
                Text(
                  FlutterI18n.translate(context,
                      "$deliveryTermsLocaleKey.personal_account.paragraphs.$i"),
                  style: text(context),
                ),
                Space.vertical(10)
              ],
              Text(
                FlutterI18n.translate(
                    context, "$paymentTermsLocaleKey.business_account.title"),
                style: accountTypeTitle(context),
              ),
              Text(
                FlutterI18n.translate(context,
                    "$paymentTermsLocaleKey.business_account.paragraphs.1"),
                style: text(context),
              ),
              Space.vertical(10),
              Text(
                FlutterI18n.translate(
                    context, "$paymentTermsLocaleKey.personal_account.title"),
                style: accountTypeTitle(context),
              ),
              Text(
                FlutterI18n.translate(context,
                    "$paymentTermsLocaleKey.personal_account.paragraphs.1"),
                style: text(context),
              ),
              Space.vertical(10)
            ],
          ),
        ),
      ),
    );
  }
}
