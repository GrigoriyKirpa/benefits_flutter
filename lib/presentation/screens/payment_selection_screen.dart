import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/data/enums/payment_method.dart';
import 'package:benefit_flutter/data/models/card_token.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/models/payment.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/screens/company_details_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/credit_card_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/credit_card_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/main_navigation_screen.dart';
import 'package:benefit_flutter/presentation/screens/order_registered_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/payment_method_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:screen_loader/screen_loader.dart';

class PaymentSelectionScreen extends StatefulWidget {
  static const routeName = "/payment_selection";
  final Order order;

  PaymentSelectionScreen({Key key, @required this.order})
      : assert(order != null &&
            order.payment != null &&
            order.payment.price != null &&
            order.payment.delivery != null);
  @override
  _PaymentSelectionScreenState createState() => _PaymentSelectionScreenState();
}

class _PaymentSelectionScreenState extends State<PaymentSelectionScreen>
    with ScreenLoader {
  void registerOrder() async {
    try {
      await ServerRequests.createOrder(
          Shop.currentUser.info.type, widget.order, context);
    } catch (e) {
      System.displayErrorSnackBar(context,
          errorLocaleKey: "error.order_registration");
      return;
    }
    openPaymentResultPage(widget.order.payment);
  }

  void openPaymentResultPage(Payment payment) {
    Navigator.of(context, rootNavigator: true).pushReplacementNamed(
        OrderRegisteredScreen.routeName,
        arguments: payment);
  }

  void onCardPaymentSuccess(Order order, String transactionId) {
    ServerRequests.payOrder(order);
  }

  PaymentMethod _currentMethod = defaultMethod;
  @override
  Widget build(BuildContext context) {
    Map<PaymentMethod, VoidCallback> actions = {
      PaymentMethod.Cash: null,
      PaymentMethod.OnlineBanking: () => performFuture(() async {
            final List<CardToken> cardTokens =
                await ServerRequests.getCardTokens();
            Navigator.of(context, rootNavigator: true)
                .popUntil((route) => false);
            Navigator.pushNamed(context, MainNavigationScreen.routeName);
            if (cardTokens == null || cardTokens.isEmpty) {
              Navigator.of(context, rootNavigator: true).pushNamed(
                CreditCardEditorScreen.routeName,
                arguments: CreditCardEditorScreenArguments(
                  order: widget.order,
                  onSuccess: onCardPaymentSuccess,
                ),
              );
            } else {
              Navigator.of(context, rootNavigator: true).pushNamed(
                CreditCardListScreen.routeName,
                arguments: CreditCardListScreenArguments(
                  cardTokens: cardTokens,
                  order: widget.order,
                  onSuccess: onCardPaymentSuccess,
                ),
              );
            }
          }),
      PaymentMethod.ApplePay: null,
      PaymentMethod.Transfer: () {
        Navigator.of(context, rootNavigator: true).pushNamed(
            CompanyDetailsListScreen.routeName, arguments: (index) async {
          try {
            await ServerRequests.createOrder(
                AccountType.LegalEntity,
                widget.order,
                context,
                Shop.currentUser.info.companyDetails[index]);
          } catch (e) {
            System.displayErrorSnackBar(context,
                errorLocaleKey: "error.order_registration");
            return;
          }
          openPaymentResultPage(widget.order.payment);
        });
      },
      PaymentMethod.Contract: null,
    };
    return ScaffoldMessenger(
      key: UniqueKey(),
      child: loadableWidget(
        child: Scaffold(
          appBar: CustomWidgets.appbarWithText(
            context,
            FlutterI18n.translate(context, "order.payment.method.page.title"),
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 11),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Space.vertical(28),
                Flexible(
                  fit: FlexFit.tight,
                  child: ListView(
                    children: PaymentMethodExtension.availableMethods(
                            Shop.currentUser.info.type)
                        .map((method) {
                      return Padding(
                        padding: EdgeInsets.symmetric(vertical: 11),
                        child: PaymentMethodWidget(
                          method: method,
                          onSelected: () => setState(() {
                            _currentMethod = method;
                          }),
                          active: _currentMethod == method,
                        ),
                      );
                    }).toList(),
                  ),
                ),
                Center(
                  child: OutlinedButton(
                    onPressed: () {
                      widget.order.payment.method = _currentMethod;
                      final VoidCallback action = actions[_currentMethod];
                      if (action == null) {
                        registerOrder();
                      } else {
                        action();
                      }
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 9, horizontal: 45),
                      child: Text(
                        FlutterI18n.translate(context, "action.confirm.order"),
                      ),
                    ),
                  ),
                ),
                Space.vertical(76),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
