import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/user_check_status.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/password_reset_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class PasswordResetAccountSelectionScreen extends StatefulWidget {
  static const routeName = "/password_reset_acc_selection";
  _PasswordResetAccountSelectionScreenState createState() =>
      _PasswordResetAccountSelectionScreenState();
}

class _PasswordResetAccountSelectionScreenState
    extends State<PasswordResetAccountSelectionScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _loginController = TextEditingController();
  bool _error = false;
  UserCheckStatus _status = UserCheckStatus.Unknown;

  TextStyle get headline {
    return Theme.of(context)
        .textTheme
        .headline1
        .copyWith(color: Colors.white, fontSize: 30);
  }

  TextStyle get bodyText {
    return Theme.of(context)
        .textTheme
        .bodyText1
        .copyWith(color: Colors.white, fontSize: 18);
  }

  @override
  void dispose() {
    if (mounted) {
      _loginController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: loginPageColor,
      ),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            left: 27,
            right: 47,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(41),
              Text(
                FlutterI18n.translate(
                    context, "auth.screen.change_password.title"),
                style: headline,
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Space.vertical(30),
                    Text(
                      FlutterI18n.translate(context, "auth.email"),
                      style: bodyText,
                    ),
                    Space.vertical(6),
                    FormFields.compulsoryTextField(
                      context,
                      "",
                      _loginController,
                      maxLength: 50,
                      type: TextInputType.emailAddress,
                      disableHelpText: true,
                      validator: (text) =>
                          FormFields.emailFieldValidator(context, text),
                    ),
                  ],
                ),
              ),
              Space.vertical(28),
              if (_error)
                CustomWidgets.errorIcon(
                  context: context,
                  message: FlutterI18n.translate(context,
                      "auth.screen.change_password.error.${_status.errorKey}"),
                ),
            ],
          ),
        ),
        floatingActionButton: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            right: 46,
            left: 27,
            bottom: 92 - MediaQuery.of(context).viewInsets.bottom > 10
                ? 92 - MediaQuery.of(context).viewInsets.bottom
                : 10,
          ),
          child: Row(
            children: [
              Container(
                width: 21,
                height: 29,
                child: RawMaterialButton(
                  onPressed: () => Navigator.pop(context),
                  shape: CircleBorder(),
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  fillColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  child: Image.asset(
                    imageAssetBuilder("loginButtonBack.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 43,
                height: 73,
                child: FloatingActionButton(
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  foregroundColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  shape: CircleBorder(),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      final UserCheckStatus status =
                          await ServerRequests.checkUser(
                        _loginController.text,
                        Shop.currentUser.info.phone.nonFormatted,
                        (id) => Shop.currentUser.info.id = int.tryParse(id),
                      );
                      if (status == UserCheckStatus.Ok) {
                        Navigator.pushNamed(
                            context, PasswordResetScreen.routeName);
                      } else {
                        setState(() {
                          _loginController.text = "";
                          _error = true;
                          _status = status;
                        });
                      }
                    }
                  },
                  child: Image.asset(
                    imageAssetBuilder("loginButton.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        resizeToAvoidBottomInset: false,
      ),
    );
  }
}
