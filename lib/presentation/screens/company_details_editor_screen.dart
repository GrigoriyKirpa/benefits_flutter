import 'package:benefit_flutter/data/models/company_details.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CompanyDetailsEditorScreenParameters {
  final Function(CompanyDetails) onCreateButtonPressed;
  final CompanyDetails initialCompanyDetails;

  CompanyDetailsEditorScreenParameters({
    Function(CompanyDetails) onCreateButtonPressed,
    this.initialCompanyDetails,
  }) : this.onCreateButtonPressed = onCreateButtonPressed ?? ((_) => null);
}

class CompanyDetailsEditorScreen extends StatefulWidget {
  static const routeName = "/company_details_editor";
  final CompanyDetailsEditorScreenParameters params;

  CompanyDetailsEditorScreen({
    Key key,
    @required this.params,
  })  : assert(params != null),
        super(key: key);

  @override
  _CompanyDetailsEditorScreenState createState() =>
      _CompanyDetailsEditorScreenState();
}

class _CompanyDetailsEditorScreenState
    extends State<CompanyDetailsEditorScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  //final TextEditingController _kppController = TextEditingController();
  final TextEditingController _innController = TextEditingController();

  @override
  void initState() {
    if (widget.params.initialCompanyDetails != null) {
      _nameController.text = widget.params.initialCompanyDetails.name;
      //_kppController.text = widget.params.initialCompanyDetails.kpp;
      _innController.text = widget.params.initialCompanyDetails.inn;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomWidgets.appbarWithText(context,
          FlutterI18n.translate(context, "action.add.company_details")),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Space.vertical(26),
            Flexible(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 27),
                child: ListView(
                  children: [
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.company_details.editor.name"),
                      _nameController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                      maxLength: 50,
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.company_details.editor.inn"),
                      _innController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                      maxLength: 50,
                    ),
                    /*Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.company_details.editor.kpp"),
                      _kppController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                      maxLength: 50,
                    ),*/
                  ],
                ),
              ),
            ),
            OutlinedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  widget.params.onCreateButtonPressed(
                    CompanyDetails(
                      name: _nameController.text,
                      inn: _innController.text,
                      //kpp: _kppController.text,
                    ),
                  );
                  Navigator.of(context).pop();
                }
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 9.0, horizontal: 75),
                child: Text(
                  FlutterI18n.translate(context, "action.continue.default"),
                ),
              ),
            ),
            Space.vertical(32),
          ],
        ),
      ),
    );
  }
}
