import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/card_token.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/payment_api.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/screens/credit_card_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/three_ds_verification_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:screen_loader/screen_loader.dart';

import 'order_registered_screen.dart';

class CreditCardListScreenArguments {
  final List<CardToken> cardTokens;
  final Order order;
  final Function(Order, String) onSuccess;

  CreditCardListScreenArguments(
      {@required this.cardTokens,
      @required this.order,
      Function(Order, String) onSuccess})
      : this.onSuccess = onSuccess ?? ((_, __) => null),
        assert(order != null && order.payment != null),
        assert(cardTokens != null && cardTokens.isNotEmpty);
}

class CreditCardListScreen extends StatefulWidget {
  static const routeName = "/card_list";
  final CreditCardListScreenArguments arguments;

  CreditCardListScreen({@required this.arguments}) : assert(arguments != null);

  _CreditCardListScreenState createState() => _CreditCardListScreenState();
}

class _CreditCardListScreenState extends State<CreditCardListScreen>
    with ScreenLoader {
  List<CardToken> _cardTokens;
  Order _order;

  String get localeKey {
    return "order.payment.card.wallet";
  }

  @override
  void initState() {
    _cardTokens = widget.arguments.cardTokens.reversed.toList();
    super.initState();
  }

  void reload() {
    if (widget.arguments.cardTokens.isEmpty) {
      Navigator.of(context, rootNavigator: true).pushReplacementNamed(
        CreditCardEditorScreen.routeName,
        arguments: CreditCardEditorScreenArguments(
          order: _order ?? widget.arguments.order,
          onSuccess: widget.arguments.onSuccess,
          onOrderCreated: (_newOrder) => _order = _newOrder,
        ),
      );
    } else {
      setState(() {
        _cardTokens = widget.arguments.cardTokens;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        child: loadableWidget(
          child: Scaffold(
            appBar: CustomWidgets.appbarWithText(
                context, FlutterI18n.translate(context, "$localeKey.title")),
            resizeToAvoidBottomInset: false,
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  fit: FlexFit.tight,
                  child: ListView.builder(
                    padding: EdgeInsets.symmetric(vertical: 9),
                    itemCount: _cardTokens.length,
                    itemBuilder: (context, index) {
                      final CardToken cardToken = _cardTokens[index];
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 27.0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: RawMaterialButton(
                                onPressed: () async => await performFuture(
                                  () async {
                                    if (_order == null) {
                                      try {
                                        _order =
                                            await ServerRequests.createOrder(
                                                Shop.currentUser.info.type,
                                                widget.arguments.order,
                                                context);
                                      } catch (e) {
                                        System.displayErrorSnackBar(context,
                                            errorLocaleKey:
                                                "error.order_registration");
                                        return;
                                      }
                                    }

                                    final paymentApi = PaymentAPI(
                                      userId: Shop.currentUser.info.id,
                                      email: Shop.currentUser.info.email,
                                      order: _order,
                                      on3dsRequired: (md, acs, paReq) async {
                                        return await Navigator.of(context,
                                                rootNavigator: true)
                                            .pushNamed(
                                          ThreeDsVerificationScreen.routeName,
                                          arguments:
                                              ThreeDsVerificationArguments(
                                            transactionId: md,
                                            paReq: paReq,
                                            acsUrl: acs,
                                          ),
                                        );
                                      },
                                      onSuccess: (cardToken, transactionId) {
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .pushReplacementNamed(
                                                OrderRegisteredScreen.routeName,
                                                arguments: _order.payment);
                                        widget.arguments
                                            .onSuccess(_order, transactionId);
                                      },
                                      onFailure: (message) {
                                        System.displayErrorSnackBarWithMessage(
                                            context, message);
                                      },
                                      onServerError: () =>
                                          System.displayErrorSnackBar(context,
                                              errorLocaleKey: "error.server"),
                                    );
                                    await paymentApi
                                        .chargeCardByToken(cardToken.token);
                                  },
                                ),
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.all(18),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "**** ${cardToken.lastDigits}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1
                                            .copyWith(fontSize: 24),
                                      ),
                                      Text(
                                        "${cardToken.cardType}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1
                                            .copyWith(
                                                fontSize: 24,
                                                color: customBlueColor),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Space.vertical(4),
                          TextButton(
                            onPressed: () {
                              widget.arguments.cardTokens.remove(cardToken);
                              ServerRequests.updateCardTokens(
                                  widget.arguments.cardTokens);
                              reload();
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.delete,
                                  color: customTextGrayColor1,
                                  size: 18,
                                ),
                                Space.horizontal(14),
                                Text(
                                  FlutterI18n.translate(
                                      context, "action.delete.card"),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline1
                                      .copyWith(
                                        fontSize: 14,
                                        color: customTextGrayColor1,
                                      ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                OutlinedButton(
                  style: Theme.of(context).outlinedButtonTheme.style.copyWith(
                    backgroundColor: MaterialStateProperty.resolveWith<Color>(
                      (Set<MaterialState> states) {
                        if (states.contains(MaterialState.pressed) ||
                            states.contains(MaterialState.hovered) ||
                            states.contains(MaterialState.selected) ||
                            states.contains(MaterialState.focused))
                          return Colors.black.withOpacity(0.5);
                        return Colors.black; // Use the component's default.
                      },
                    ),
                  ),
                  onPressed: () =>
                      Navigator.of(context, rootNavigator: true).pushNamed(
                    CreditCardEditorScreen.routeName,
                    arguments: CreditCardEditorScreenArguments(
                      order: _order ?? widget.arguments.order,
                      onSuccess: widget.arguments.onSuccess,
                      cardTokens: widget.arguments.cardTokens,
                      onOrderCreated: (_newOrder) => _order = _newOrder,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 9.0, horizontal: 75),
                    child: Text(
                      FlutterI18n.translate(context, "action.choose.different"),
                    ),
                  ),
                ),
                Space.vertical(70),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
