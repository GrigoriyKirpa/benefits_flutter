import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/user_check_status.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/account_type_selection_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class RegistrationScreen extends StatefulWidget {
  static const routeName = "/registration";
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _loginController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _rePasswordController = TextEditingController();
  bool _error = false;
  UserCheckStatus _status = UserCheckStatus.Unknown;

  TextStyle get headline {
    return Theme.of(context)
        .textTheme
        .headline1
        .copyWith(color: Colors.white, fontSize: 30);
  }

  TextStyle get bodyText {
    return Theme.of(context)
        .textTheme
        .bodyText1
        .copyWith(color: Colors.white, fontSize: 18);
  }

  @override
  void dispose() {
    if (mounted) {
      _loginController.dispose();
      _passwordController.dispose();
      _rePasswordController.dispose();
    }
    super.dispose();
  }

  String validatePassword(BuildContext context, String text) {
    final String errorText = FormFields.passwordFieldValidator(context, text);
    return errorText ?? _passwordController.text == _rePasswordController.text
        ? null
        : FlutterI18n.translate(context, "auth.password.error");
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: loginPageColor,
      ),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            left: 27,
            right: 47,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(41),
              Text(
                FlutterI18n.translate(context, "auth.screen.register.title"),
                style: headline,
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Space.vertical(30),
                    Text(
                      FlutterI18n.translate(context, "auth.email"),
                      style: bodyText,
                    ),
                    Space.vertical(6),
                    FormFields.compulsoryTextField(
                      context,
                      "",
                      _loginController,
                      maxLength: 50,
                      type: TextInputType.emailAddress,
                      disableHelpText: true,
                      validator: (text) =>
                          FormFields.emailFieldValidator(context, text),
                    ),
                    Space.vertical(12),
                    Text(
                      FlutterI18n.translate(context, "auth.password.default"),
                      style: bodyText,
                    ),
                    Space.vertical(6),
                    FormFields.compulsoryTextField(
                      context,
                      "",
                      _passwordController,
                      type: TextInputType.text,
                      disableHelpText: true,
                      isPassword: true,
                      validator: (text) => validatePassword(context, text),
                    ),
                    Space.vertical(12),
                    Text(
                      FlutterI18n.translate(context, "auth.password.repeat"),
                      style: bodyText,
                    ),
                    Space.vertical(6),
                    FormFields.compulsoryTextField(
                      context,
                      "",
                      _rePasswordController,
                      type: TextInputType.text,
                      disableHelpText: true,
                      isPassword: true,
                      validator: (text) => validatePassword(context, text),
                    ),
                  ],
                ),
              ),
              Space.vertical(28),
              if (_error)
                CustomWidgets.errorIcon(
                  context: context,
                  message: FlutterI18n.translate(context,
                      "auth.screen.register.error.${_status.errorKey}"),
                ),
            ],
          ),
        ),
        floatingActionButton: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            right: 46,
            left: 27,
            bottom: 92 - MediaQuery.of(context).viewInsets.bottom > 10
                ? 92 - MediaQuery.of(context).viewInsets.bottom
                : 10,
          ),
          child: Row(
            children: [
              Container(
                width: 21,
                height: 29,
                child: RawMaterialButton(
                  onPressed: () => Navigator.pop(context),
                  shape: CircleBorder(),
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  fillColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  child: Image.asset(
                    imageAssetBuilder("loginButtonBack.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 43,
                height: 73,
                child: FloatingActionButton(
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  foregroundColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  shape: CircleBorder(),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      final UserCheckStatus status =
                          await ServerRequests.register(
                        _loginController.text,
                        _passwordController.text,
                        Shop.currentUser.info.phone,
                        (info) => Shop.currentUser.info = info,
                      );
                      if (status == UserCheckStatus.Ok) {
                        Shop.saveUserInfo();
                        Navigator.pushReplacementNamed(
                            context, AccountTypeSelectionScreen.routeName);
                      } else {
                        setState(() {
                          _error = true;
                          _status = status;
                          _passwordController.text = "";
                          _rePasswordController.text = "";
                        });
                      }
                    }
                  },
                  child: Image.asset(
                    imageAssetBuilder("loginButton.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        resizeToAvoidBottomInset: false,
      ),
    );
  }
}
