import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/user_check_status.dart';
import 'package:benefit_flutter/data/models/user.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/account_type_selection_screen.dart';
import 'package:benefit_flutter/presentation/screens/main_navigation_screen.dart';
import 'package:benefit_flutter/presentation/screens/password_reset_account_selection_screen.dart';
import 'package:benefit_flutter/presentation/screens/registration_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = "/login";
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _loginController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _error = false;
  UserCheckStatus _status = UserCheckStatus.Unknown;

  TextStyle get headline {
    return Theme.of(context)
        .textTheme
        .headline1
        .copyWith(color: Colors.white, fontSize: 30);
  }

  TextStyle get bodyText {
    return Theme.of(context)
        .textTheme
        .bodyText1
        .copyWith(color: Colors.white, fontSize: 18);
  }

  @override
  void dispose() {
    if (mounted) {
      _loginController.dispose();
      _passwordController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: loginPageColor,
      ),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            left: 27,
            right: 47,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(41),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    FlutterI18n.translate(context, "auth.screen.login.title"),
                    style: headline,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(
                          context, RegistrationScreen.routeName);
                    },
                    child: Text(
                      FlutterI18n.translate(context, "action.register.default"),
                      style: headline.copyWith(fontSize: 12),
                    ),
                  ),
                ],
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Space.vertical(30),
                    Text(
                      FlutterI18n.translate(context, "auth.email"),
                      style: bodyText,
                    ),
                    Space.vertical(6),
                    FormFields.compulsoryTextField(
                      context,
                      "",
                      _loginController,
                      type: TextInputType.emailAddress,
                      disableHelpText: true,
                      maxLength: 50,
                      validator: (text) =>
                          FormFields.emailFieldValidator(context, text),
                    ),
                    Space.vertical(12),
                    Text(
                      FlutterI18n.translate(context, "auth.password.default"),
                      style: bodyText,
                    ),
                    Space.vertical(6),
                    FormFields.compulsoryTextField(
                      context,
                      "",
                      _passwordController,
                      type: TextInputType.text,
                      disableHelpText: true,
                      isPassword: true,
                      validator: (text) =>
                          FormFields.passwordFieldValidator(context, text),
                    ),
                  ],
                ),
              ),
              Space.vertical(48),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(
                      context, PasswordResetAccountSelectionScreen.routeName);
                },
                child: Text(
                  FlutterI18n.translate(context, "auth.password.forgot"),
                  style:
                      headline.copyWith(fontSize: 12, color: customBlueColor),
                ),
              ),
              Space.vertical(24),
              if (_error)
                CustomWidgets.errorIcon(
                  context: context,
                  message: FlutterI18n.translate(
                      context, "auth.screen.login.error.${_status.errorKey}"),
                ),
            ],
          ),
        ),
        floatingActionButton: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            right: 46,
            left: 27,
            bottom: 92 - MediaQuery.of(context).viewInsets.bottom > 10
                ? 92 - MediaQuery.of(context).viewInsets.bottom
                : 10,
          ),
          child: Row(
            children: [
              Container(
                width: 21,
                height: 29,
                child: RawMaterialButton(
                  onPressed: () => Navigator.pop(context),
                  shape: CircleBorder(),
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  fillColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  child: Image.asset(
                    imageAssetBuilder("loginButtonBack.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 43,
                height: 73,
                child: FloatingActionButton(
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  foregroundColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  shape: CircleBorder(),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      final UserCheckStatus status =
                          await ServerRequests.checkUser(_loginController.text,
                              Shop.currentUser.info.phone.nonFormatted);
                      if (status == UserCheckStatus.Ok) {
                        final UserInfo info = await ServerRequests.login(
                            _loginController.text,
                            _passwordController.text,
                            Shop.currentUser.info.phone);
                        if (info != null) {
                          Shop.currentUser.info = info;
                          Shop.saveUserInfo();
                          if (info.type != null) {
                            Navigator.pushNamedAndRemoveUntil(context,
                                MainNavigationScreen.routeName, (_) => false);
                          } else {
                            Navigator.pushNamed(
                                context, AccountTypeSelectionScreen.routeName);
                          }
                        } else {
                          setState(() {
                            _error = true;
                            _status = UserCheckStatus.ErrorPassword;
                            _passwordController.text = "";
                          });
                        }
                      } else {
                        setState(() {
                          _loginController.text = "";
                          _passwordController.text = "";
                          _error = true;
                          _status = status;
                        });
                      }
                    }
                  },
                  child: Image.asset(
                    imageAssetBuilder("loginButton.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        resizeToAvoidBottomInset: false,
      ),
    );
  }
}
