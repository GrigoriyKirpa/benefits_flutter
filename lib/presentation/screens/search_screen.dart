import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/sort_by.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/product_list_widget.dart';
import 'package:benefit_flutter/presentation/widgets/sort_button.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SearchScreen extends StatefulWidget {
  static const routeName = "/search";
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final TextEditingController _searchController = TextEditingController();
  static GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<ProductDescriptor> _products;
  List<String> _searchHistory =
      Shop.currentUser.info.searchHistory.reversed.toList();
  bool _loading = false;
  SortBy _sortBy = SortBy.values.first;

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  Future<void> search(String query) async {
    query = query.trim().replaceAll(RegExp(' +'), ' ').toLowerCase();
    if (!mounted) return;
    try {
      final List<ProductDescriptor> products =
          await ServerRequests.search(query);
      Shop.addSearch(query);
      setState(() {
        _products = products;
        _searchHistory = Shop.currentUser.info.searchHistory.reversed.toList();
        _loading = false;
      });
    } catch (e) {
      setState(() {
        _products = null;
        _loading = false;
      });
      System.displayErrorSnackBar(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: UniqueKey(),
      child: Scaffold(
        appBar: CustomWidgets.appbarWithText(
          context,
          FlutterI18n.translate(context, "search.page.title"),
        ),
        body: Column(
          crossAxisAlignment: _products != null && _products.isNotEmpty
              ? CrossAxisAlignment.center
              : CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 10),
              margin: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                border: Border.all(width: 0.3, color: customTextGrayColor2),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    CustomIcons.search,
                    size: 16,
                    color: disabledColor,
                  ),
                  Space.horizontal(10),
                  Flexible(
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        autofocus: true,
                        onEditingComplete: () {
                          final String query = _searchController.text;
                          FocusScope.of(context).unfocus();
                          if (query.isEmpty ||
                              query.replaceAll(" ", "").isEmpty) {
                            _searchController.text = "";
                            setState(() {
                              _products = null;
                            });
                          } else {
                            setState(() {
                              _loading = true;
                              search(query);
                            });
                          }
                        },
                        maxLines: 1,
                        maxLength: 30,
                        textCapitalization: TextCapitalization.sentences,
                        controller: _searchController,
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontSize: 18),
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.only(top: 8),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          helperStyle: TextStyle(fontSize: 0),
                          hintText: FlutterI18n.translate(
                              context, "search.searchHintText"),
                        ),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Space.vertical(11),
            if (_loading)
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 11.0),
                  child: LinearProgressIndicator(),
                ),
              ),
            if (!_loading && _products != null && _products.isNotEmpty)
              ...searchResults,
            if (!_loading &&
                (_products == null || _products.isEmpty) &&
                _searchHistory.isNotEmpty)
              ...searchHistory,
          ],
        ),
        resizeToAvoidBottomInset: false,
      ),
    );
  }

  List<Widget> get searchHistory {
    return [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 27),
        child: Text(
          FlutterI18n.translate(context, "search.page.history"),
          style: Theme.of(context)
              .textTheme
              .headline1
              .copyWith(fontSize: 14, color: customTextGrayColor1),
        ),
      ),
      Space.vertical(14),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 27),
        child: Wrap(
          spacing: 8,
          runSpacing: 8,
          children: List.generate(_searchHistory.length, (index) {
            final String searchItem = _searchHistory[index];
            return RawMaterialButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                setState(() {
                  _loading = true;
                  search(searchItem);
                });
              },
              padding: EdgeInsets.symmetric(vertical: 6, horizontal: 8),
              fillColor: Colors.white,
              elevation: 0,
              highlightElevation: 0,
              focusElevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                searchItem,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            );
          }),
        ),
      ),
      if (_products != null && _products.isEmpty)
        Flexible(
          child: Center(
            child: Text(
              FlutterI18n.translate(context, "empty.search"),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: customGrayColor1),
              textAlign: TextAlign.center,
            ),
          ),
        ),
    ];
  }

  List<Widget> get searchResults {
    return [
      Text(
        "${_products.length} ${FlutterI18n.plural(
          context,
          "product.products.number",
          Language.getPlural(_products.length),
        )}",
        style: Theme.of(context)
            .textTheme
            .headline5
            .copyWith(color: customTextGrayColor1),
      ),
      Space.vertical(14),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 11.0),
        child: Row(
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: SortButton(
                sortBy: _sortBy,
                onSortOptionChanged: (option) => setState(
                  () {
                    _sortBy = option;
                  },
                ),
              ),
            ),
            // Space.horizontal(11),
            // Flexible(
            //   fit: FlexFit.tight,
            //   child: FilterButton(),
            // ),
          ],
        ),
      ),
      Space.vertical(5),
      Flexible(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 11.0),
          child: ListView.builder(
            itemCount: _products.length,
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: customGrayColor1,
                    ),
                  ),
                ),
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ProductListWidget(
                  product: _products[index],
                ),
              );
            },
          ),
        ),
      ),
    ];
  }
}
