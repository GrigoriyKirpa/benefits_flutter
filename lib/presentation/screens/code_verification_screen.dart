import 'dart:async';

import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/login_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class CodeVerificationScreen extends StatefulWidget {
  static const routeName = "/code_verification";
  final String verificationId;
  CodeVerificationScreen({Key key, @required this.verificationId});
  _CodeVerificationScreenState createState() => _CodeVerificationScreenState();
}

class _CodeVerificationScreenState extends State<CodeVerificationScreen> {
  Timer _timer;
  String _code = "";
  int _timeLeft = codeVerificationTimeout;
  bool _error = false;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_timeLeft == 0) {
          Navigator.pop(context);
        } else {
          setState(() {
            _timeLeft--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  TextStyle get headline {
    return Theme.of(context)
        .textTheme
        .headline1
        .copyWith(color: Colors.white, fontSize: 30);
  }

  TextStyle get bodyText {
    return Theme.of(context)
        .textTheme
        .bodyText1
        .copyWith(color: Colors.white, fontSize: 18);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: loginPageColor,
      ),
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(
            horizontal: 27,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Space.vertical(42),
              Text(
                FlutterI18n.translate(context,
                    "auth.screen.phone_verification.verification_code"),
                style: headline,
              ),
              Space.vertical(27),
              Text(
                FlutterI18n.translate(
                    context, "auth.screen.phone_verification.enter_code"),
                style: bodyText,
              ),
              Space.vertical(50),
              VerificationCode(
                textStyle: bodyText.copyWith(fontSize: 24),
                underlineColor: Color(0xFFFFFF).withOpacity(0.2),
                keyboardType: TextInputType.number,
                length: 6,
                digitsOnly: true,
                underlineWidth: 1,
                onCompleted: (String value) {
                  setState(() {
                    _code = value;
                  });
                },
                onEditing: (bool value) {
                  setState(() {
                    _code = "";
                  });
                },
              ),
              Space.vertical(24),
              if (_error)
                CustomWidgets.errorIcon(
                  context: context,
                  message: FlutterI18n.translate(
                      context, "auth.screen.phone_verification.error"),
                ),
              Spacer(
                flex: 2,
              ),
              Text(
                "${FlutterI18n.translate(context, "auth.screen.phone_verification.code_will_expire")} ${(_timeLeft ~/ 60).toString().padLeft(2, '0')}:${(_timeLeft % 60).toString().padLeft(2, '0')}",
                style: headline.copyWith(
                  fontSize: 12,
                  color: Colors.white,
                ),
              ),
              Spacer(
                flex: 7,
              ),
            ],
          ),
        ),
        floatingActionButton: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            right: 46,
            left: 27,
            bottom: 92 - MediaQuery.of(context).viewInsets.bottom > 10
                ? 92 - MediaQuery.of(context).viewInsets.bottom
                : 10,
          ),
          child: Row(
            children: [
              Container(
                width: 21,
                height: 29,
                child: RawMaterialButton(
                  onPressed: () => Navigator.pop(context),
                  shape: CircleBorder(),
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  fillColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  child: Image.asset(
                    imageAssetBuilder("loginButtonBack.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: 43,
                height: 73,
                child: FloatingActionButton(
                  splashColor: Colors.white.withOpacity(0.1),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  foregroundColor: Colors.transparent,
                  focusColor: Colors.white.withOpacity(0.1),
                  shape: CircleBorder(),
                  onPressed: () {
                    var credential = PhoneAuthProvider.credential(
                        verificationId: widget.verificationId, smsCode: _code);
                    ServerRequests.auth
                        .signInWithCredential(credential)
                        .then((result) {
                      Shop.saveUserInfo();
                      Navigator.pushReplacementNamed(
                          context, LoginScreen.routeName);
                    }).catchError(
                      (e) {
                        setState(() {
                          _error = true;
                        });
                        print(e);
                      },
                    );
                  },
                  child: Image.asset(
                    imageAssetBuilder("loginButton.png"),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
