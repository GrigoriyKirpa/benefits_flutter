import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/company_details.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/company_details_editor_screen.dart';
import 'package:benefit_flutter/presentation/widgets/company_details_list_item.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CompanyDetailsListScreen extends StatefulWidget {
  static const routeName = "/company_details";
  final Function(int) onContinueButtonPressed;

  CompanyDetailsListScreen({Key key, this.onContinueButtonPressed});

  @override
  _CompanyDetailsListScreenState createState() =>
      _CompanyDetailsListScreenState();
}

class _CompanyDetailsListScreenState extends State<CompanyDetailsListScreen> {
  List<CompanyDetails> _companyDetailsList = [];
  int _selectedCompanyDetailsIndex;
  bool inactive = false;

  @override
  void initState() {
    if (Shop.currentUser.info.companyDetails.isNotEmpty) {
      _companyDetailsList = Shop.currentUser.info.companyDetails;
    }
    if (widget.onContinueButtonPressed == null) {
      inactive = true;
    }
    super.initState();
  }

  bool isSelected(int index) {
    return index == _selectedCompanyDetailsIndex;
  }

  void handleSelection(int index) {
    if (inactive) return;
    if (_selectedCompanyDetailsIndex != index) {
      setState(() {
        _selectedCompanyDetailsIndex = index;
      });
    }
  }

  void reload() {
    setState(() {
      if (Shop.currentUser.info.companyDetails.isNotEmpty) {
        _companyDetailsList = Shop.currentUser.info.companyDetails;
      }
    });
    saveChanges();
  }

  void saveChanges() {
    Shop.saveUserInfo();
    ServerRequests.updateUserInfo(Shop.currentUser.info);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(context,
          FlutterI18n.translate(context, "user.company_details.page.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
        child: Column(
          children: [
            Space.vertical(18),
            RawMaterialButton(
              onPressed: () => Navigator.pushNamed(
                context,
                CompanyDetailsEditorScreen.routeName,
                arguments: CompanyDetailsEditorScreenParameters(
                  onCreateButtonPressed: (companyDetails) {
                    Shop.currentUser.info.companyDetails.add(companyDetails);
                    handleSelection(Shop.currentUser.info.companyDetails
                        .indexOf(companyDetails));
                  },
                ),
              ).then((_) => reload()),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 26),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: customBlueColor,
                    width: 1,
                  ),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        Icons.add,
                        size: 18,
                        color: customBlueColor,
                      ),
                      Space.horizontal(10),
                      Text(
                        FlutterI18n.translate(
                            context, "action.add.company_details"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: customBlueColor),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Space.vertical(24),
            Flexible(
              child: ListView.builder(
                itemCount: _companyDetailsList.length,
                itemBuilder: (context, index) {
                  CompanyDetails companyDetails = _companyDetailsList[index];
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    child: CompanyDetailsListItem(
                      companyDetails: companyDetails,
                      headerColor:
                          isSelected(index) ? customBlueColor : Colors.black,
                      onItemPressed:
                          inactive ? null : () => handleSelection(index),
                      onEditButtonPressed: () => Navigator.pushNamed(
                        context,
                        CompanyDetailsEditorScreen.routeName,
                        arguments: CompanyDetailsEditorScreenParameters(
                          onCreateButtonPressed: (companyDetails) => Shop
                              .currentUser.info.companyDetails[index]
                              .editFrom(companyDetails),
                          initialCompanyDetails: companyDetails,
                        ),
                      ).then(
                        (_) => reload(),
                      ),
                    ),
                  );
                },
              ),
            ),
            if (!inactive) ...[
              OutlinedButton(
                onPressed: _selectedCompanyDetailsIndex == null
                    ? null
                    : () => widget
                        .onContinueButtonPressed(_selectedCompanyDetailsIndex),
                style: Theme.of(context).outlinedButtonTheme.style.copyWith(
                      backgroundColor: _selectedCompanyDetailsIndex == null
                          ? MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                return customGrayColor1;
                              },
                            )
                          : MaterialStateProperty.resolveWith<Color>(
                              (Set<MaterialState> states) {
                                if (states.contains(MaterialState.pressed) ||
                                    states.contains(MaterialState.hovered) ||
                                    states.contains(MaterialState.selected) ||
                                    states.contains(MaterialState.focused))
                                  return customBlueColor.withOpacity(0.5);
                                return customBlueColor; // Use the component's default.
                              },
                            ),
                    ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 9.0, horizontal: 75),
                  child: Text(
                    FlutterI18n.translate(context, "action.continue.default"),
                  ),
                ),
              ),
              Space.vertical(32)
            ],
          ],
        ),
      ),
    );
  }
}
