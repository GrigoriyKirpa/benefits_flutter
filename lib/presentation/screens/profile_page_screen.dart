import 'dart:io';

import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/account_type.dart';
import 'package:benefit_flutter/data/enums/profile_actions.dart';
import 'package:benefit_flutter/data/models/user.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/address_book_screen.dart';
import 'package:benefit_flutter/presentation/screens/company_details_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/contacts_screen.dart';
import 'package:benefit_flutter/presentation/screens/delivery_and_payment_terms_screen.dart';
import 'package:benefit_flutter/presentation/screens/faq_screen.dart';
import 'package:benefit_flutter/presentation/screens/phone_verification_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePageScreen extends StatefulWidget {
  static const routeName = "/profile";
  @override
  _ProfilePageScreenState createState() => _ProfilePageScreenState();
}

class _ProfilePageScreenState extends State<ProfilePageScreen> {
  final User _user = Shop.currentUser;
  final UserInfo _manager = Shop.manager;
  final String priceType = "Не задан";

  void openWhatsapp() async {
    final String whatsappURl_android =
        "whatsapp://send?phone=${_manager.phone.nonFormatted}";
    final String whatappURL_ios =
        "https://wa.me/${_manager.phone.nonFormatted}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunch(whatappURL_ios)) {
        await launch(whatappURL_ios, forceSafariVC: false);
      } else {
        noWhatsappNotification();
      }
    } else {
      // android , web
      if (await canLaunch(whatsappURl_android)) {
        await launch(whatsappURl_android);
      } else {
        noWhatsappNotification();
      }
    }
  }

  void noWhatsappNotification() {
    final SnackBar snackBar = CustomWidgets.textSnackBar(
      customRedColor,
      FlutterI18n.translate(context, "user.profile.no_whatsapp_message"),
      Theme.of(context)
          .textTheme
          .bodyText1
          .copyWith(fontSize: 20, color: Colors.white),
    );
    System.displaySnackBar(context, snackBar);
  }

  void logout() {
    Shop.resetShop();
    Navigator.of(
      context,
      rootNavigator: true,
    ).popUntil((route) => false);
    Navigator.of(
      context,
      rootNavigator: true,
    ).pushNamed(PhoneVerificationScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    Map<ProfileActions, VoidCallback> actions = {
      ProfileActions.Messenger: () => openWhatsapp(),
      ProfileActions.QuestionAndAnswer: () =>
          Navigator.pushNamed(context, FAQScreen.routeName),
      // ProfileActions.AddFiles: () => System.pickFiles(),
      ProfileActions.DeliveryTermsAndConditions: () =>
          Navigator.pushNamed(context, DeliveryAndPaymentTermsScreen.routeName),
      ProfileActions.CompanyContacts: () =>
          Navigator.pushNamed(context, ContactsScreen.routeName),
      ProfileActions.DeliveryAddress: () =>
          Navigator.pushNamed(context, AddressBookScreen.routeName),
      // ProfileActions.CatalogType: () => null,
    };
    if (_user.info.type == AccountType.LegalEntity) {
      actions[ProfileActions.CompanyDetails] = () =>
          Navigator.pushNamed(context, CompanyDetailsListScreen.routeName);
    }
    return Theme(
      data: Theme.of(context).copyWith(
        iconTheme: IconThemeData(
          color: customGrayColor1,
          size: 32,
        ),
      ),
      child: ScaffoldMessenger(
        key: UniqueKey(),
        child: Container(
          color: Theme.of(context).backgroundColor,
          child: SafeArea(
            child: Scaffold(
              appBar: CustomWidgets.appbar(
                context,
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Center(
                      child: Image.asset(
                        imageAssetBuilder('appBar.png'),
                      ),
                    ),
                  ],
                ),
                true,
              ),
              body: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Space.vertical(6),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 27),
                      child: Row(
                        children: [
                          Icon(
                            CustomIcons.phone_call,
                          ),
                          Space.horizontal(11),
                          Text(
                            "${_user.info.phone.toString()}",
                            style: Theme.of(context)
                                .textTheme
                                .headline5
                                .copyWith(fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                    Space.vertical(3),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(left: 67, right: 27),
                      child: Row(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                FlutterI18n.translate(
                                    context, "user.profile.your_manager"),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(color: customGrayColor1),
                              ),
                              Text(
                                _manager.fullName,
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ],
                          ),
                          Spacer(),
                          Icon(
                            CustomIcons.shopping_cart,
                            size: 44,
                            color: customGrayColor1,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                FlutterI18n.translate(
                                    context, "user.profile.price_type.title"),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline5
                                    .copyWith(color: customGrayColor1),
                              ),
                              Text(
                                priceType,
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Space.vertical(25),
                    Flexible(
                      fit: FlexFit.tight,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: actions.length,
                        itemBuilder: (context, index) {
                          ProfileActions action = actions.keys.elementAt(index);
                          return RawMaterialButton(
                            onPressed: actions[action],
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 27, vertical: 9),
                              decoration: BoxDecoration(
                                border: Border(
                                  top: index == 0
                                      ? BorderSide(
                                          width: 0.5, color: customGrayColor1)
                                      : BorderSide.none,
                                  bottom: BorderSide(
                                      width: 0.5, color: customGrayColor1),
                                ),
                              ),
                              child: Row(
                                children: [
                                  Icon(
                                    action.icon,
                                    color: action.iconColor,
                                  ),
                                  Space.horizontal(12),
                                  Text(
                                    action.label(context),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline5
                                        .copyWith(
                                            fontSize: 18,
                                            color: action.textColor),
                                  ),
                                  Spacer(),
                                  Icon(
                                    Icons.arrow_forward_ios_rounded,
                                    size: 24,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    Space.vertical(29),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 27.0, vertical: 4),
                      child: Text(
                        FlutterI18n.translate(context, "user.profile.email"),
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 27.0),
                      child: SelectableText(
                        _manager.email,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    Space.vertical(8),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 27.0, vertical: 4),
                      child: Text(
                        FlutterI18n.translate(
                            context, "user.profile.messenger_links"),
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 27.0),
                      child: SelectableText(
                        _manager.phone.toString(),
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    Space.vertical(8),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 27.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            imageAssetBuilder('telegram.png'),
                          ),
                          Space.horizontal(8),
                          Image.asset(
                            imageAssetBuilder('viber.png'),
                          ),
                          Space.horizontal(8),
                          Image.asset(
                            imageAssetBuilder('whatsapp.png'),
                          ),
                          Spacer(),
                          IconButton(
                            onPressed: () => logout(),
                            icon: Icon(
                              CustomIcons.logout,
                              size: 32,
                              color: customGrayColor1,
                            ),
                          )
                        ],
                      ),
                    ),
                    Space.vertical(40),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
