import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/card_token.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/payment_api.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/screens/three_ds_verification_screen.dart';
import 'package:benefit_flutter/presentation/widgets/circled_checkbox.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_brand.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:flutter_credit_card/custom_card_type_icon.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:screen_loader/screen_loader.dart';

import 'order_registered_screen.dart';

class CreditCardEditorScreenArguments {
  final List<CardToken> cardTokens;
  final Order order;
  final Function(Order) onOrderCreated;
  final Function(Order, String) onSuccess;

  CreditCardEditorScreenArguments({
    List<CardToken> cardTokens,
    @required this.order,
    Function(Order, String) onSuccess,
    Function(Order) onOrderCreated,
  })  : this.onOrderCreated = onOrderCreated ?? ((_) => null),
        this.cardTokens = cardTokens ?? [],
        this.onSuccess = onSuccess ?? ((_, __) => null),
        assert(order != null && order.payment != null);
}

class CreditCardEditorScreen extends StatefulWidget {
  static const routeName = "/card_editor";
  final CreditCardEditorScreenArguments arguments;

  CreditCardEditorScreen({@required this.arguments})
      : assert(arguments != null);

  _CreditCardEditorScreenState createState() => _CreditCardEditorScreenState();
}

class _CreditCardEditorScreenState extends State<CreditCardEditorScreen>
    with ScreenLoader {
  Order _order;
  String _cardNumber = '';
  String _expiryDate = '';
  String _cardHolderName = '';
  String _cvvCode = '';
  bool _isCvvFocused = false;
  bool _saveCard = false;
  String localeKey = "order.payment.card.editor";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String editorLabels(String key) {
    return FlutterI18n.translate(context, "$localeKey.labels.$key");
  }

  String editorHints(String key) {
    return FlutterI18n.translate(context, "$localeKey.hints.$key");
  }

  String editorErrors(String key) {
    return FlutterI18n.translate(context, "$localeKey.errors.$key");
  }

  TextStyle get hintStyle {
    return Theme.of(context).textTheme.bodyText1.copyWith(
          fontSize: 16,
          color: Color(0xFF111113).withOpacity(0.6),
        );
  }

  TextStyle get labelStyle {
    return Theme.of(context).textTheme.headline1.copyWith(fontSize: 14);
  }

  InputBorder get focusedBorder {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: customBlueColor,
        width: 0.5,
      ),
      borderRadius: BorderRadius.circular(5.0),
    );
  }

  InputBorder get border {
    return OutlineInputBorder(
      borderSide: BorderSide(
        width: 0.5,
        color: Colors.black.withOpacity(0.5),
      ),
      borderRadius: BorderRadius.circular(5.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return loadableWidget(
      child: Scaffold(
        appBar: CustomWidgets.appbarWithText(
            context, FlutterI18n.translate(context, "$localeKey.title")),
        resizeToAvoidBottomInset: false,
        body: Column(
          children: <Widget>[
            CreditCardWidget(
              glassmorphismConfig: null,
              cardNumber: _cardNumber,
              expiryDate: _expiryDate,
              cardHolderName: _cardHolderName,
              cvvCode: _cvvCode,
              showBackView: _isCvvFocused,
              obscureCardNumber: true,
              obscureCardCvv: true,
              isHolderNameVisible: false,
              cardBgColor: Colors.red,
              backgroundImage: null,
              isSwipeGestureEnabled: true,
              onCreditCardWidgetChange: (CreditCardBrand creditCardBrand) {},
              customCardTypeIcons: <CustomCardTypeIcon>[
                CustomCardTypeIcon(
                  cardType: CardType.mastercard,
                  cardImage: Image.asset(
                    imageAssetBuilder("mastercard.png"),
                    height: 48,
                    width: 48,
                  ),
                ),
                CustomCardTypeIcon(
                  cardType: CardType.visa,
                  cardImage: Image.asset(
                    imageAssetBuilder("visa.png"),
                    height: 48,
                    width: 48,
                  ),
                ),
              ],
            ),
            CreditCardForm(
              formKey: _formKey,
              obscureCvv: true,
              obscureNumber: true,
              cardNumber: _cardNumber,
              cvvCode: _cvvCode,
              isHolderNameVisible: false,
              isCardNumberVisible: true,
              isExpiryDateVisible: true,
              cardHolderName: _cardHolderName,
              expiryDate: _expiryDate,
              themeColor: Colors.blue,
              textColor: Colors.black,
              cardNumberDecoration: InputDecoration(
                labelText: editorLabels("card_number"),
                hintText: editorHints("card_number"),
                hintStyle: hintStyle,
                labelStyle: labelStyle,
                errorBorder: border.copyWith(
                    borderSide: BorderSide(color: customRedColor, width: 0.5)),
                focusedBorder: focusedBorder,
                enabledBorder: border,
              ),
              expiryDateDecoration: InputDecoration(
                hintStyle: hintStyle,
                labelStyle: labelStyle,
                errorBorder: border.copyWith(
                    borderSide: BorderSide(color: customRedColor, width: 0.5)),
                focusedBorder: focusedBorder,
                enabledBorder: border,
                labelText: editorLabels("expiry"),
                hintText: editorHints("expiry"),
              ),
              cvvCodeDecoration: InputDecoration(
                hintStyle: hintStyle,
                labelStyle: labelStyle,
                errorBorder: border.copyWith(
                    borderSide: BorderSide(color: customRedColor, width: 0.5)),
                focusedBorder: focusedBorder,
                enabledBorder: border,
                labelText: editorLabels("cvc"),
                hintText: editorHints("cvc"),
              ),
              cardHolderDecoration: InputDecoration(
                hintStyle: hintStyle,
                labelStyle: labelStyle,
                errorBorder: border.copyWith(
                    borderSide: BorderSide(color: customRedColor, width: 0.5)),
                focusedBorder: focusedBorder,
                enabledBorder: border,
                labelText: editorLabels("card_holder"),
                hintText: editorHints("card_holder"),
              ),
              numberValidationMessage: editorErrors("card_number"),
              cvvValidationMessage: editorErrors("cvc"),
              dateValidationMessage: editorErrors("expiry"),
              onCreditCardModelChange: onCreditCardModelChange,
            ),
            Space.vertical(12),
            Row(
              children: [
                Space.horizontal(16),
                CircledCheckBox(
                  initialValue: _saveCard,
                  onChecked: (checked) => _saveCard = checked,
                ),
                Space.horizontal(8),
                Text(
                  FlutterI18n.translate(context, "$localeKey.save_card"),
                  style: labelStyle,
                ),
              ],
            ),
            Spacer(),
            OutlinedButton(
              onPressed: () async => this.performFuture(makePayment),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 9.0, horizontal: 75),
                child: Text(
                  FlutterI18n.translate(context, "action.continue.default"),
                ),
              ),
            ),
            Space.vertical(70),
          ],
        ),
      ),
    );
  }

  Future<void> makePayment() async {
    if (_formKey.currentState.validate()) {
      final String cryptogram = await System.cardPaymentsChannel.invokeMethod(
        "createCryptogram",
        {
          "card_number": _cardNumber,
          "card_date": _expiryDate,
          "card_cvc": _cvvCode,
          "merchant_public_id": PaymentAPI.merchantPublicId,
        },
      );
      if (cryptogram == null || cryptogram.isEmpty) {
        System.displayErrorSnackBar(context,
            errorLocaleKey: "error.card_validation_error");
        setState(() {
          _cardNumber = "";
          _expiryDate = "";
          _cardHolderName = "";
          _cvvCode = "";
          _isCvvFocused = false;
        });
      } else {
        if (_order == null) {
          try {
            _order = await ServerRequests.createOrder(
                Shop.currentUser.info.type, widget.arguments.order, context);
            widget.arguments.onOrderCreated(_order);
          } catch (e) {
            System.displayErrorSnackBar(context,
                errorLocaleKey: "error.order_registration");
            return;
          }
        }
        final paymentApi = PaymentAPI(
          userId: Shop.currentUser.info.id,
          email: Shop.currentUser.info.email,
          order: _order,
          on3dsRequired: (md, acs, paReq) async {
            return await Navigator.of(context, rootNavigator: true).pushNamed(
              ThreeDsVerificationScreen.routeName,
              arguments: ThreeDsVerificationArguments(
                  transactionId: md, paReq: paReq, acsUrl: acs),
            );
          },
          onSuccess: (cardToken, transactionId) {
            if (_saveCard) {
              widget.arguments.cardTokens.add(cardToken);
              ServerRequests.updateCardTokens(widget.arguments.cardTokens);
            }
            Navigator.of(context, rootNavigator: true)
                .popUntil((route) => route.isFirst);
            Navigator.of(context, rootNavigator: true).pushNamed(
                OrderRegisteredScreen.routeName,
                arguments: _order.payment);
            widget.arguments.onSuccess(_order, transactionId);
          },
          onFailure: (message) =>
              System.displayErrorSnackBarWithMessage(context, message),
          onServerError: () => System.displayErrorSnackBar(context,
              errorLocaleKey: "error.server"),
        );
        await paymentApi.chargeCard(cryptogram);
      }
    }
  }

  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      _cardNumber = creditCardModel.cardNumber ?? "";
      _expiryDate = creditCardModel.expiryDate ?? "";
      _cardHolderName = creditCardModel.cardHolderName ?? "";
      _cvvCode = creditCardModel.cvvCode.length > 3
          ? creditCardModel.cvvCode.substring(1, 4)
          : creditCardModel.cvvCode;
      _isCvvFocused = creditCardModel.isCvvFocused ?? "";
    });
  }
}
