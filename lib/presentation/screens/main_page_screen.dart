import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/banner.dart';
import 'package:benefit_flutter/data/models/category.dart';
import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/catalog_screen.dart';
import 'package:benefit_flutter/presentation/screens/category_screen.dart';
import 'package:benefit_flutter/presentation/widgets/carousel_slider_with_indicator.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/search_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:benefit_flutter/presentation/widgets/specification_list.dart';
import 'package:benefit_flutter/presentation/widgets/web_module_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class MainPageScreen extends StatefulWidget {
  static const routeName = "/main_page";
  @override
  _MainPageScreenState createState() => _MainPageScreenState();
}

class _MainPageScreenState extends State<MainPageScreen> {
  ScrollController _scrollController;
  Category _discountedCategory;
  Category _newArrivalCategory;
  List<Specification> _specifications;

  @override
  void initState() {
    _scrollController = ScrollController();
    _discountedCategory = Shop.discountedCategory;
    _newArrivalCategory = Shop.newArrivalCategory;
    super.initState();
  }

  void reload() {
    setState(() {
      _specifications = Shop.currentUser.specifications;
    });
  }

  Widget get _newArrivalModule {
    return WebModuleWidget(
      title: _newArrivalCategory.descriptor.name,
      products: _newArrivalCategory.products,
      onMoreButtonPressed: () => openCategory(_newArrivalCategory),
      onItemPressed: reload,
    );
  }

  Widget get _discountedModule {
    return WebModuleWidget(
      title: _discountedCategory.descriptor.name,
      products: _discountedCategory.products,
      onMoreButtonPressed: () => openCategory(_discountedCategory),
      onItemPressed: reload,
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: UniqueKey(),
      child: Scaffold(
        appBar: CustomWidgets.customAppbar(
          63,
          Center(
            child: Image.asset(
              imageAssetBuilder('appBar.png'),
            ),
          ),
        ),
        body: FutureBuilder(
            future: Shop.initializeShop(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data is Exception ||
                    snapshot.data is Error ||
                    !snapshot.data) {
                  return Center(
                    child: Text(
                      snapshot.data is Exception
                          ? snapshot.data.message
                          : FlutterI18n.translate(context, "error.no_service"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customGrayColor1),
                      textAlign: TextAlign.center,
                    ),
                  );
                }
                _specifications = Shop.currentUser.specifications;
                return SingleChildScrollView(
                  controller: _scrollController,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: SearchWidget(),
                      ),
                      Space.vertical(14),
                      CarouselSliderWithIndicator(
                        images: Shop.bannersForType(BannerType.Main),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 16),
                        child: Row(
                          children: [
                            OutlinedButton(
                              onPressed: openCatalog,
                              style: ButtonStyle(
                                fixedSize: MaterialStateProperty.all<Size>(
                                    Size(100, 100)),
                              ),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 11.0, left: 11),
                                    child: Row(
                                      children: [
                                        Text(
                                          FlutterI18n.translate(
                                              context, 'catalog.text'),
                                          style: TextStyle(fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 16.0, right: 8),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Icon(
                                          CustomIcons.option_list,
                                          size: 28,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: _discountedCategory == null
                            ? FutureBuilder(
                                future: Shop.discountedProducts,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    _specifications =
                                        Shop.currentUser.specifications;
                                    _discountedCategory = Category(
                                      descriptor: CategoryDescriptor(
                                        id: 1,
                                        name: FlutterI18n.translate(context,
                                            "product.category.discounted"),
                                        productCount: snapshot.data.length,
                                      ),
                                      products: snapshot.data,
                                    );
                                    Shop.discountedCategory =
                                        _discountedCategory;
                                    return _discountedModule;
                                  } else if (snapshot.hasError) {
                                    return Center(
                                      child: Text(
                                        //FlutterI18n.translate(context, "error.no_service"),
                                        snapshot.error.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .copyWith(color: customGrayColor1),
                                        textAlign: TextAlign.center,
                                      ),
                                    );
                                  } else {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                },
                              )
                            : _discountedModule,
                      ),
                      Space.vertical(21),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: SpecificationList(
                          specifications: _specifications,
                          onSpecificationPressed: reload,
                          onSpecificationAdded:
                              Shop.currentUser.addSpecification,
                        ),
                      ),
                      Space.vertical(15),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: _newArrivalCategory == null
                            ? FutureBuilder(
                                future: Shop.newArrivalProducts,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    _newArrivalCategory = Category(
                                      descriptor: CategoryDescriptor(
                                        id: 2,
                                        name: FlutterI18n.translate(context,
                                            "product.category.newArrivals"),
                                        productCount: snapshot.data.length,
                                      ),
                                      products: snapshot.data,
                                    );
                                    Shop.newArrivalCategory =
                                        _newArrivalCategory;
                                    return _newArrivalModule;
                                  } else if (snapshot.hasError) {
                                    return Center(
                                      child: Text(
                                        //FlutterI18n.translate( context, "error.no_service"),
                                        snapshot.error.toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5
                                            .copyWith(color: customGrayColor1),
                                        textAlign: TextAlign.center,
                                      ),
                                    );
                                  } else {
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                },
                              )
                            : _newArrivalModule,
                      ),
                      Space.vertical(10),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, bottom: 11),
                        child: Text(
                          FlutterI18n.translate(
                              context, 'product.delivery.about'),
                          style: Theme.of(context).textTheme.headline1,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      CarouselSliderWithIndicator(
                        images: Shop.bannersForType(BannerType.Delivery),
                      ),
                    ],
                  ),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }

  void openCatalog() {
    Navigator.pushNamed(context, CatalogScreen.routeName).then((_) => reload());
  }

  void openCategory(Category category) {
    Navigator.pushNamed(context, CategoryScreen.routeName, arguments: category)
        .then((_) => reload());
  }
}
