import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/form_fields.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class AddressEditorScreenParameters {
  final Function(Address) onCreateButtonPressed;
  final Address initialAddress;

  AddressEditorScreenParameters({
    Function(Address) onCreateButtonPressed,
    this.initialAddress,
  }) : this.onCreateButtonPressed = onCreateButtonPressed ?? ((_) => null);
}

class AddressEditorScreen extends StatefulWidget {
  static const routeName = "/address_editor";
  final AddressEditorScreenParameters params;

  AddressEditorScreen({
    Key key,
    @required this.params,
  })  : assert(params != null),
        super(key: key);

  @override
  _AddressEditorScreenState createState() => _AddressEditorScreenState();
}

class _AddressEditorScreenState extends State<AddressEditorScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _flatController = TextEditingController();
  final TextEditingController _streetController = TextEditingController();
  final TextEditingController _levelController = TextEditingController();
  final TextEditingController _houseController = TextEditingController();
  final TextEditingController _infoController = TextEditingController();

  @override
  void initState() {
    if (widget.params.initialAddress != null) {
      _cityController.text = widget.params.initialAddress.city;
      _streetController.text = widget.params.initialAddress.street;
      _houseController.text = widget.params.initialAddress.house;
      _levelController.text = widget.params.initialAddress.level;
      _flatController.text = widget.params.initialAddress.flat;
      _infoController.text = widget.params.initialAddress.extraInfo;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: CustomWidgets.appbarWithText(
          context, FlutterI18n.translate(context, "action.add.address")),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Space.vertical(26),
            Flexible(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 27),
                child: ListView(
                  children: [
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.address.editor.city"),
                      _cityController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.address.editor.street"),
                      _streetController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.address.editor.house"),
                      _houseController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.address.editor.level"),
                      _levelController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.address.editor.flat"),
                      _flatController,
                      validator: (text) =>
                          FormFields.compulsoryFieldValidator(context, text),
                    ),
                    Space.vertical(8),
                    FormFields.compulsoryTextField(
                      context,
                      FlutterI18n.translate(
                          context, "user.address.editor.extra_info"),
                      _infoController,
                      maxLength: 35,
                    ),
                  ],
                ),
              ),
            ),
            OutlinedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  widget.params.onCreateButtonPressed(
                    Address(
                      city: _cityController.text,
                      flat: _flatController.text,
                      street: _streetController.text,
                      level: _levelController.text,
                      house: _houseController.text,
                      extraInfo: _infoController.text,
                    ),
                  );
                  Navigator.of(context).pop();
                }
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 9.0, horizontal: 75),
                child: Text(
                  FlutterI18n.translate(context, "action.continue.default"),
                ),
              ),
            ),
            Space.vertical(32),
          ],
        ),
      ),
    );
  }
}
