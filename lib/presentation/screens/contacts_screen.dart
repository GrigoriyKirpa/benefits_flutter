import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ContactsScreen extends StatelessWidget {
  static String routeName = "/contacts";

  TextStyle text(BuildContext context) {
    return Theme.of(context).textTheme.bodyText1;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomWidgets.appbarWithText(context,
          FlutterI18n.translate(context, "info_screens.contacts.title")),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 34, vertical: 8),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                FlutterI18n.translate(context, "info_screens.contacts.address"),
                style: text(context),
              ),
              Space.vertical(12),
              ...List.generate(
                Shop.departments.length,
                (departmentIndex) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        FlutterI18n.translate(context,
                            "info_screens.contacts.department.${departmentIndex.toString()}"),
                        style: text(context),
                      ),
                      ...List.generate(
                        Shop.departments[departmentIndex].phone.length,
                        (phoneIndex) => GestureDetector(
                          onTap: () => System.call(Shop
                              .departments[departmentIndex].phone[phoneIndex]),
                          child: Text(
                            Shop.departments[departmentIndex].phone[phoneIndex],
                            style:
                                text(context).copyWith(color: customBlueColor),
                          ),
                        ),
                      ),
                      ...List.generate(
                        Shop.departments[departmentIndex].email.length,
                        (emailIndex) => GestureDetector(
                          onTap: () => System.email(Shop
                              .departments[departmentIndex].email[emailIndex]),
                          child: Text(
                            Shop.departments[departmentIndex].email[emailIndex],
                            style:
                                text(context).copyWith(color: customBlueColor),
                          ),
                        ),
                      ),
                      Space.vertical(12),
                    ],
                  );
                },
              ),
              Space.vertical(12),
              Text(
                FlutterI18n.translate(
                    context, "info_screens.contacts.work_time.office"),
                style: text(context),
              ),
              Space.vertical(12),
              Text(
                FlutterI18n.translate(
                    context, "info_screens.contacts.work_time.storage"),
                style: text(context),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
