import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/utils/system.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

mixin BasketNotifier {
  void displayAddedToBasket(BuildContext context) {
    _displayBasketMessage(context, "product_added", customBlueColor);
  }

  void displayNotAddedToBasket(BuildContext context) {
    _displayBasketMessage(context, "product_not_added", customRedColor);
  }

  void _displayBasketMessage(
      BuildContext context, String message, Color color) {
    final snackBar = CustomWidgets.snackBar(
      color,
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            FlutterI18n.translate(context, "basket.$message"),
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(fontSize: 20, color: Colors.white),
          ),
          Icon(
            CustomIcons.basket,
            size: 28,
            color: Colors.white,
          ),
          Text(
            " !",
            style: Theme.of(context)
                .textTheme
                .bodyText1
                .copyWith(fontSize: 20, color: Colors.white),
          ),
        ],
      ),
    );

    System.displaySnackBar(context, snackBar);
  }
}
