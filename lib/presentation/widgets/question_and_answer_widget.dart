import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/question_and_answer.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class QuestionAndAnswerWidget extends StatelessWidget {
  final QuestionAndAnswer questionAndAnswer;

  QuestionAndAnswerWidget({Key key, @required this.questionAndAnswer})
      : assert(questionAndAnswer != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 1,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: BorderSide(
          width: 0.5,
          color: customGrayColor1,
        ),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              questionAndAnswer.question.sender.info.fullName,
              style:
                  Theme.of(context).textTheme.headline1.copyWith(fontSize: 12),
            ),
            Space.vertical(2),
            Text(
              DateFormat("dd/MM/yyyy").format(questionAndAnswer.question.date),
              style: Theme.of(context).textTheme.headline5.copyWith(
                    fontSize: 9,
                    color: customGrayColor1,
                  ),
            ),
            Space.vertical(3),
            Text(
              questionAndAnswer.question.text,
              style: Theme.of(context).textTheme.headline5.copyWith(
                    fontSize: 10,
                  ),
            ),
            if (questionAndAnswer.isNotClosed) ...[
              Space.vertical(12),
              Text(
                questionAndAnswer.answer.sender.info.fullName,
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 12, color: customBlueColor),
              ),
              Space.vertical(2),
              Text(
                DateFormat("dd/MM/yyyy").format(questionAndAnswer.answer.date),
                style: Theme.of(context).textTheme.headline5.copyWith(
                      fontSize: 9,
                      color: customGrayColor1,
                    ),
              ),
              Space.vertical(3),
              Text(
                questionAndAnswer.answer.text,
                style: Theme.of(context).textTheme.headline5.copyWith(
                      fontSize: 10,
                    ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
