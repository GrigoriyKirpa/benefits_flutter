import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/mixins/basket_notifier.dart';
import 'package:benefit_flutter/presentation/screens/product_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/double_stated_buttons.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProductMiniWidget extends StatefulWidget {
  final ProductDescriptor product;
  final VoidCallback onProductPressed;

  ProductMiniWidget(
      {Key key, @required this.product, VoidCallback onProductPressed})
      : this.onProductPressed = onProductPressed ?? (() => {}),
        assert(product != null),
        super(key: key);

  @override
  _ProductMiniWidgetState createState() => _ProductMiniWidgetState();
}

class _ProductMiniWidgetState extends State<ProductMiniWidget>
    with BasketNotifier {
  bool _isLiked = false;

  @override
  void initState() {
    _isLiked = Shop.currentUser.isLiked(widget.product.id);
    super.initState();
  }

  void reloadLikeButton() {
    if (mounted) {
      setState(() {
        _isLiked = Shop.currentUser.isLiked(widget.product.id);
      });
    }
  }

  @override
  void displayAddedToBasket(BuildContext context) {
    if (!mounted) {
      return;
    }
    super.displayAddedToBasket(context);
  }

  @override
  void displayNotAddedToBasket(BuildContext context) {
    if (!mounted) {
      return;
    }
    super.displayNotAddedToBasket(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: miniProductWidgetWidth,
      child: RawMaterialButton(
        onPressed: () => Navigator.pushNamed(context, ProductScreen.routeName,
                arguments: widget.product)
            .then(
          (value) {
            reloadLikeButton();
            widget.onProductPressed();
          },
        ),
        child: Card(
          child: Padding(
            padding:
                const EdgeInsets.only(top: 6, bottom: 7, left: 3, right: 3),
            child: Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Space.filler(),
                    SizedBox(
                      height: 60,
                      width: 60,
                      child: widget.product.image != null
                          ? CachedNetworkImage(
                              imageUrl: widget.product.image,
                              fit: BoxFit.cover,
                              placeholder: (context, url) =>
                                  Center(child: CircularProgressIndicator()),
                            )
                          : Center(
                              child: Icon(
                                Icons.image_not_supported,
                                color: Colors.black,
                                size: 24,
                              ),
                            ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: DoubleStateButtons.likeButton(
                        size: 10,
                        isInitiallyActive: _isLiked,
                        handleTap: (_) =>
                            Shop.currentUser.handleLike(widget.product).then(
                                  (success) =>
                                      success ? null : reloadLikeButton(),
                                ),
                      ),
                    ),
                  ],
                ),
                Text(
                  widget.product.name,
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .copyWith(fontSize: 10, fontWeight: FontWeight.w500),
                ),
                Space.vertical(2),
                CustomWidgets.pricePerPieceWidget(
                  context,
                  widget.product.price.price,
                  Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontSize: 10, color: Color(0xFF333333)),
                  align: TextAlign.center,
                ),
                Space.vertical(6),
                InkWell(
                  onTap: () async {
                    if (await Shop.basket.add(
                      OrderItem(
                        product: widget.product,
                        price: widget.product.price,
                        amount: widget.product.minAmount,
                      ),
                    ))
                      displayAddedToBasket(context);
                    else
                      displayNotAddedToBasket(context);

                    reloadLikeButton();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.add_circle_outline_rounded,
                        color: customTextGrayColor1,
                        size: 17,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
