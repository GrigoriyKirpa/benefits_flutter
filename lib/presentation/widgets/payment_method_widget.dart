import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/payment_method.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';

class PaymentMethodWidget extends StatelessWidget {
  final PaymentMethod method;
  final VoidCallback onSelected;
  final bool active;

  PaymentMethodWidget(
      {Key key,
      @required this.method,
      this.active = false,
      VoidCallback onSelected})
      : this.onSelected = onSelected ?? (() => null),
        assert(method != null),
        assert(active != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onSelected,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 14.0, horizontal: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Stack(
                children: [
                  Container(
                    width: 32,
                    height: 32,
                    decoration: BoxDecoration(
                      color: method.color,
                      shape: BoxShape.circle,
                    ),
                  ),
                  Positioned(
                    top: 8,
                    left: 8,
                    child: Icon(
                      method.icon,
                      color: Colors.white,
                      size: 16,
                    ),
                  ),
                ],
              ),
              Space.horizontal(14),
              Flexible(
                fit: FlexFit.tight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      method.title(context),
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 14),
                    ),
                    Space.vertical(3),
                    Text(
                      method.description(context),
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 10, color: customGrayColor1),
                    ),
                  ],
                ),
              ),
              Space.horizontal(14),
              Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: active
                      ? null
                      : Border.all(
                          width: 2,
                          color: customGrayColor1,
                        ),
                  color: active ? customBlueColor : Colors.transparent,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
