import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class BasketListItem extends StatefulWidget {
  final OrderItem item;
  final VoidCallback onAmountChanged;
  final VoidCallback onItemDeleted;

  BasketListItem({
    Key key,
    @required this.item,
    VoidCallback onAmountChanged,
    VoidCallback onItemDeleted,
  })  : this.onAmountChanged = onAmountChanged ?? (() => null),
        this.onItemDeleted = onItemDeleted ?? (() => null),
        assert(item != null),
        super(key: key);

  @override
  _BasketListItemState createState() => _BasketListItemState();
}

class _BasketListItemState extends State<BasketListItem> {
  int _amount;
  double _total;

  @override
  void initState() {
    super.initState();
    _amount = widget.item.amount;
    _total = widget.item.totalPrice;
  }

  void reload() {
    if (mounted) {
      setState(() {
        _amount = widget.item.amount;
        _total = widget.item.totalPrice;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 105,
          width: 80,
          child: widget.item.product.image != null &&
                  widget.item.product.image.isNotEmpty
              ? CachedNetworkImage(
                  imageUrl: widget.item.product.image,
                  fit: BoxFit.cover,
                  placeholder: (context, url) =>
                      Center(child: CircularProgressIndicator()),
                )
              : Container(
                  color: customGrayColor1,
                  child: Icon(
                    Icons.image_not_supported,
                    color: Colors.black,
                    size: 64,
                  ),
                ),
        ),
        Space.horizontal(15),
        Flexible(
          flex: 15,
          fit: FlexFit.tight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text(
                widget.item.product.name,
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 14),
              ),
              Space.vertical(1),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(
                          context, "order.item.manufacturer"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                    TextSpan(
                      text: " ${widget.item.product.manufacturer ?? ""}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                  ],
                ),
              ),
              Space.vertical(2),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(
                          context, "order.item.set_number"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                    TextSpan(
                      text: " ${widget.item.product.setNumber ?? ""}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                  ],
                ),
              ),
              Space.vertical(7),
              CustomWidgets.pricePerPieceRichText(
                context,
                widget.item.price.price,
                Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: customBlueColor, fontSize: 14),
                Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: customBlueColor, fontSize: 12),
                leading: TextSpan(
                  text:
                      "${FlutterI18n.translate(context, "order.item.price")} ",
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(color: customTextGrayColor1, fontSize: 9),
                ),
              ),
              if (widget.item.selectedColor != null) ...[
                Space.vertical(4),
                Row(
                  children: [
                    Text(
                      "${FlutterI18n.translate(context, "order.item.color")} ",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 9, color: customTextGrayColor1),
                    ),
                    Container(
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: Shop.getColor(widget.item.selectedColor),
                          fit: BoxFit.fill,
                        ),
                      ),
                      width: 15,
                      height: 15,
                    ),
                  ],
                ),
              ],
              if (widget.item.selectedSize != null) ...[
                Space.vertical(4),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: FlutterI18n.translate(context, "order.item.size"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: customTextGrayColor1, fontSize: 9),
                      ),
                      TextSpan(
                        text: " ${widget.item.selectedSize}",
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ],
            ],
          ),
        ),
        Flexible(
          flex: 10,
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Space.vertical(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        if (_amount > 0)
                          widget.item.amount =
                              _amount - widget.item.product.minAmount;

                        if (widget.item.amount <= 0)
                          widget.onItemDeleted();
                        else
                          widget.onAmountChanged();
                        reload();
                      },
                      customBorder: CircleBorder(),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.remove,
                          size: 18,
                        ),
                      ),
                    ),
                    Text(
                      _amount.toString(),
                      style: Theme.of(context)
                          .textTheme
                          .headline1
                          .copyWith(fontSize: 14),
                    ),
                    InkWell(
                      onTap: () {
                        if (_amount <
                            defaultStock * widget.item.product.minAmount)
                          widget.item.amount =
                              _amount + widget.item.product.minAmount;
                        reload();
                        widget.onAmountChanged();
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.add,
                          size: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                CustomWidgets.totalPriceRichText(
                  context,
                  _total,
                  Theme.of(context).textTheme.headline5.copyWith(fontSize: 14),
                  leading: TextSpan(
                    text:
                        "${FlutterI18n.translate(context, "order.item.total")} ",
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(color: customTextGrayColor1, fontSize: 9),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
