import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ReviewWidget extends StatelessWidget {
  final Review review;

  ReviewWidget({Key key, @required this.review})
      : assert(review != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 1,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
        side: BorderSide(
          width: 0.5,
          color: customGrayColor1,
        ),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  review.name,
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(fontSize: 12),
                ),
              ],
            ),
            Space.vertical(2),
            Text(
              DateFormat("dd/MM/yyyy").format(review.date),
              style: Theme.of(context).textTheme.headline5.copyWith(
                    fontSize: 9,
                    color: customGrayColor1,
                  ),
            ),
            Space.vertical(3),
            if (review.text.isNotEmpty)
              Text(
                review.text,
                style: Theme.of(context).textTheme.headline5.copyWith(
                      fontSize: 10,
                    ),
              ),
          ],
        ),
      ),
    );
  }
}
