import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/search_screen.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SearchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pushNamed(SearchScreen.routeName),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 26,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(width: 0.3, color: customTextGrayColor2),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              CustomIcons.search,
              size: 12,
              color: disabledColor,
            ),
            Space.horizontal(5),
            Text(
              FlutterI18n.translate(context, "search.searchHintText"),
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: disabledColor, fontSize: 12),
            ),
          ],
        ),
      ),
    );
  }
}
