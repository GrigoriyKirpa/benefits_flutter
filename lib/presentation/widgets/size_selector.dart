import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';

class SizeSelector extends StatefulWidget {
  final List<String> sizes;
  final int initialSizeIndex;
  final Function(String) onSizeSelected;

  SizeSelector({
    Key key,
    @required this.sizes,
    this.initialSizeIndex = 0,
    Function(String) onSizeSelected,
  })  : this.onSizeSelected = onSizeSelected ?? ((_) => {}),
        assert(sizes != null && sizes.isNotEmpty),
        assert(sizes.contains(sizes[initialSizeIndex])),
        super(key: key);

  @override
  _SizeSelectorState createState() => _SizeSelectorState();
}

class _SizeSelectorState extends State<SizeSelector> {
  int _selectedSizerIndex;

  bool isSelected(int index) {
    return index == _selectedSizerIndex;
  }

  void handleSelection(String size) {
    setState(() {
      _selectedSizerIndex = widget.sizes.indexOf(size);
    });
    widget.onSizeSelected(size);
  }

  @override
  void initState() {
    _selectedSizerIndex = widget.initialSizeIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      runSpacing: 9,
      spacing: 14,
      children: widget.sizes.map((size) {
        return GestureDetector(
          onTap: () => handleSelection(size),
          child: new Text(
            size.toString(),
            style: Theme.of(context).textTheme.headline1.copyWith(
                  color: isSelected(widget.sizes.indexOf(size))
                      ? Colors.black
                      : customGrayColor1,
                ),
          ),
        );
      }).toList(),
    );
  }
}
