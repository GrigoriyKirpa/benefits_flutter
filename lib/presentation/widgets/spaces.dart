import 'package:flutter/material.dart';

class Space {
  Space._();

  static Widget vertical(double size) {
    return SizedBox.fromSize(size: Size(1, size));
  }

  static Widget horizontal(double size) {
    return SizedBox.fromSize(size: Size(size, 1));
  }

  static Widget filler([Size size]) {
    size = size ?? Size(1, 1);
    return Flexible(
      fit: FlexFit.tight,
      child: SizedBox.fromSize(
        size: size,
      ),
    );
  }

  static Widget whiteSpace() {
    return SizedBox.fromSize(size: Size(1, 1));
  }
}
