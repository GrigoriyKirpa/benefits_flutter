import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/product_feedback.dart';
import 'package:benefit_flutter/data/models/question_and_answer.dart';
import 'package:benefit_flutter/data/models/rating.dart';
import 'package:benefit_flutter/data/models/review.dart';
import 'package:benefit_flutter/presentation/screens/question_and_answer_screen.dart';
import 'package:benefit_flutter/presentation/screens/review_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/review_screen.dart';
import 'package:benefit_flutter/presentation/widgets/question_and_answer_widget.dart';
import 'package:benefit_flutter/presentation/widgets/review_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

enum FeedbackWidgetTabs {
  Rating,
  QuestionAndAnswer,
}

extension FeedbackWidgetTabsExtension on FeedbackWidgetTabs {
  String _localeKey() {
    String _localeKey;
    if (this == FeedbackWidgetTabs.Rating) {
      _localeKey = "review";
    } else if (this == FeedbackWidgetTabs.QuestionAndAnswer) {
      _localeKey = "question_and_answer";
    }
    return _localeKey;
  }

  String tabName(BuildContext context) {
    return FlutterI18n.translate(
        context, "product.page.feedback.tab_name.${_localeKey()}");
  }

  String buttonText(BuildContext context) {
    return FlutterI18n.translate(context, "action.leave.${_localeKey()}");
  }
}

class FeedbackWidget extends StatefulWidget {
  final ProductFeedback feedback;
  final Rating rating;
  final bool rated;
  final Function(int) onRatingAdded;
  final Function(Review) onReviewAdded;
  final Function(QuestionAndAnswer) onQuestionAndAnswerAdded;

  FeedbackWidget(
      {Key key,
      @required this.rating,
      @required this.feedback,
      this.rated = false,
      Function(int) onRatingAdded,
      Function(Review) onReviewAdded,
      Function(QuestionAndAnswer) onQuestionAndAnswerAdded})
      : this.onReviewAdded = onReviewAdded ?? ((_) => {}),
        this.onRatingAdded = onRatingAdded ?? ((_) => {}),
        this.onQuestionAndAnswerAdded = onQuestionAndAnswerAdded ?? ((_) => {}),
        assert(feedback != null),
        assert(rating != null),
        super(key: key);

  @override
  _FeedbackWidgetState createState() => _FeedbackWidgetState();
}

class _FeedbackWidgetState extends State<FeedbackWidget> {
  FeedbackWidgetTabs _currentTab = FeedbackWidgetTabs.values.first;
  double _stars = 0.0;

  @override
  void initState() {
    super.initState();
  }

  String label(FeedbackWidgetTabs tab) {
    String number;
    if (tab == FeedbackWidgetTabs.Rating) {
      number = widget.feedback.reviews.length.toString();
    } else if (tab == FeedbackWidgetTabs.QuestionAndAnswer) {
      number = widget.feedback.questionsAndAnswers.length.toString();
    } else {
      throw UnimplementedError();
    }
    return "${tab.tabName(context)} ($number)";
  }

  List<Widget> get example {
    if (_currentTab == FeedbackWidgetTabs.Rating) {
      if (widget.feedback.reviews.length > 0) {
        return [
          Space.vertical(20),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, ReviewScreen.routeName,
                arguments: widget.feedback.reviews),
            child: ReviewWidget(
              review: widget.feedback.reviews.last,
            ),
          ),
        ];
      } else {
        return [Space.vertical(1)];
      }
    } else if (_currentTab == FeedbackWidgetTabs.QuestionAndAnswer) {
      if (widget.feedback.questionsAndAnswers.length > 0) {
        return [
          Space.vertical(20),
          GestureDetector(
            onTap: () => Navigator.pushNamed(
                context, QuestionAndAnswerScreen.routeName,
                arguments: widget.feedback.questionsAndAnswers),
            child: QuestionAndAnswerWidget(
              questionAndAnswer: widget.feedback.questionsAndAnswers.last,
            ),
          ),
        ];
      } else {
        return [Space.vertical(1)];
      }
    } else {
      throw UnimplementedError();
    }
  }

  Function get onPressed {
    if (_currentTab == FeedbackWidgetTabs.Rating) {
      return () => Navigator.pushNamed(
            context,
            ReviewEditorScreen.routeName,
            arguments: (review) => widget.onReviewAdded(review),
          );
    } else if (_currentTab == FeedbackWidgetTabs.QuestionAndAnswer) {
      return null;
      // return () => Navigator.pushNamed(
      //       context,
      //       QuestionAndAnswerEditor.routeName,
      //       arguments: (message) => widget.onAddQuestionAndAnswerAdded(
      //         QuestionAndAnswer(
      //           question: message,
      //         ),
      //       ),
      //     );
    } else {
      throw UnimplementedError();
    }
  }

  void openRateDialog() {
    final Widget alert = AlertDialog(
      contentPadding: const EdgeInsets.all(14),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () => Navigator.of(context, rootNavigator: true).pop(),
                child: Icon(
                  Icons.close_rounded,
                  size: 16,
                  color: customGrayColor1,
                ),
              ),
            ],
          ),
          Text(
            FlutterI18n.translate(context, "review_screen.input_stars"),
            style: Theme.of(context).textTheme.headline5,
          ),
          Space.vertical(8),
          RatingBar.builder(
            itemPadding: EdgeInsets.only(right: 10),
            itemBuilder: (context, index) {
              return Icon(
                Icons.star,
                color: starColor,
              );
            },
            itemSize: 30,
            allowHalfRating: false,
            onRatingUpdate: (stars) => _stars = stars,
            maxRating: 5,
            minRating: 0,
            unratedColor: customGrayColor1,
            initialRating: _stars,
          ),
          Space.vertical(8),
          Center(
            child: OutlinedButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
                widget.onRatingAdded(_stars.toInt());
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 36, vertical: 8),
                child: Text(
                  FlutterI18n.translate(context, "action.send.default"),
                ),
              ),
            ),
          ),
        ],
      ),
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // ToggleSwitch(
        //   onToggle: (selectedSwitch) {
        //     setState(() {
        //       _currentTab = FeedbackWidgetTabs.values[selectedSwitch];
        //     });
        //   },
        //   totalSwitches: FeedbackWidgetTabs.values.length,
        //   labels: FeedbackWidgetTabs.values.map((e) => label(e)).toList(),
        //   initialLabelIndex: _currentTab.index,
        //   borderWidth: 2,
        //   customTextStyles: [
        //     Theme.of(context).textTheme.headline5.copyWith(fontSize: 12),
        //     Theme.of(context).textTheme.headline5.copyWith(fontSize: 12)
        //   ],
        //   inactiveBgColor: customGrayColor2,
        //   inactiveFgColor: customTextGrayColor1,
        //   activeFgColor: Colors.black,
        //   borderColor: [customGrayColor2],
        //   activeBgColor: [Colors.white],
        //   cornerRadius: 5,
        //   minWidth: MediaQuery.of(context).size.width * 0.5,
        //   minHeight: 28,
        //   radiusStyle: true,
        // ),
        // Space.vertical(12),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 22),
          height: 50,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 0.5, color: customGrayColor1),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    FlutterI18n.translate(
                        context, "product.page.feedback.rating"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 9, color: customTextGrayColor1),
                  ),
                  Space.vertical(2),
                  RatingBar.builder(
                    itemPadding: EdgeInsets.only(right: 4),
                    itemBuilder: (context, _) {
                      return Icon(
                        Icons.star,
                        color: starColor,
                      );
                    },
                    itemSize: 15,
                    allowHalfRating: true,
                    onRatingUpdate: (_) {},
                    ignoreGestures: true,
                    maxRating: 5,
                    minRating: 0,
                    unratedColor: customGrayColor1,
                    initialRating: widget.rating.rating,
                  ),
                ],
              ),
              Spacer(),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: widget.rating.ratingString,
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 36),
                    ),
                    TextSpan(
                      text: "/5",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 24, color: customTextGrayColor1),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Space.vertical(16),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 28,
          child: OutlinedButton(
            onPressed: onPressed,
            child: Text(
              _currentTab.buttonText(context),
            ),
          ),
        ),

        if (!widget.rated) ...[
          Space.vertical(9),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 28,
            child: OutlinedButton(
              style: Theme.of(context).outlinedButtonTheme.style.copyWith(
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) {
                    if (states.contains(MaterialState.pressed) ||
                        states.contains(MaterialState.hovered) ||
                        states.contains(MaterialState.selected) ||
                        states.contains(MaterialState.focused))
                      return starColor.withOpacity(0.5);
                    return starColor; // Use the component's default.
                  },
                ),
              ),
              onPressed: openRateDialog,
              child: Text(
                FlutterI18n.translate(context, "review_screen.input_stars"),
              ),
            ),
          ),
        ],
        ...example,
      ],
    );
  }
}
