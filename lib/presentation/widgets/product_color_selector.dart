import 'package:benefit_flutter/data/shop.dart';
import 'package:flutter/material.dart';

class ProductColorSelector extends StatefulWidget {
  final List<String> colors;
  final int initialColorIndex;
  final Function(String) onColorSelected;

  ProductColorSelector({
    Key key,
    @required this.colors,
    this.initialColorIndex = 0,
    Function(String) onColorSelected,
  })  : this.onColorSelected = onColorSelected ?? ((_) => {}),
        assert(colors != null && colors.isNotEmpty),
        assert(colors.contains(colors[initialColorIndex])),
        super(key: key);

  @override
  _ProductColorSelectorState createState() => _ProductColorSelectorState();
}

class _ProductColorSelectorState extends State<ProductColorSelector> {
  int _selectedColorIndex;

  bool isSelected(int index) {
    return index == _selectedColorIndex;
  }

  void handleSelection(String color) {
    setState(() {
      _selectedColorIndex = widget.colors.indexOf(color);
    });
    widget.onColorSelected(color);
  }

  @override
  void initState() {
    _selectedColorIndex = widget.initialColorIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      runSpacing: 14,
      spacing: 14,
      children: widget.colors.map((color) {
        return GestureDetector(
          onTap: () => handleSelection(color),
          child: Container(
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: Shop.getColor(widget.colors[_selectedColorIndex]),
                fit: BoxFit.fill,
              ),
              border: isSelected(widget.colors.indexOf(color))
                  ? Border.all(color: Colors.black)
                  : null,
            ),
            width: 25,
            height: 25,
          ),
        );
      }).toList(),
    );
  }
}
