import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/widgets/product_mini_widget.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';

class WebModuleWidget extends StatelessWidget {
  final List<ProductDescriptor> products;
  final String title;
  final VoidCallback onMoreButtonPressed;
  final VoidCallback onItemPressed;

  WebModuleWidget({
    Key key,
    @required this.title,
    List<ProductDescriptor> products,
    this.onMoreButtonPressed,
    VoidCallback onItemPressed,
  })  : this.products = products ?? [],
        this.onItemPressed = onItemPressed ?? (() => {}),
        assert(title != null && title.isNotEmpty),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return products.length == 0
        ? Space.whiteSpace()
        : Column(
            children: [
              Row(
                children: [
                  Text(
                    title,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Spacer(),
                  if (onMoreButtonPressed != null)
                    IconButton(
                      onPressed: onMoreButtonPressed,
                      icon: Icon(
                        CustomIcons.right_arrow,
                        color: customTextGrayColor1,
                      ),
                    ),
                ],
              ),
              Space.vertical(12),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: miniProductWidgetHeight,
                child: ListView.builder(
                  itemCount: products.length,
                  itemBuilder: (context, index) {
                    return ProductMiniWidget(
                      product: products[index],
                      onProductPressed: onItemPressed,
                    );
                  },
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.only(right: 6.5),
                ),
              )
            ],
          );
  }
}
