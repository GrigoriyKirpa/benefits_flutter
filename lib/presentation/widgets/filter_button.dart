import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/filters.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:flutter/material.dart';

class FilterButton extends StatelessWidget {
  final Function() onFilterChanged;

  FilterButton({
    Key key,
    Function() onFilterChanged,
  }) : this.onFilterChanged = onFilterChanged ?? (() => null), super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () => null,
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              Filters.label(context),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: lightBlack),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 6),
              child: Icon(
                CustomIcons.list,
                size: 24,
                color: disabledColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
