import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:flutter/material.dart';

class CategoryListItem extends StatelessWidget {
  final CategoryDescriptor category;
  final VoidCallback onPressed;
  final Color backgroundColor;
  final Color textColor;

  CategoryListItem({
    Key key,
    @required this.category,
    this.onPressed,
    this.backgroundColor = Colors.white,
    this.textColor = Colors.black,
  })  : assert(category != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: RawMaterialButton(
        onPressed: onPressed,
        child: Card(
          color: backgroundColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: (category.hasImage ? 10 : 20), horizontal: 18),
            child: Row(
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  child: Text(
                    category.name,
                    style: Theme.of(context).textTheme.headline5.copyWith(
                          fontSize: 18,
                          color: textColor,
                        ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                // if (category.hasImage)
                //   CachedNetworkImage(
                //     imageUrl: category.image,
                //     fit: BoxFit.cover,
                //     placeholder: (context, url) =>
                //         Center(child: CircularProgressIndicator()),
                //   )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
