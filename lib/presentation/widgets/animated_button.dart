import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';

class AnimatedButton extends StatefulWidget {
  final VoidCallback onPressed;
  final Widget child;
  final Color primaryColor;
  final Color activatedColor;

  AnimatedButton({
    Key key,
    @required this.child,
    this.primaryColor = customGrayColor1,
    this.activatedColor = customBlueColor,
    this.onPressed,
  })  : assert(child != null),
        super(key: key);

  @override
  _AnimatedButtonState createState() => _AnimatedButtonState();
}

class _AnimatedButtonState extends State<AnimatedButton> {
  bool _isActive = false;
  VoidCallback _callback;

  @override
  void initState() {
    if (widget.onPressed != null) {
      _callback = () {
        setState(() {
          _isActive = true;
        });
        widget.onPressed();
      };
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (_callback != null &&
                (states.contains(MaterialState.pressed) ||
                    states.contains(MaterialState.hovered) ||
                    states.contains(MaterialState.selected) ||
                    states.contains(MaterialState.focused)))
              return widget.activatedColor;
            return _isActive ? widget.activatedColor : widget.primaryColor;
          },
        ),
      ),
      onPressed: _callback,
      child: widget.child,
    );
  }
}
