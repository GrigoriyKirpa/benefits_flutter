import 'package:flutter/material.dart';

class ColorSelector extends StatefulWidget {
  final List<Color> colors;
  final int initialColorIndex;
  final Function(Color) onColorSelected;

  ColorSelector({
    Key key,
    @required this.colors,
    this.initialColorIndex = 0,
    Function(Color) onColorSelected,
  })  : this.onColorSelected = onColorSelected ?? ((_) => {}),
        assert(colors != null && colors.isNotEmpty),
        assert(colors.contains(colors[initialColorIndex])),
        super(key: key);

  @override
  _ColorSelectorState createState() => _ColorSelectorState();
}

class _ColorSelectorState extends State<ColorSelector> {
  int _selectedColorIndex;

  bool isSelected(int index) {
    return index == _selectedColorIndex;
  }

  void handleSelection(Color color) {
    setState(() {
      _selectedColorIndex = widget.colors.indexOf(color);
    });
    widget.onColorSelected(color);
  }

  @override
  void initState() {
    _selectedColorIndex = widget.initialColorIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      runSpacing: 14,
      spacing: 14,
      children: widget.colors.map((color) {
        return GestureDetector(
          onTap: () => handleSelection(color),
          child: Container(
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              color: color,
              border: isSelected(widget.colors.indexOf(color))
                  ? Border.all(color: Colors.black)
                  : null,
            ),
            width: 25,
            height: 25,
          ),
        );
      }).toList(),
    );
  }
}
