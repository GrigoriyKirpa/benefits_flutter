import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/sort_by.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:flutter/material.dart';

class SortButton extends StatelessWidget {
  final Function(SortBy) onSortOptionChanged;
  final SortBy sortBy;

  SortButton({
    Key key,
    Function(SortBy) onSortOptionChanged,
    @required this.sortBy,
  })  : this.onSortOptionChanged = onSortOptionChanged ?? ((_) => null),
        assert(sortBy != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () => openSortBy(context),
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              sortBy.label(context),
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: lightBlack),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: Icon(
                CustomIcons.double_arrows,
                size: 24,
                color: disabledColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> openSortBy(BuildContext context) async {
    final Widget alert = AlertDialog(
      contentPadding: const EdgeInsets.all(8),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: SortBy.values.map((option) {
          return RawMaterialButton(
            onPressed: () {
              Navigator.of(context, rootNavigator: true).pop();
              onSortOptionChanged(option);
            },
            child: Center(
              child: Text(
                option.label(context),
                style: Theme.of(context).textTheme.headline5.copyWith(
                    color: option == sortBy ? customBlueColor : lightBlack),
              ),
            ),
          );
        }).toList(),
      ),
    );

    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
