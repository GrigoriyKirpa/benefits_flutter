import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/specification_editor_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_list_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_screen.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:benefit_flutter/presentation/widgets/specification_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SpecificationList extends StatefulWidget {
  final List<Specification> specifications;
  final VoidCallback onSpecificationPressed;
  final Function(Specification) onSpecificationAdded;

  SpecificationList({
    Key key,
    List<Specification> specifications,
    VoidCallback onSpecificationPressed,
    Function(Specification) onSpecificationAdded,
  })  : this.specifications = specifications ?? [],
        this.onSpecificationPressed = onSpecificationPressed ?? (() => {}),
        this.onSpecificationAdded = onSpecificationAdded ?? ((_) => {}),
        super(key: key);
  @override
  _SpecificationListState createState() => _SpecificationListState();
}

class _SpecificationListState extends State<SpecificationList> {
  List<Specification> _specifications = [];

  @override
  void initState() {
    _specifications = widget.specifications;
    super.initState();
  }

  void reloadSpecifications() {
    setState(() {
      _specifications = widget.specifications;
    });
    widget.onSpecificationPressed();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                FlutterI18n.translate(context, 'user.specification.text'),
                style: Theme.of(context).textTheme.headline1,
              ),
              Spacer(),
              IconButton(
                onPressed: () => Navigator.pushNamed(
                        context, SpecificationListScreen.routeName)
                    .then((value) {
                  reloadSpecifications();
                  widget.onSpecificationPressed();
                }),
                icon: Icon(
                  CustomIcons.right_arrow,
                  color: customTextGrayColor1,
                ),
              ),
            ],
          ),
          Space.vertical(19),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width - 20,
                height: specificationWidgetHeight,
                child: ListView(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context, rootNavigator: true).pushNamed(
                          SpecificationEditorScreen.routeName,
                          arguments: SpecificationEditorScreenArguments(
                            onCreatePressed: (specification) {
                              widget.onSpecificationAdded(specification);
                              Navigator.of(context, rootNavigator: true)
                                  .pushReplacementNamed(
                                SpecificationScreen.routeName,
                                arguments: specification,
                              )
                                  .then((_) {
                                reloadSpecifications();
                              });
                            },
                          ),
                        );
                      },
                      child: Icon(
                        CustomIcons.add_folder,
                        color: customBlueColor,
                        size: specificationWidgetHeight,
                      ),
                    ),
                    ...List.generate(_specifications.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 22),
                        child: SpecificationWidget(
                          specification: _specifications[index],
                          onSpecificationPressed: reloadSpecifications,
                        ),
                      );
                    }),
                  ],
                  scrollDirection: Axis.horizontal,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
