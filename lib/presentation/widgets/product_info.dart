import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

enum ProductInfoTabs {
  Description,
  Features,
}

extension ProductInfoTabsExtension on ProductInfoTabs {
  String tabText(BuildContext context) {
    String tabText;

    if (this == ProductInfoTabs.Description) {
      tabText = FlutterI18n.translate(
          context, "product.page.description_tabs.description");
    } else if (this == ProductInfoTabs.Features) {
      tabText = FlutterI18n.translate(
          context, "product.page.description_tabs.features");
    }

    return tabText;
  }
}

class ProductInfo extends StatefulWidget {
  final String description;
  final Map<String, String> features;

  ProductInfo({Key key, this.description, this.features}) : super(key: key);

  @override
  _ProductInfoState createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  ProductInfoTabs _currentTab = ProductInfoTabs.Description;

  bool isSelected(ProductInfoTabs tab) {
    return tab == _currentTab;
  }

  TextStyle style(ProductInfoTabs tab) {
    return Theme.of(context).textTheme.headline1.copyWith(
          fontSize: 14,
          color: isSelected(tab) ? Colors.black : customTextGrayColor1,
        );
  }

  Widget get tabContent {
    if (_currentTab == ProductInfoTabs.Description) {
      return Html(
        data: widget.description,
      );
    } else if (_currentTab == ProductInfoTabs.Features) {
      return Table(
        border: TableBorder.all(),
        children: widget.features.keys.map((key) {
          return TableRow(
            children: [
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    key,
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.features[key],
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                ),
              ),
            ],
          );
        }).toList(),
      );
    } else {
      throw UnimplementedError();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: ProductInfoTabs.values.map((tab) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  _currentTab = tab;
                });
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                  border: isSelected(tab)
                      ? Border(
                          bottom:
                              BorderSide(width: 0.5, color: customGrayColor1),
                        )
                      : null,
                ),
                margin: const EdgeInsets.only(right: 16.0),
                child: Text(
                  tab.tabText(context),
                  style: style(tab),
                ),
              ),
            );
          }).toList(),
        ),
        Space.vertical(16),
        tabContent,
      ],
    );
  }
}
