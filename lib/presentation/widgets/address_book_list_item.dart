import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/address.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class AddressBookListItem extends StatelessWidget {
  final Address address;
  final VoidCallback onItemPressed;
  final VoidCallback onEditButtonPressed;
  final String headerText;
  final Color headerColor;

  AddressBookListItem({
    Key key,
    @required this.address,
    @required this.headerText,
    this.headerColor = Colors.black,
    VoidCallback onItemPressed,
    VoidCallback onEditButtonPressed,
  })  : this.onItemPressed = onItemPressed ?? (() => null),
        this.onEditButtonPressed = onEditButtonPressed ?? (() => null),
        assert(address != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () => onItemPressed(),
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 13, horizontal: 26),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(
                  Icons.house,
                  size: 16,
                  color: headerColor,
                ),
                Space.horizontal(8),
                Text(
                  headerText,
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(fontSize: 10, color: headerColor),
                ),
                Spacer(),
                TextButton(
                  onPressed: () => onEditButtonPressed(),
                  child: Text(
                    FlutterI18n.translate(
                        context, "user.address.page.edit_button"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 9, color: customTextGrayColor1),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 9),
              child: Text(
                "${address.street} ${FlutterI18n.translate(context, "user.address.street")}, ${FlutterI18n.translate(context, "user.address.flat")} ${address.flat}, " +
                    "${FlutterI18n.translate(context, "user.address.house")} ${address.house}, ${FlutterI18n.translate(context, "user.address.level")} ${address.level}." +
                    "\n${address.city}\n${FlutterI18n.translate(context, "user.address.country")}",
                style: Theme.of(context).textTheme.headline5.copyWith(
                      fontSize: 10,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
