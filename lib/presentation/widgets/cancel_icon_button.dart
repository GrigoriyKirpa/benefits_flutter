import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';

class CancelIconButton extends StatelessWidget {
  final double size;

  CancelIconButton({Key key, this.size = 32})
      : assert(size > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: size,
      onPressed: () => Navigator.pop(context),
      icon: Icon(
        Icons.clear,
        color: customGrayColor1,
      ),
    );
  }
}
