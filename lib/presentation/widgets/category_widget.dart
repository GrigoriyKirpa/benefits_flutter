import 'package:benefit_flutter/data/models/category_descriptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CategoryWidget extends StatelessWidget {
  final CategoryDescriptor category;
  final VoidCallback onPressed;
  final Color backgroundColor;
  final Color textColor;

  CategoryWidget({
    Key key,
    @required this.category,
    this.onPressed,
    this.backgroundColor = Colors.white,
    this.textColor = Colors.black,
  })  : assert(category != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPressed,
      child: Card(
        color: backgroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 12),
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  category.name,
                  style: Theme.of(context).textTheme.headline5.copyWith(
                        fontSize: 18,
                        color: textColor,
                      ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            if (category.hasImage)
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0, right: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CachedNetworkImage(
                      imageUrl: category.image,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          Center(child: CircularProgressIndicator()),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
