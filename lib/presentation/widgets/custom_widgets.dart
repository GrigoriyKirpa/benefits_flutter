import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/price.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CustomWidgets {
  CustomWidgets._();

  static Widget totalPriceWidget(
      BuildContext context, double price, TextStyle style) {
    return Text(
      Price.doubleToPriceString(context, price),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: style,
    );
  }

  static Widget errorIcon({
    @required BuildContext context,
    @required String message,
  }) {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: customRedColor,
          ),
          width: 29,
          height: 28,
          child: Center(
            child: Text(
              "!",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(color: Colors.white, fontSize: 18),
            ),
          ),
        ),
        Space.horizontal(10),
        Container(
          width: 200,
          child: Text(
            message,
            style: Theme.of(context)
                .textTheme
                .headline1
                .copyWith(color: Colors.white, fontSize: 13),
          ),
        ),
      ],
    );
  }

  static SnackBar snackBar(Color color, Widget content) {
    return SnackBar(
      backgroundColor: color,
      elevation: 2,
      duration: snackBarDisplayDuration,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(10),
          topLeft: Radius.circular(10),
        ),
      ),
      content: content,
    );
  }

  static SnackBar textSnackBar(Color color, String message, TextStyle style) {
    return snackBar(
      color,
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                message,
                style: style,
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ),
          ),
        ],
      ),
    );
  }

  static Widget pricePerPieceWidget(
      BuildContext context, double price, TextStyle style,
      {TextAlign align}) {
    return Text(
      "${Price.doubleToPriceString(context, price)}/${FlutterI18n.translate(context, 'per_piece')}",
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      textAlign: align,
      style: style,
    );
  }

  static Widget totalPriceRichText(
      BuildContext context, double price, TextStyle priceStyle,
      {TextSpan leading}) {
    return RichText(
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        children: [
          if (leading != null) leading,
          TextSpan(
            text: Price.doubleToPriceString(context, price),
            style: priceStyle,
          ),
        ],
      ),
    );
  }

  static Widget pricePerPieceRichText(BuildContext context, double price,
      TextStyle priceStyle, TextStyle pieceStyle,
      {TextSpan leading}) {
    return RichText(
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        children: [
          if (leading != null) leading,
          TextSpan(
            children: [
              TextSpan(
                text: Price.doubleToPriceString(context, price),
                style: priceStyle,
              ),
              TextSpan(
                text: "/${FlutterI18n.translate(context, 'per_piece')}",
                style: pieceStyle,
              ),
            ],
          ),
        ],
      ),
    );
  }

  static Widget pricePerPieceRichTextDiscounted(BuildContext context,
      Price price, TextStyle priceStyle, TextStyle pieceStyle,
      {double discountTextSize = 14}) {
    return RichText(
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        children: [
          TextSpan(
            text: price.getPriceString(context),
            style: priceStyle,
          ),
          TextSpan(
            text: "/${FlutterI18n.translate(context, 'per_piece')}",
            style: pieceStyle,
          ),
          if (price.isDiscounted)
            TextSpan(
              text: " ${Price.doubleToPriceString(context, price.basePrice)}",
              style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: discountTextSize,
                  color: customTextGrayColor1,
                  decoration: TextDecoration.lineThrough),
            ),
        ],
      ),
    );
  }

  static PreferredSizeWidget customAppbar(double height, Widget center,
      [bool bottomInsets = true]) {
    return PreferredSize(
      preferredSize: Size.fromHeight(height), // here the desired height
      child: SizedBox(
        height: height,
        child: Padding(
          padding:
              bottomInsets ? EdgeInsets.only(bottom: 10.0) : EdgeInsets.zero,
          child: center,
        ),
      ),
    );
  }

  static PreferredSizeWidget appbar(BuildContext context, Widget widget,
      [bool noLeading = false]) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: noLeading
          ? null
          : Navigator.of(context).canPop()
              ? Builder(
                  builder: (BuildContext context) {
                    return IconButton(
                      icon: const Icon(
                        Icons.arrow_back_ios_rounded,
                      ), // Put icon of your preference.
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    );
                  },
                )
              : null,
      title: widget,
    );
  }

  static PreferredSizeWidget appbarWithText(BuildContext context, String text) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: Navigator.of(context).canPop()
          ? Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(
                    Icons.arrow_back_ios_rounded,
                  ), // Put icon of your preference.
                  onPressed: () {
                    Navigator.pop(context);
                  },
                );
              },
            )
          : null,
      title: Text(
        text,
        style: Theme.of(context).textTheme.headline1,
        textAlign: TextAlign.center,
      ),
      centerTitle: true,
    );
  }
}
