import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/models/specification.dart';
import 'package:benefit_flutter/data/utils/language.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/screens/specification_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SpecificationWidget extends StatefulWidget {
  final VoidCallback onSpecificationPressed;
  final Specification specification;

  @override
  _SpecificationWidgetState createState() => _SpecificationWidgetState();

  SpecificationWidget({
    Key key,
    @required this.specification,
    VoidCallback onSpecificationPressed,
  })  : this.onSpecificationPressed = onSpecificationPressed ?? (() => {}),
        assert(specification != null),
        super(key: key);
}

class _SpecificationWidgetState extends State<SpecificationWidget> {
  List<ProductDescriptor> _products;

  @override
  void initState() {
    _products = widget.specification.products;
    super.initState();
  }

  void reloadProducts() {
    if (!mounted) return;
    setState(() {
      _products = widget.specification.products;
    });
    widget.onSpecificationPressed();
  }

  void openSpecification() {
    Navigator.of(context, rootNavigator: true)
        .pushNamed(
      SpecificationScreen.routeName,
      arguments: widget.specification,
    )
        .then(
      (_) {
        reloadProducts();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: openSpecification,
      child: SizedBox(
        height: specificationWidgetHeight,
        child: Stack(
          children: [
            Icon(
              CustomIcons.folder,
              color: widget.specification.color,
              size: specificationWidgetHeight,
            ),
            Positioned(
              left: 12,
              top: 20,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "№${widget.specification.id}",
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(color: customRedColor),
                  ),
                  Text(
                    "${_products.length} ${FlutterI18n.plural(
                      context,
                      "product.products.number",
                      Language.getPlural(_products.length),
                    )}",
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                  CustomWidgets.totalPriceWidget(
                    context,
                    ProductDescriptor.calculateTotalPrice(_products),
                    Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
