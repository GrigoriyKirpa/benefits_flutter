import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/presentation/custom_icons_icons.dart';
import 'package:benefit_flutter/presentation/widgets/double_stated_button.dart';
import 'package:flutter/material.dart';

class DoubleStateButtons {
  DoubleStateButtons._();

  static DoubleStateButton likeButton({
    Key key,
    double size,
    bool isInitiallyActive = false,
    Function(bool) handleTap,
  }) =>
      DoubleStateButton(
        key: UniqueKey(),
        size: size,
        isInitiallyActive: isInitiallyActive,
        handleTap: handleTap,
        icon: CustomIcons.like,
        activeColor: customRedColor,
      );

  static Widget basketButton({
    Key key,
    double size,
    bool isInitiallyActive,
    VoidCallback handleTap,
  }) =>
      InkWell(
        key: UniqueKey(),
        customBorder: CircleBorder(),
        onTap: handleTap,
        child: Container(
          width: size * 1.5,
          height: size * 1.5,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: Icon(
            CustomIcons.basket,
            color: customRedColor,
            size: size,
          ),
        ),
      );

  static DoubleStateButton specificationButton({
    Key key,
    double size,
    bool isInitiallyActive,
    Function(bool) handleTap,
  }) =>
      DoubleStateButton(
        key: UniqueKey(),
        size: size,
        isInitiallyActive: isInitiallyActive,
        handleTap: handleTap,
        icon: CustomIcons.folder,
        activeColor: customBlueColor,
      );
}
