import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ProductNumberSelector extends StatefulWidget {
  final int stock;
  final Function(int) onValueChanged;
  final int initiallySelected;
  final int step;
  final Color color;

  ProductNumberSelector(
      {Key key,
      @required this.stock,
      Function(int) onValueChanged,
      this.color = Colors.white,
      this.step = 1,
      this.initiallySelected = 1})
      : this.onValueChanged = onValueChanged ?? ((_) {}),
        assert(stock != null && stock > 0),
        assert(step != null && step > 0),
        super(key: key);

  @override
  _ProductNumberSelectorState createState() => _ProductNumberSelectorState();
}

class _ProductNumberSelectorState extends State<ProductNumberSelector> {
  FocusNode _focus;
  int _selected;
  bool _isOpened = false;

  @override
  void initState() {
    _focus = FocusNode(skipTraversal: true);
    _focus.addListener(() {
      setState(() {
        _isOpened = !_isOpened;
      });
    });
    _selected = widget.initiallySelected;
    super.initState();
  }

  bool isSelected(int index) {
    return _selected == index;
  }

  @override
  void dispose() {
    _focus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      height: 24,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        color: widget.color,
      ),
      child: DropdownButton<int>(
        dropdownColor: widget.color,
        value: (_selected / widget.step - 1).floor(),
        iconSize: 18,
        underline: Container(
          height: 0,
        ),
        icon: Icon(
          _isOpened
              ? Icons.keyboard_arrow_up_rounded
              : Icons.keyboard_arrow_down_rounded,
        ),
        elevation: 0,
        onChanged: (index) {
          setState(() {
            _selected = (index + 1) * widget.step;
          });
        },
        focusNode: _focus,
        items: List.generate(
          (widget.stock / widget.step).floor(),
          (index) => DropdownMenuItem<int>(
            value: index,
            child: Text(
              "${(index + 1) * widget.step} ${FlutterI18n.translate(context, "per_piece")}",
              style: Theme.of(context).textTheme.headline5.copyWith(
                  fontSize: 12,
                  color: isSelected((index + 1) * widget.step) && _isOpened
                      ? customBlueColor
                      : Colors.black),
            ),
          ),
        ),
      ),
    );
  }
}
