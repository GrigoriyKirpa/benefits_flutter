import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';

class CircledCheckBox extends StatefulWidget {
  final bool initialValue;
  final Function(bool) onChecked;
  final double size;

  CircledCheckBox(
      {Key key,
      this.initialValue = false,
      this.size = 20,
      Function(bool) onChecked})
      : this.onChecked = onChecked ?? ((_) => null),
        assert(initialValue != null),
        super(key: key);

  @override
  _CircledCheckBoxState createState() => _CircledCheckBoxState();
}

class _CircledCheckBoxState extends State<CircledCheckBox> {
  bool _checked;

  @override
  void initState() {
    _checked = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.size,
      height: widget.size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          width: 2,
          color: customGrayColor1,
        ),
      ),
      child: Checkbox(
        checkColor: customBlueColor,
        fillColor: MaterialStateProperty.all(Colors.transparent),
        value: _checked,
        shape: CircleBorder(),
        onChanged: (bool value) {
          setState(() {
            _checked = value;
          });
          widget.onChecked(value);
        },
      ),
    );
  }
}
