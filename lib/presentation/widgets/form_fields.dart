import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class FormFields {
  FormFields._();

  static Widget simpleTextField(
    BuildContext context,
    String hint,
    TextEditingController controller, {
    int maxLength = 20,
    int maxLines = 1,
    TextInputType type = TextInputType.text,
    bool disableHelpText = false,
  }) {
    return TextFormField(
      maxLines: maxLines,
      maxLength: maxLength,
      textCapitalization: TextCapitalization.sentences,
      controller: controller,
      style: Theme.of(context).textTheme.headline5.copyWith(fontSize: 18),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.zero,
        helperStyle: disableHelpText ? TextStyle(fontSize: 0) : null,
        hintText: hint,
      ),
      keyboardType: type,
    );
  }

  static Widget compulsoryTextField(
      BuildContext context, String label, TextEditingController controller,
      {Function(String) validator,
      int maxLength = 20,
      int maxLines = 1,
      TextInputType type = TextInputType.text,
      bool disableHelpText = false,
      bool disableErrorText = false,
      bool isPassword = false,
      List<TextInputFormatter> formatters}) {
    return TextFormField(
      validator: validator,
      maxLength: maxLength,
      maxLines: maxLines,
      textCapitalization: TextCapitalization.sentences,
      controller: controller,
      style: Theme.of(context).textTheme.headline5.copyWith(fontSize: 18),
      inputFormatters: formatters,
      obscureText: isPassword,
      enableSuggestions: !isPassword,
      autocorrect: !isPassword,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 6, vertical: 2),
        helperStyle: disableHelpText ? TextStyle(fontSize: 0) : null,
        errorStyle: disableErrorText ? TextStyle(fontSize: 0) : null,
        fillColor: Colors.white,
        filled: true,
        labelText: label,
        labelStyle: Theme.of(context)
            .textTheme
            .headline5
            .copyWith(fontSize: 10, color: customTextGrayColor1),
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
      ),
      keyboardType: type,
    );
  }

  static String compulsoryFieldValidator(BuildContext context, String text) {
    return text == null || text.isEmpty
        ? FlutterI18n.translate(context, "form.compulsory_field")
        : null;
  }

  static String phoneNumberValidator(BuildContext context, String text) {
    final String phone = unMask(text);
    return compulsoryFieldValidator(context, text) ??
        (phone.length != 10
            ? FlutterI18n.translate(context, "form.invalid_phone")
            : null);
  }

  static String emailFieldValidator(BuildContext context, String text) {
    bool valid = RegExp(
            r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$")
        .hasMatch(text);
    return compulsoryFieldValidator(context, text) ??
        (valid ? null : FlutterI18n.translate(context, "form.invalid_email"));
  }

  static String passwordFieldValidator(BuildContext context, String text) {
    return compulsoryFieldValidator(context, text) ??
        (text.length >= 6
            ? null
            : FlutterI18n.translate(context, "form.password.short")) ??
        (RegExp(r"[^0-9a-zA-Z+\-=@#$%^&*()!]").hasMatch(text)
            ? FlutterI18n.translate(context, "form.password.invalid")
            : null);
  }

  static String unMask(String phone) {
    return phone
        .replaceAll(" ", "")
        .replaceAll("(", "")
        .replaceAll(")", "")
        .replaceAll("-", "");
  }

  static MaskTextInputFormatter maskPhoneFormatter = MaskTextInputFormatter(
      mask: '### ###-##-##', filter: {"#": RegExp('[0-9]')});
}
