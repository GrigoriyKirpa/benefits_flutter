import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/order_status.dart';
import 'package:benefit_flutter/data/models/order.dart';
import 'package:benefit_flutter/data/utils/server_requests.dart';
import 'package:benefit_flutter/presentation/screens/order_page_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

class OrderListItemWidget extends StatefulWidget {
  final Order order;
  final VoidCallback onCancelFailed;
  final VoidCallback onOrderRepeatPressed;

  OrderListItemWidget(
      {Key key,
      @required this.order,
      VoidCallback onCancelFailed,
      VoidCallback onOrderRepeatPressed})
      : this.onCancelFailed = onCancelFailed ?? (() => null),
        this.onOrderRepeatPressed = onOrderRepeatPressed ?? (() => null),
        assert(order != null),
        super(key: key);

  @override
  _OrderListItemWidgetState createState() => _OrderListItemWidgetState();
}

class _OrderListItemWidgetState extends State<OrderListItemWidget> {
  bool _active = false;

  Future<void> cancelOrder() async {
    final bool result = await ServerRequests.cancelOrder(widget.order.id);
    if (result) {
      setState(() {
        widget.order.status = OrderStatus.Canceled;
      });
    } else {
      widget.onCancelFailed();
    }
  }

  @override
  Widget build(BuildContext context) {
    final Map<OrderStatus, Function()> actionButtonCallbacks = {
      OrderStatus.InProcess: cancelOrder,
      OrderStatus.Confirmed: cancelOrder,
      OrderStatus.Packed: cancelOrder,
      OrderStatus.OnTheWay: cancelOrder,
      OrderStatus.Delivered: widget.onOrderRepeatPressed,
      OrderStatus.Canceled: widget.onOrderRepeatPressed,
    };
    return RawMaterialButton(
      fillColor: _active ? customBlueColor : Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      elevation: 6,
      onPressed: () {
        setState(() {
          _active = true;
        });
        Navigator.pushNamed(context, OrderPageScreen.routeName,
                arguments: widget.order)
            .then(
          (value) => setState(
            () => _active = false,
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 28, vertical: 11),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "#${widget.order.id}",
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(color: _active ? Colors.white : Colors.black),
                ),
                Space.vertical(3),
                Text(
                  widget.order.status.label(context),
                  style: Theme.of(context).textTheme.headline5.copyWith(
                      color: _active
                          ? Colors.white
                          : widget.order.status.statusLabelColor),
                ),
                Space.vertical(4),
                Text(
                  DateFormat("dd.MM.yy").format(widget.order.date),
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(color: _active ? Colors.white : Colors.black),
                ),
                Space.vertical(5),
              ],
            ),
            Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CustomWidgets.totalPriceWidget(
                  context,
                  widget.order.payment.total,
                  Theme.of(context).textTheme.headline5.copyWith(
                        fontSize: 18,
                        color: _active ? Colors.white : Colors.black,
                      ),
                ),
                // Space.vertical(3),
                // GestureDetector(
                //   onTap: () {},
                //   child: Text(
                //     FlutterI18n.translate(context, "order.receipt"),
                //     style: Theme.of(context).textTheme.headline5.copyWith(
                //         decoration: TextDecoration.underline,
                //         color: _active ? Colors.white : lightBlack),
                //   ),
                // ),
                Space.vertical(4),
                GestureDetector(
                  onTap: actionButtonCallbacks[widget.order.status],
                  child: Text(
                    widget.order.status.actionButtonLabel(context),
                    style: Theme.of(context).textTheme.headline5.copyWith(
                        fontSize: 12,
                        color: _active
                            ? Colors.black
                            : widget.order.status.actionButtonLabelColor),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
