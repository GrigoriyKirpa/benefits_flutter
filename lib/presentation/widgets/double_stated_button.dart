import 'package:benefit_flutter/constants.dart';
import 'package:flutter/material.dart';

class DoubleStateButton extends StatefulWidget {
  final double size;
  final bool isInitiallyActive;
  final Function(bool) handleTap;
  final Color notActiveColor;
  final Color activeColor;
  final IconData icon;

  DoubleStateButton({
    Key key,
    this.size,
    this.isInitiallyActive = false,
    this.notActiveColor = customGrayColor1,
    @required this.activeColor,
    @required this.icon,
    Function(bool) handleTap,
  })  : this.handleTap = handleTap ?? ((_) => {}),
        assert(icon != null),
        super(key: key);

  @override
  _DoubleStateButtonState createState() => _DoubleStateButtonState();
}

class _DoubleStateButtonState extends State<DoubleStateButton> {
  bool isActive;

  @override
  void initState() {
    isActive = widget.isInitiallyActive ?? false;
    super.initState();
  }

  void handleTap() {
    setState(() {
      isActive = !isActive;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        handleTap();
        widget.handleTap(isActive);
      },
      child: Icon(
        widget.icon,
        color: isActive ? widget.activeColor : widget.notActiveColor,
        size: widget.size,
      ),
    );
  }
}
