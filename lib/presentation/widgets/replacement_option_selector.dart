import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/replacement_options.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class ReplacementOptionSelector extends StatefulWidget {
  final ReplacementOptions initialOption;
  final Function(ReplacementOptions) onChanged;

  ReplacementOptionSelector(
      {Key key,
      @required this.initialOption,
      Function(ReplacementOptions) onChanged})
      : this.onChanged = onChanged ?? ((_) => null),
        assert(initialOption != null),
        super(key: key);

  @override
  _ReplacementOptionSelectorState createState() =>
      _ReplacementOptionSelectorState();
}

class _ReplacementOptionSelectorState extends State<ReplacementOptionSelector> {
  ReplacementOptions _option;

  @override
  void initState() {
    _option = widget.initialOption;
    super.initState();
  }

  void onChanged(ReplacementOptions option) {
    setState(() {
      _option = option;
    });
    widget.onChanged(option);
  }

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () => changeReplacementOptions(),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(7),
        ),
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        child: Row(
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                _option.label(context),
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontSize: 12),
              ),
            ),
            Space.horizontal(36),
            Icon(
              Icons.keyboard_arrow_down,
              size: 18,
              color: customGrayColor1,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> changeReplacementOptions() async {
    final Widget alert = AlertDialog(
      contentPadding: const EdgeInsets.all(14),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Icon(
                  Icons.close_rounded,
                  size: 16,
                  color: customGrayColor1,
                ),
              ),
            ],
          ),
          ...ReplacementOptions.values.map((option) {
            return RawMaterialButton(
              onPressed: () {
                onChanged(option);
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 4),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 0.3, color: customGrayColor1),
                    top: BorderSide(width: 0.3, color: customGrayColor1),
                  ),
                ),
                child: Text(
                  option.label(context),
                  style: Theme.of(context).textTheme.headline5.copyWith(
                      color:
                          option == _option ? customBlueColor : Colors.black),
                ),
              ),
            );
          }).toList(),
        ],
      ),
    );

    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
