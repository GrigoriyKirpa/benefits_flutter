import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/company_details.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CompanyDetailsListItem extends StatelessWidget {
  final CompanyDetails companyDetails;
  final VoidCallback onItemPressed;
  final VoidCallback onEditButtonPressed;
  final Color headerColor;

  CompanyDetailsListItem({
    Key key,
    @required this.companyDetails,
    this.headerColor = Colors.black,
    this.onItemPressed,
    VoidCallback onEditButtonPressed,
  })  : this.onEditButtonPressed = onEditButtonPressed ?? (() => null),
        assert(companyDetails != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onItemPressed,
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 13, horizontal: 26),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(
                  Icons.house,
                  size: 16,
                  color: headerColor,
                ),
                Space.horizontal(8),
                Text(
                  companyDetails.name,
                  style: Theme.of(context)
                      .textTheme
                      .headline1
                      .copyWith(fontSize: 10, color: headerColor),
                ),
                Spacer(),
                TextButton(
                  onPressed: onEditButtonPressed,
                  child: Text(
                    FlutterI18n.translate(
                        context, "user.company_details.page.edit_button"),
                    style: Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 9, color: customTextGrayColor1),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 9),
              child: Text(
                "${FlutterI18n.translate(context, "user.company_details.inn")} ${companyDetails.inn}\n"
                "${FlutterI18n.translate(context, "user.company_details.kpp")} ${companyDetails.kpp}\n",
                style: Theme.of(context).textTheme.headline5.copyWith(
                      fontSize: 10,
                    ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
