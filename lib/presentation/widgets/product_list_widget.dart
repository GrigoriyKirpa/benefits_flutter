import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/enums/main_navigation.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/models/product_descriptor.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/mixins/basket_notifier.dart';
import 'package:benefit_flutter/presentation/screens/product_screen.dart';
import 'package:benefit_flutter/presentation/screens/specification_selector_screen.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/double_stated_buttons.dart';
import 'package:benefit_flutter/presentation/widgets/product_number_selector.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ProductListWidget extends StatefulWidget {
  final ProductDescriptor product;
  final VoidCallback onItemPressed;
  final VoidCallback onLikeHandled;
  final VoidCallback onBasketHandled;
  final VoidCallback onSpecificationHandled;

  ProductListWidget({
    Key key,
    @required this.product,
    VoidCallback onLikeHandled,
    VoidCallback onSpecificationHandled,
    VoidCallback onItemPressed,
    VoidCallback onBasketHandled,
  })  : this.onItemPressed = onItemPressed ?? (() => {}),
        this.onLikeHandled = onLikeHandled ?? (() => {}),
        this.onSpecificationHandled = onSpecificationHandled ?? (() => {}),
        this.onBasketHandled = onBasketHandled ?? (() => {}),
        assert(product != null),
        super(key: key);

  @override
  _ProductListWidget createState() => _ProductListWidget();
}

class _ProductListWidget extends State<ProductListWidget> with BasketNotifier {
  bool _isLiked = false;
  bool _isInSpecification = false;
  int _selectedNumber;

  @override
  void initState() {
    _selectedNumber = widget.product.minAmount;
    _isLiked = Shop.currentUser.isLiked(widget.product.id);
    _isInSpecification = Shop.currentUser.isInSpecification(widget.product);
    super.initState();
  }

  @override
  void displayAddedToBasket(BuildContext context) {
    if (!mounted) {
      return;
    }
    super.displayAddedToBasket(context);
  }

  @override
  void displayNotAddedToBasket(BuildContext context) {
    if (!mounted) {
      return;
    }
    super.displayNotAddedToBasket(context);
  }

  void reloadButtons() {
    if (!mounted) return;
    setState(() {
      _isLiked = Shop.currentUser.isLiked(widget.product.id);
      _isInSpecification = Shop.currentUser.isInSpecification(widget.product);
    });
  }

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: () {
        widget.onItemPressed();
        MainNavigationExtension.currentTab.navigationKey.currentState
            .pushNamed(ProductScreen.routeName, arguments: widget.product)
            .then((value) => reloadButtons());
      },
      child: SizedBox(
        height: productListWidgetHeight,
        width: MediaQuery.of(context).size.width,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 105,
              width: 80,
              child: widget.product.image != null
                  ? CachedNetworkImage(
                      imageUrl: widget.product.image,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          Center(child: CircularProgressIndicator()),
                    )
                  : Container(
                      color: customGrayColor1,
                      child: Icon(
                        Icons.image_not_supported,
                        color: Colors.black,
                        size: 64,
                      ),
                    ),
            ),
            Space.horizontal(15),
            Flexible(
              fit: FlexFit.tight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.product.name,
                    style: Theme.of(context).textTheme.headline5,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Space.vertical(7),
                  Row(
                    children: [
                      RatingBar.builder(
                        itemPadding: EdgeInsets.zero,
                        itemBuilder: (context, index) {
                          return Icon(
                            Icons.star,
                            color: starColor,
                          );
                        },
                        allowHalfRating: true,
                        ignoreGestures: true,
                        itemSize: 15.0,
                        onRatingUpdate: (_) => null,
                        maxRating: 5,
                        minRating: 0,
                        unratedColor: customGrayColor1,
                        initialRating: widget.product.rating.rating,
                      ),
                      Space.horizontal(6),
                      Text(
                        "(${widget.product.rating.voteCount})",
                        style: Theme.of(context).textTheme.headline5.copyWith(
                            fontSize: 12, color: customTextGrayColor1),
                      ),
                    ],
                  ),
                  Space.vertical(7),
                  CustomWidgets.pricePerPieceRichTextDiscounted(
                    context,
                    widget.product.price,
                    Theme.of(context).textTheme.headline5.copyWith(
                          color: widget.product.price.isDiscounted
                              ? customRedColor
                              : Colors.black,
                        ),
                    Theme.of(context)
                        .textTheme
                        .headline5
                        .copyWith(fontSize: 12, color: customTextGrayColor1),
                  ),
                  Spacer(),
                  ProductNumberSelector(
                    stock: defaultMiniStock * widget.product.minAmount,
                    initiallySelected: _selectedNumber,
                    onValueChanged: (value) => _selectedNumber = value,
                    step: widget.product.minAmount,
                  ),
                ],
              ),
            ),
            Space.horizontal(10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  children: [
                    DoubleStateButtons.likeButton(
                      size: 28,
                      isInitiallyActive: _isLiked,
                      handleTap: (_) {
                        Shop.currentUser.handleLike(widget.product).then(
                          (success) {
                            if (success) {
                              reloadButtons();
                              widget.onLikeHandled();
                            }
                          },
                        );
                      },
                    ),
                    Space.horizontal(15),
                    DoubleStateButtons.specificationButton(
                        size: 28,
                        isInitiallyActive: _isInSpecification,
                        handleTap: (wasAdded) {
                          if (wasAdded) {
                            Navigator.pushNamed(
                                context, SpecificationSelectorScreen.routeName,
                                arguments: (specification) {
                              Shop.currentUser.addToSpecification(
                                  specification, widget.product);
                              widget.onSpecificationHandled();
                            });
                          } else {
                            Shop.currentUser
                                .removeFromSpecification(widget.product);
                            widget.onSpecificationHandled();
                          }
                        }),
                  ],
                ),
                Spacer(),
                DoubleStateButtons.basketButton(
                  size: 28,
                  handleTap: () async {
                    if (await Shop.basket.add(
                      OrderItem(
                        product: widget.product,
                        price: widget.product.price,
                        amount: _selectedNumber,
                      ),
                    )) {
                      reloadButtons();
                      widget.onBasketHandled();
                      displayAddedToBasket(context);
                    } else {
                      reloadButtons();
                      widget.onBasketHandled();
                      displayNotAddedToBasket(context);
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
