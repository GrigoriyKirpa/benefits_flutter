import 'package:benefit_flutter/constants.dart';
import 'package:benefit_flutter/data/models/order_item.dart';
import 'package:benefit_flutter/data/shop.dart';
import 'package:benefit_flutter/presentation/widgets/custom_widgets.dart';
import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class OrderItemListWidget extends StatelessWidget {
  final OrderItem item;

  OrderItemListWidget({Key key, @required this.item})
      : assert(item != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 105,
          width: 80,
          child: item.product.image != null && item.product.image.isNotEmpty
              ? CachedNetworkImage(
                  imageUrl: item.product.image,
                  fit: BoxFit.cover,
                  placeholder: (context, url) =>
                      Center(child: CircularProgressIndicator()),
                )
              : Container(
                  color: customGrayColor1,
                  child: Icon(
                    Icons.image_not_supported,
                    color: Colors.black,
                    size: 64,
                  ),
                ),
        ),
        Space.horizontal(15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.product.name,
                style: Theme.of(context)
                    .textTheme
                    .headline1
                    .copyWith(fontSize: 14),
              ),
              Space.vertical(1),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(
                          context, "order.item.manufacturer"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                    TextSpan(
                      text: " ${item.product.manufacturer ?? ""}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                  ],
                ),
              ),
              Space.vertical(2),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(
                          context, "order.item.set_number"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                    TextSpan(
                      text: " ${item.product.setNumber ?? ""}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                  ],
                ),
              ),
              Space.vertical(7),
              CustomWidgets.pricePerPieceRichText(
                context,
                item.price.price,
                Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: customBlueColor, fontSize: 14),
                Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(color: customBlueColor, fontSize: 12),
                leading: TextSpan(
                  text:
                      "${FlutterI18n.translate(context, "order.item.price")} ",
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(color: customTextGrayColor1, fontSize: 9),
                ),
              ),
              Space.vertical(4),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: FlutterI18n.translate(context, "order.item.amount"),
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(color: customTextGrayColor1, fontSize: 9),
                    ),
                    TextSpan(
                      text:
                          " ${item.amount} ${FlutterI18n.translate(context, 'per_piece')}",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 14),
                    ),
                  ],
                ),
              ),
              Space.vertical(4),
              CustomWidgets.totalPriceRichText(
                context,
                item.totalPrice,
                Theme.of(context).textTheme.headline5.copyWith(fontSize: 14),
                leading: TextSpan(
                  text:
                      "${FlutterI18n.translate(context, "order.item.total")} ",
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(color: customTextGrayColor1, fontSize: 9),
                ),
              ),
              if (item.selectedColor != null) ...[
                Space.vertical(4),
                Row(
                  children: [
                    Text(
                      "${FlutterI18n.translate(context, "order.item.color")} ",
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontSize: 9),
                    ),
                    Container(
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: Shop.getColor(item.selectedColor),
                          fit: BoxFit.fill,
                        ),
                      ),
                      width: 15,
                      height: 15,
                    ),
                  ],
                ),
              ],
              if (item.selectedSize != null) ...[
                Space.vertical(4),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: FlutterI18n.translate(context, "order.item.size"),
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(color: customTextGrayColor1, fontSize: 9),
                      ),
                      TextSpan(
                        text: " ${item.selectedSize}",
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ],
            ],
          ),
        ),
      ],
    );
  }
}
