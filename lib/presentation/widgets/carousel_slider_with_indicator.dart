import 'package:benefit_flutter/presentation/widgets/spaces.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class CarouselSliderWithIndicator extends StatefulWidget {
  final List<CachedNetworkImage> images;
  final double height;
  final bool indicatorOnImage;

  CarouselSliderWithIndicator({
    Key key,
    List<CachedNetworkImage> images,
    this.height = 150.0,
    this.indicatorOnImage = true,
  })  : this.images = images ?? [],
        super(key: key);
  @override
  _CarouselSliderWithIndicatorState createState() =>
      _CarouselSliderWithIndicatorState();
}

class _CarouselSliderWithIndicatorState
    extends State<CarouselSliderWithIndicator> {
  Widget get carousel {
    return CarouselSlider(
      carouselController: _controller,
      options: CarouselOptions(
        height: widget.height,
        viewportFraction: 1,
        onPageChanged: (index, reason) {
          setState(() {
            _current = index;
          });
        },
      ),
      items: widget.images,
    );
  }

  Widget get body {
    if (widget.indicatorOnImage) {
      return Stack(
        children: [
          carousel,
          Positioned(
            bottom: 13,
            width: MediaQuery.of(context).size.width,
            child: indicator,
          ),
        ],
      );
    } else {
      return Column(
        children: [
          carousel,
          Space.vertical(4),
          indicator,
        ],
      );
    }
  }

  Widget get indicator {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xFFBABABA).withOpacity(0.39),
          borderRadius: BorderRadius.circular(8),
        ),
        padding: EdgeInsets.symmetric(horizontal: 7.6),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: widget.images.asMap().entries.map((entry) {
            return GestureDetector(
              onTap: () => _controller.animateToPage(entry.key),
              child: Container(
                width: 7.65,
                height: 7.65,
                margin: EdgeInsets.symmetric(vertical: 3.06, horizontal: 2.29),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == entry.key
                      ? Color(0xFFB6B6B6)
                      : Color(0xFFC4C4C4).withOpacity(0.5),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }

  int _current = 0;
  final CarouselController _controller = CarouselController();
  @override
  Widget build(BuildContext context) {
    return widget.images.length == 0
        ? SizedBox(
            height: widget.height,
            width: MediaQuery.of(context).size.width,
            child: const Icon(
              Icons.image_not_supported,
              size: 56,
            ),
          )
        : SizedBox(
            width: MediaQuery.of(context).size.width,
            child: body,
          );
  }
}
